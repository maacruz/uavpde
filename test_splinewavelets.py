import numpy as np
from splinewavelets import *

def test_splmake():
    # simplest spline degree 0, single vector of coefs
    x = np.array([2,3])
    degree = 0
    tck, u = splmake(x, degree)
    np.testing.assert_equal(tck[0], np.array([0,0.5,1]))
    np.testing.assert_equal(tck[1], np.array([2,3]))
    np.testing.assert_equal(u,np.array([0,0.5]))

    # spline degree 1, multiple rows of coefs
    x = np.array([[2,3,5]])
    degree = 1
    tck, u = splmake(x, degree)
    np.testing.assert_equal(tck[0], np.array([0,0,0.5,1,1]))
    np.testing.assert_equal(tck[1], np.array([[2,3,5]]))
    np.testing.assert_equal(u, np.array([0,0.5,1]))

    # spline degree 2, multiple rows of coefs
    x = np.array([[2,3,5],[1,2,4]])
    degree = 2
    tck, u = splmake(x, degree)
    np.testing.assert_equal(tck[0], np.array([0,0,0,1,1,1]))
    np.testing.assert_equal(tck[1], np.array([[2,2.5,5],[1,1.5,4]]))
    np.testing.assert_equal(u, np.array([0,0.5,1]))

    for k in xrange(2,6):
        for n in xrange(k+1,k+10):
            x = np.random.random(n)
            tck, u = splmake(x, k)
            assert len(tck[0]) == n+k+1
            np.testing.assert_allclose(x,splev(u,tck))

def test_double_knots():
    t = np.array([0.,1.])
    t1 = double_knots(t)
    np.testing.assert_almost_equal(t1, np.array([0.,0.5,1.]))

def test_splrefine():
    # 1st degree 2 control points, vector of coefs
    tck = (np.array([0,0,1,1],float), np.array([0,1],float), 1)
    tck1 = splrefine(tck)
    np.testing.assert_equal(tck1,(np.array([0,0,0.5,1,1]), np.array([0,0.5,1]), 1))
    
    tck = (np.array([0,0,1,1],float), np.array([[0,1],[1,2]],float), 1)
    tck1 = splrefine(tck)
    np.testing.assert_equal(tck1,(np.array([0,0,0.5,1,1]), np.array([[0,0.5,1],[1,1.5,2]]), 1))
    
    # 2nd degree 3 control points, 2 splines
    tck = (np.array([0,0,0,1,1,1],float), np.array([[0,1,0],[1,3,1]],float), 2)
    tck1 = splrefine(tck)
    np.testing.assert_equal(tck1,(np.array([0,0,0,0.5,1,1,1]), np.array([[0,0.5,0.5,0],[1,2,2,1]]), 2))
    
    # 3rd degree 6 control points
    tck = (np.array([0,0,0,0,1./3.,2./3.,1,1,1,1]), np.array([0,1,3,2,5,4]), 3)
    P = np.array([[16, 0, 0, 0, 0, 0],
                  [ 8, 8, 0, 0, 0, 0],
                  [ 0,12, 4, 0, 0, 0],
                  [ 0, 3,11, 2, 0, 0],
                  [ 0, 0, 8, 8, 0, 0],
                  [ 0, 0, 2,11, 3, 0],
                  [ 0, 0, 0, 4,12, 0],
                  [ 0, 0, 0, 0, 8, 8],
                  [ 0, 0, 0, 0, 0,16]],float)/16
    tck1 = splrefine(tck)
    np.testing.assert_almost_equal(tck1[0],np.array([0,0,0,0,1./6.,2./6.,3./6.,4./6.,5./6.,1,1,1,1]))
    np.testing.assert_almost_equal(tck1[1],np.dot(tck[1],P.T))

def test_frenet3D():
    x = np.array([[0.,0.,0.],[0.5,0.0,0.0],[1.,0.,0.]]).T
    tck, u = splmake(x, 2)
    T, N, B = frenet3D(u, tck)
    np.testing.assert_equal(T, np.repeat(np.array([[1.,0.,0.]]).T, 3, 1))
    np.testing.assert_equal(N, np.repeat(np.array([[0.,1.,0.]]).T, 3, 1))
    np.testing.assert_equal(B, np.repeat(np.array([[0.,0.,1.]]).T, 3, 1))

    x = np.array([[0.,0.,0.],[0.5,0.0,0.25],[1.,0.,1.]]).T
    tck, u = splmake(x, 2)
    T, N, B = frenet3D(u, tck)
    np.testing.assert_almost_equal(T, np.array([[1.,0.,0.],[np.sqrt(0.5),0.,np.sqrt(0.5)],[np.cos(np.arctan(2)),0.,np.sin(np.arctan(2))]]).T, 4)
    np.testing.assert_almost_equal(N, np.array([[0.,0.,1.],[-np.sqrt(0.5),0.,np.sqrt(0.5)],[-np.sin(np.arctan(2)),0.,np.cos(np.arctan(2))]]).T)
    np.testing.assert_almost_equal(B, np.repeat(np.array([[0.,-1.,0.]]).T, 3, 1))

def test_spldiscretize():
    x = np.array([0., 1.])
    tck, u = splmake(x, 1)
    points, u_i = spldiscretize(tck, 0.1)
    np.testing.assert_equal(u_i, np.array([]))
    np.testing.assert_almost_equal(points, np.linspace(0.,1.,1 + int(1./0.1)))

    x = np.array([[0., 1.], [0., 1.]])
    tck, u = splmake(x, 1)
    d = np.sqrt(2.)/2.
    points, u_i = spldiscretize(tck, d, np.array([0.25]))
    np.testing.assert_equal(u_i, np.array([1]))
    np.testing.assert_almost_equal(points, np.array([[0., 0.25, 0.5, 1.], [0., 0.25, 0.5, 1.]]))


def test_wsplBernsteinWeightsMatrix():
    F = wsplBernsteinWeightsMatrix(degree=2, ncoefs=3).full()
    np.testing.assert_equal(F,np.eye(3))

    F = wsplBernsteinWeightsMatrix(degree=2, ncoefs=4).full()
    F1 = np.array([[1,  0,  0,0],
                   [0,  1,  0,0],
                   [0,0.5,0.5,0]])
    F1 = np.vstack([F1,F1[::-1,::-1]])
    np.testing.assert_equal(F,F1)

    for degree in xrange(2,5):
        for ncoefs in xrange(degree+1,20):
            F = wsplBernsteinWeightsMatrix(degree, ncoefs).full()
            np.testing.assert_allclose(F.sum(1),np.ones((ncoefs-degree)*(degree+1)))


def test_BernsteinInner():
    np.testing.assert_equal(BernsteinInner(0),np.array([[1.0]]))
    np.testing.assert_allclose(BernsteinInner(1),np.array([[1./3,1./6],[1./6,1./3]]))


def test_wsplScalingFInnerProductMatrix():
    F = wsplScalingFInnerProductMatrix(1,2)
    np.testing.assert_allclose(F.full(),np.array([[1./3,1./6], [1./6, 1./3]]))


def test_wsplSynthesisMatrixP():
    F = wsplSynthesisMatrixP(1,2)
    B = np.array([[1,0],[0.5,0.5],[0,1]])
    np.testing.assert_equal(B, F.full())

def test_wsplSynthesisMatrices():
    # 3rd level wavelets
    P3 = np.array([[16, 0, 0, 0, 0, 0, 0],
                   [ 8, 8, 0, 0, 0, 0, 0],
                   [ 0,12, 4, 0, 0, 0, 0],
                   [ 0, 3,11, 2, 0, 0, 0],
                   [ 0, 0, 8, 8, 0, 0, 0],
                   [ 0, 0, 2,12, 2, 0, 0]],float)/16
    P3 = np.vstack([P3, np.flipud(np.fliplr(P3[:-1]))])
    Q3 = np.array([[-394762./574765, 0, 0, 0],
                   [1, -7166160./28124263, 0, 0],
                   [-33030599./41383080, 333497715./478112471, 6908335./478112471, 0],
                   [633094403./1655323200, -881412943./956224942, -74736797./956224942, 27877./1655323200],
                   [-19083341./137943600, 1, 8833647./28124263, -864187./413830800],
                   [4681957./165532320, -689203555./956224942, -689203555./956224942, 4681957./165532320]])
    Q3 = np.vstack([Q3,np.flipud(np.fliplr(Q3[:-1]))])
    I3 = np.array([[1440,  882,  186,   12,    0,    0],
                   [ 882, 2232, 1575,  348,    3,    0],
                   [ 186, 1575, 3294, 2264,  239,    2],
                   [  12,  348, 2264, 4832, 2382,  240],
                   [   0,    3,  239, 2382, 4832, 2382],
                   [   0,    0,    2,  240, 2382, 4832],
                   [   0,    0,    0,    2,  240, 2382],
                   [   0,    0,    0,    0,    2,  240],
                   [   0,    0,    0,    0,    0,    2],
                   [   0,    0,    0,    0,    0,    0],
                   [   0,    0,    0,    0,    0,    0]],float)/10080/8
    I3 = np.hstack([I3,np.flipud(np.fliplr(I3[:,:-1]))])
    P, Q, I = wsplSynthesisMatrices(3,7)
    np.testing.assert_almost_equal(P.full(), P3)
    np.testing.assert_almost_equal(Q.full(), Q3)
    np.testing.assert_almost_equal(I.full(), I3)

def test_BernsteinFuncDer():
    #1st degree
    np.testing.assert_equal(BernsteinFuncDer(0, 1, 0), np.array([1, 0]))
    np.testing.assert_equal(BernsteinFuncDer(1, 1, 0), np.array([0, 1]))
    np.testing.assert_equal(BernsteinFuncDer(0, 1, 1), np.array([-1, 1]))
    
    #2st degree
    np.testing.assert_equal(BernsteinFuncDer(0.5, 2, 0), np.array([0.25,  0.5, 0.25]))
    np.testing.assert_equal(BernsteinFuncDer(0.5, 2, 1), np.array([-1, 0, 1]))

def test_SplineWaveletTransform_simple():
    for degree in xrange(1,6):
        wv = SplineWaveletTransform(degree)
        
        for knots in xrange(1,2*(degree+1)+1,2):
            #test decompose
            n = degree + 1 + knots
            c = np.array([1.]*n)
            c1, w1 = wv.decompose(c)
            np.testing.assert_almost_equal(c1, np.array([1]*((n+degree)/2)))
            np.testing.assert_almost_equal(w1, np.array([0]*((n-degree)/2)))
            
            #and then reconstruct to check the cache
            np.testing.assert_almost_equal(c, wv.reconstruct(c1,w1))

    for degree in xrange(1,6):
        wv = SplineWaveletTransform(degree)
        
        for knots in xrange(1,2*(degree+1)+1,2):
            #test reconstruct
            n = degree + knots
            c1 = np.array([1.]*n)
            w1 = np.array([0.]*(n-degree))
            c = np.array([1.]*(2*n-degree))
            np.testing.assert_almost_equal(c, wv.reconstruct(c1,w1))

            #and then decompose to check the cache
            c2,w2 = wv.decompose(c)
            np.testing.assert_almost_equal(c1,c2)
            np.testing.assert_almost_equal(w1,w2)

    #test coefs shape
    degree = 3
    wv = SplineWaveletTransform(degree)
    n = 5
    c = np.array([[[1.] * n]])
    c1, w1 = wv.decompose(c)
    np.testing.assert_almost_equal(c1, np.array([[[1.] * ((n + degree) / 2)]]))
    np.testing.assert_almost_equal(w1, np.array([[[0.] * ((n - degree) / 2)]]))
    np.testing.assert_almost_equal(c, wv.reconstruct(c1, w1))

def test_SplineWaveletTransform_simple_constrained():
    constraints = [(0,0)]
    for degree in xrange(1,6):
        wv = SplineWaveletTransform(degree, constraints)
        
        #test decompose
        c = np.array([1.]*((degree+1)+1))
        c1, w1 = wv.decompose(c)
        np.testing.assert_almost_equal(c1, np.array([1.]*(degree+1)))
        np.testing.assert_almost_equal(w1, np.array([0.]))
        
        #and then reconstruct to check the cache
        np.testing.assert_almost_equal(c, wv.reconstruct(c1,w1))

    for degree in xrange(1,6):
        wv = SplineWaveletTransform(degree, constraints)
        
        #test reconstruct
        c1 = np.array([1.]*(degree+1))
        w1 = np.array([0.])
        c = np.array([1.]*((degree+1)+1))
        np.testing.assert_almost_equal(c, wv.reconstruct(c1,w1))

        #and then decompose to check the cache
        c2,w2 = wv.decompose(c)
        np.testing.assert_almost_equal(c1,c2)
        np.testing.assert_almost_equal(w1,w2)

    #test coefs shape
    degree = 3
    wv = SplineWaveletTransform(degree, constraints)
    n = 5
    c = np.array([[[1.] * n]])
    c1, w1 = wv.decompose(c)
    np.testing.assert_almost_equal(c1, np.array([[[1.] * ((n + degree) / 2)]]))
    np.testing.assert_almost_equal(w1, np.array([[[0.] * ((n - degree) / 2)]]))
    np.testing.assert_almost_equal(c, wv.reconstruct(c1, w1))

def test_SplineWaveletTransform_decompose_reconstruct():
    
    Q = np.array([[1,-2,3,-2,1]],float).T/3
    P = np.array([[16,0,0,0],[8,8,0,0],[0,8,8,0],[0,0,8,8],[0,0,0,16]],float)/16
    c1 = np.array([1,2,1,2], float)
    w1 = np.array([10], float)
    c = np.dot(P,c1) + np.dot(Q,w1)
    #test reconstruct
    wv = SplineWaveletTransform(3) #3rd degree spline
    np.testing.assert_almost_equal(wv.reconstruct(c1,w1), c)
    
    #test decompose
    wv = SplineWaveletTransform(3) #3rd degree spline
    c2, w2 = wv.decompose(c)
    np.testing.assert_almost_equal(c1, c2)
    np.testing.assert_almost_equal(w1, w2)

    #test both
    for d in xrange(2,7):
        x = np.random.rand(d+2)
        wv = SplineWaveletTransform(d)
        np.testing.assert_almost_equal(x, wv.reconstruct(*wv.decompose(x)))

def test_SplineWaveletTransform_reconstruct_constrained():
    from scipy.interpolate import splev

    c = np.array([1,2,1,2], float)
    w = np.array([10], float)
    t = np.array([0,0,0,0,1,1,1,1], float)
    t1 = np.array([0,0,0,0,0.5,1,1,1,1])
    # positional constraint at the endpoints
    constraints = [(0,0),(0,1)] #(derivative, knot)
    wv = SplineWaveletTransform(3, constraints) #3rd degree spline
    #test reconstruct
    c1 = wv.reconstruct(c,w)
    np.testing.assert_almost_equal(splev([0,1], (t,c,3)), splev([0,1], (t1,c1,3)))
    #and then decompose to check the cache
    c2, w2 = wv.decompose(c1)
    np.testing.assert_almost_equal(splev([0,1], (t1,c1,3)), splev([0,1], (t,c2,3)))

    # derivative constraint at the endpoints
    constraints = [(1,0),(1,1)] #(derivative, knot)
    wv = SplineWaveletTransform(3, constraints) #3rd degree spline
    #test reconstruct
    c1 = wv.reconstruct(c,w)
    np.testing.assert_almost_equal(splev([0,1], (t,c,3), der=1), splev([0,1], (t1,c1,3), der=1))
    #and then decompose to check the cache
    c2, w2 = wv.decompose(c1)
    np.testing.assert_almost_equal(splev([0,1], (t1,c1,3), der=1), splev([0,1], (t,c2,3), der=1))

def test_SplineWaveletTransform_decompose_constrained():
    from scipy.interpolate import splev

    c = np.array([1,2,1,2,1], float)
    t = np.array([0,0,0,0,0.5,1,1,1,1])
    t1 = np.array([0,0,0,0,1,1,1,1], float)
    # positional constraint at the endpoints
    constraints = [(0,0),(0,1)] #(derivative, knot)
    wv = SplineWaveletTransform(3, constraints) #3rd degree spline
    #test decompose
    c1, w1 = wv.decompose(c)
    np.testing.assert_almost_equal(splev([0,1], (t,c,3)), splev([0,1], (t1,c1,3)))
    # and then reconstruct to check the cache
    np.testing.assert_almost_equal(splev([0,1], (t1,c1,3)), splev([0,1], (t,wv.reconstruct(c1,w1),3)))

    # derivative constraint at the endpoints
    constraints = [(1,0),(1,1)] #(derivative, knot)
    wv = SplineWaveletTransform(3, constraints) #3rd degree spline
    #test decompose
    c1, w1 = wv.decompose(c)
    np.testing.assert_almost_equal(splev([0,1], (t,c,3), der=1), splev([0,1], (t1,c1,3), der=1))
    # and then reconstruct to check the cache
    np.testing.assert_almost_equal(splev([0,1], (t1,c1,3), der=1), splev([0,1], (t,wv.reconstruct(c1,w1),3), der=1))

    
def test_SplineWaveletTransform_constrained():
    from scipy.interpolate import splev
    # positional constraint at the endpoints
    constraints = [(0,0),(0,1)]
    wv = SplineWaveletTransform(1, constraints)
    c0 = np.array([1.,2.,1.5])
    t0 = np.array([0.,0.,0.5,1.,1.])
    t1 = np.array([0.,0.,1.,1.])
    c1, w1 = wv.decompose(c0)
    y0 = splev([0,1],(t0,c0,1))
    y1 = splev([0,1],(t1,c1,1))
    np.testing.assert_almost_equal(y0, y1)

    constraints = [(0,0),(0,1)]
    wv = SplineWaveletTransform(1, constraints)
    c0 = np.array([1.,1.5])
    w0 = np.array([5.])
    t0 = np.array([0.,0.,1.,1.])
    t1 = np.array([0.,0.,0.5,1.,1.])
    c1 = wv.reconstruct(c0, w0)
    y0 = splev([0,1],(t0,c0,1))
    y1 = splev([0,1],(t1,c1,1))
    np.testing.assert_almost_equal(y0, y1)

    constraints = [(0,0),(0,1)]
    wv = SplineWaveletTransform(1, constraints)
    c0 = np.array([[[1.,2.,1.5],[1.5,2.,1.]],[[0.,1.,0.5],[0.5,1.,0.]]])
    t0 = np.array([0.,0.,0.5,1.,1.])
    t1 = np.array([0.,0.,1.,1.])
    c1, w1 = wv.decompose(c0)
    y0 = splev([0,1],(t0,c0,1))
    y1 = splev([0,1],(t1,c1,1))
    np.testing.assert_almost_equal(y0, y1)

    constraints = [(0,0),(0,1)]
    wv = SplineWaveletTransform(1, constraints)
    c0 = np.array([[[1.,1.5],[1.5,1.]],[[0.,0.5],[0.5,0.]]])
    w0 = np.array([[[5.],[4.]],[[3.],[2.]]])
    t0 = np.array([0.,0.,1.,1.])
    t1 = np.array([0.,0.,0.5,1.,1.])
    c1 = wv.reconstruct(c0, w0)
    y0 = splev([0,1],(t0,c0,1))
    y1 = splev([0,1],(t1,c1,1))
    np.testing.assert_almost_equal(y0, y1)

    # n = 11
    # for degree in xrange(6):
    #     for x in np.linspace(0.1, 0.9, num=9):
    #         constraints = [(0,0),(0,1),(1,0),(1,1),(0,x)]
    #         wv = SplineWaveletTransform(degree, constraints)
    #
    #         c = np.random.rand(n)
    #         t = np.hstack([[0]*degree, np.linspace(0,1,n-degree+1), [1]*degree])
    #
    #         n1 = (n+degree)/2
    #         t1 = np.hstack([[0]*degree, np.linspace(0,1,n1-degree+1), [1]*degree])
    #
    #
    #         c1, w1 = wv.decompose(c)
    #         np.testing.assert_almost_equal(splev([0,x,1], (t,c,degree)), splev([0,x,1], (t1,c1,degree)))
    #         np.testing.assert_almost_equal(splev([0,1], (t,c,degree), der=1), splev([0,1], (t1,c1,degree), der=1))
    #
    #         c2 = wv.reconstruct(c1,w1*.5)
    #         np.testing.assert_almost_equal(splev([0,x,1], (t,c,degree)), splev([0,x,1], (t, c2,degree)))
    #         np.testing.assert_almost_equal(splev([0,1], (t,c,degree), der=1), splev([0,1], (t,c2,degree), der=1))

def test_SplineWaveletTransform_direct_inverse():
    c = np.array([1,2,1,2,1], float)
    wv = SplineWaveletTransform(3) #3rd degree spline
    #test decompose
    c1, w1 = wv.decompose(c)
    splw = [c]
    d1, n = wv.direct(splw, 0)
    assert n == 0 and d1 == splw

    d1, n = wv.direct(splw)
    assert n == 1
    np.testing.assert_equal(d1[0], c1)
    np.testing.assert_equal(d1[1], w1)

    d0, n = wv.inverse(d1,0)
    assert n == 0
    np.testing.assert_almost_equal(d0[0], d1[0])
    np.testing.assert_almost_equal(d0[1], d1[1])

    d0, n = wv.inverse(d1)
    assert n == 1
    np.testing.assert_almost_equal(d0[0], c)

def test_SplineWaveletTransform_array_to_from_splw():
    a = np.arange(5)
    wv = SplineWaveletTransform(3) #3rd degree spline
    w = wv.array_to_splw(a,1)
    np.testing.assert_equal(w[0],a[:4])
    np.testing.assert_equal(w[1],a[4])

    a1, n = wv.splw_to_array(w)
    assert n == 1
    np.testing.assert_equal(a1,a)


    a = np.arange(7)
    wv = SplineWaveletTransform(3) #3rd degree spline
    w = wv.array_to_splw(a,2)
    np.testing.assert_equal(w[0],a[:4])
    np.testing.assert_equal(w[1],a[4])
    np.testing.assert_equal(w[2],a[5:7])

    a1, n = wv.splw_to_array(w)
    assert n == 2
    np.testing.assert_equal(a1,a)


    w = wv.array_to_splw(a,1)
    np.testing.assert_equal(w[0],a[:5])
    np.testing.assert_equal(w[1],a[5:7])
