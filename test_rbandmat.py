import numpy as np
import rbandmat as bm
from rbandmat import BandMat, dot, cho_solve, cholesky
import scipy

def test_BandMat_basic():
    A = BandMat(size=0)
    assert type(A) == BandMat
    assert type(A.T) == BandMat
    assert type(A.T.copy()) == BandMat
    assert type(A.full()) == np.ndarray

    A = BandMat(size=(0,0), bandwidth=1)
    assert type(A) == BandMat
    assert type(A.T) == BandMat
    assert type(A.T.copy()) == BandMat
    assert type(A.T.copy(True)) == BandMat
    assert type(A.full()) == np.ndarray

def test_BandMat_set_row():
    # test square matrices
    A = BandMat(l=1, u=2, size=6)
    A.data[:] = 0
    B = np.zeros((6,6))
    A.set_row(0,0,range(1,7))
    A.set_row(5,3,[1,2,3,4,5])
    B[0,:3] = range(1,4)
    B[5,4:] = [2,3]
    np.testing.assert_equal(B, A.full())

    # test rectangular matrices
    A = BandMat(size=(4,5), bandwidth=2)
    A.set_row(0,0,[1,2])
    A.set_row(1,1,[2,3])
    A.set_row(2,2,[3,4])
    A.set_row(3,3,[4,5,6])
    np.testing.assert_equal(A.full(),np.array([[1,2,0,0,0],
                                              [0,2,3,0,0],
                                              [0,0,3,4,0],
                                              [0,0,0,4,5]]))

    A = BandMat(size=(5,4), bandwidth=2, store_as_rows=False, transposed=True)
    A.set_row(0,0,[1,0])
    A.set_row(1,0,[2,2])
    A.set_row(2,1,[3,3])
    A.set_row(3,2,[4,4])
    A.set_row(4,3,[5,5])
    np.testing.assert_equal(A.full(),np.array([[1,2,0,0,0],
                                              [0,2,3,0,0],
                                              [0,0,3,4,0],
                                              [0,0,0,4,5]]).T)

def test_BandMat_set_column():
    # test square matrices
    A = BandMat(l=1, u=2, size=6)
    A.data[:] = 0
    B = np.zeros((6,6))
    A.set_column(0,0,range(1,7))
    A.set_column(5,2,[1,2,3,4,5])
    B[:2,0] = range(1,3)
    B[3:,5] = [2,3,4]
    np.testing.assert_equal(B,A.full())

    A = BandMat(l=2, u=1, size=6)
    A.data[:] = 0
    B = np.zeros((6,6))
    A.set_column(0,0,range(1,7))
    A.set_column(5,2,[1,2,3,4,5])
    B[:3,0] = range(1,4)
    B[4:,5] = [3,4]
    np.testing.assert_equal(B,A.full())

    # test rectangular matrices
    A = BandMat(size=(4,5), bandwidth=2, store_as_rows=False)
    A.set_column(0,0,[1,0])
    A.set_column(1,0,[2,2])
    A.set_column(2,1,[3,3])
    A.set_column(3,2,[4,4])
    A.set_column(4,3,[5,5])
    np.testing.assert_equal(A.full(),np.array([[1,2,0,0,0],
                                              [0,2,3,0,0],
                                              [0,0,3,4,0],
                                              [0,0,0,4,5]]))

    A = BandMat(size=(5,4), bandwidth=2, transposed=True)
    A.set_column(0,0,[1,2])
    A.set_column(1,1,[2,3])
    A.set_column(2,2,[3,4])
    A.set_column(3,3,[4,5,6])
    np.testing.assert_equal(A.full(),np.array([[1,2,0,0,0],
                                              [0,2,3,0,0],
                                              [0,0,3,4,0],
                                              [0,0,0,4,5]]).T)

def test_BandMat_copy():
    #test square matrix
    A = BandMat(size=3, l=1, u=1)
    A.set_row(0,0,[2,3])
    A.set_row(1,0,[1,2,3])
    A.set_row(2,1,[1,2])
    B = A.T.copy()
    np.testing.assert_equal(A.full().T, B.full())

    A = BandMat(size=3, l=1, u=0)
    A.set_row(0,0,[2])
    A.set_row(1,0,[2,3])
    A.set_row(2,1,[1,2])
    B = A.T.copy()
    np.testing.assert_equal(A.full().T, B.full())

    #test rectangular matrices
    A = BandMat(size=(4,5), bandwidth=2)
    A.set_row(0,0,[1,2])
    A.set_row(1,1,[2,3])
    A.set_row(2,2,[3,4])
    A.set_row(3,3,[4,5,6])
    B = A.T.copy()
    np.testing.assert_equal(B.full(),np.array([[1,2,0,0,0],
                                              [0,2,3,0,0],
                                              [0,0,3,4,0],
                                              [0,0,0,4,5]]).T)

    B = A.copy(switch_storage_format=True)
    np.testing.assert_equal(B.full(),np.array([[1,2,0,0,0],
                                              [0,2,3,0,0],
                                              [0,0,3,4,0],
                                              [0,0,0,4,5]]))

    A.set_row(0,0,[1,2])
    A.set_row(1,0,[2,3])
    A.set_row(2,2,[3,4])
    A.set_row(3,2,[4,5])
    B = A.T.copy()
    np.testing.assert_equal(B.full(),np.array([[1,2,0,0,0],
                                              [2,3,0,0,0],
                                              [0,0,3,4,0],
                                              [0,0,4,5,0]]).T)

    A.set_row(0,0,[1,2])
    A.set_row(1,0,[2,3])
    A.set_row(2,2,[3,4])
    A.set_row(3,2,[4,5])
    B = A.copy(switch_storage_format=True)
    np.testing.assert_equal(B.full(),np.array([[1,2,0,0,0],
                                              [2,3,0,0,0],
                                              [0,0,3,4,0],
                                              [0,0,4,5,0]]))

    A.set_row(0,1,[1,2])
    A.set_row(1,1,[2,3])
    A.set_row(2,3,[3,4])
    A.set_row(3,3,[4,5])
    B = A.T.copy()
    np.testing.assert_equal(B.full(),np.array([[0,1,2,0,0],
                                              [0,2,3,0,0],
                                              [0,0,0,3,4],
                                              [0,0,0,4,5]]).T)

    A.set_row(0,1,[1,2])
    A.set_row(1,1,[2,3])
    A.set_row(2,3,[3,4])
    A.set_row(3,3,[4,5])
    B = A.copy(switch_storage_format=True)
    np.testing.assert_equal(B.full(),np.array([[0,1,2,0,0],
                                              [0,2,3,0,0],
                                              [0,0,0,3,4],
                                              [0,0,0,4,5]]))

    A.set_row(0,0,[1,2])
    A.set_row(1,0,[2,3])
    A.set_row(2,3,[3,4])
    A.set_row(3,3,[4,5])
    B = A.T.copy()
    np.testing.assert_equal(B.full(),np.array([[1,2,0,0,0],
                                              [2,3,0,0,0],
                                              [0,0,0,3,4],
                                              [0,0,0,4,5]]).T)

    B = A.copy(switch_storage_format=True)
    np.testing.assert_equal(B.full(),np.array([[1,2,0,0,0],
                                              [2,3,0,0,0],
                                              [0,0,0,3,4],
                                              [0,0,0,4,5]]))


def test_BandMat_getitem():

    A = BandMat(size=(4,5), bandwidth=2)
    A.set_row(0,0,[1,2])
    A.set_row(1,1,[2,3])
    A.set_row(2,2,[3,4])
    A.set_row(3,3,[4,5])
    B=np.array([[1, 2, 0, 0, 0],
              [0, 2, 3, 0, 0],
              [0, 0, 3, 4, 0],
              [0, 0, 0, 4, 5]])
    # single values
    for i in xrange(A.shape[0]):
        for j in xrange(A.shape[1]):
            assert A[i,j] == B[i,j]
            assert A.T[j, i] == B[i, j]
    # row slices (only row stored matrices)
    for i in xrange(A.shape[0]):
        np.testing.assert_equal(A[i],B[i])
        np.testing.assert_equal(A[i,:],B[i])
        np.testing.assert_equal(A.T[:,i],B[i])
        np.testing.assert_equal(A[i,1:3],B[i,1:3])
        np.testing.assert_equal(A[i,2:4],B[i,2:4])
        np.testing.assert_equal(A[i,::2],B[i,::2])
        np.testing.assert_equal(A[i,1::2],B[i,1::2])
        np.testing.assert_equal(A[i,::3],B[i,::3])

    np.testing.assert_equal(A[1:3], B[1:3])
    np.testing.assert_equal(A[1:3,:], B[1:3])
    A.slicing = 'non-zero'
    np.testing.assert_equal(A[1:3], B[1:3,1:4])

    # column slices (only column stored matrices)
    A = BandMat(size=(4,5), bandwidth=2, store_as_rows=False)
    A.set_column(0,0,[1,0])
    A.set_column(1,0,[2,2])
    A.set_column(2,1,[3,3])
    A.set_column(3,2,[4,4])
    A.set_column(4,3,[5,5])
    for i in xrange(A.shape[1]):
        np.testing.assert_equal(A[:,i],B[:,i])
        np.testing.assert_equal(A.T[i,:],B[:,i])
        np.testing.assert_equal(A[1:3,i],B[1:3,i])
        np.testing.assert_equal(A[2:4,i],B[2:4,i])
        np.testing.assert_equal(A[::2,i],B[::2,i])
        np.testing.assert_equal(A[1::2,i],B[1::2,i])
        np.testing.assert_equal(A[::3,i],B[::3,i])

    np.testing.assert_equal(A[:,3:4], B[:,3:4])
    A.slicing = 'non-zero'
    np.testing.assert_equal(A[:,3:4], B[2:4,3:4])


def test_BandMat_setitem():
    #test square matrix
    A = BandMat(size=3, l=1, u=1)
    B = np.zeros((3,3))
    for i in xrange(3):
        A[i,i] = B[i,i] = i
    for i in xrange(2):
        A[i,i+1] = B[i,i+1] = -i
        A[i+1,i] = B[i+1,i] = i+5
    np.testing.assert_equal(A.full(),B)
    try:
        # OOB assignment should fail with IndexError
        A[0,2] = 1
        raise AssertionError
    except IndexError:
        pass

def test_dot():

    # square bandmat * ndarray vector => ndarray vector
    A = BandMat(size=3, l=1, u=1)
    A.set_row(0,0,[1,2])
    A.set_row(1,0,[2,3,4])
    A.set_row(2,1,[3,4])
    B = np.array([1,2,3], dtype=float)
    np.testing.assert_equal(dot(A,B), np.dot(A.full(),B))
    # ndarray vector * square bandmat => ndarray vector
    np.testing.assert_equal(dot(B.T,A.T), np.dot(B.T,A.T.full()))

    # square bandmat * ndarray matrix => ndarray matrix
    B = np.array([[1,2,3],[2,3,4]], dtype=float).T
    np.testing.assert_equal(dot(A, B), np.dot(A.full(), B))
    # ndarray matrix * square bandmat => ndarray matrix
    np.testing.assert_equal(dot(B.T, A.T), np.dot(B.T, A.T.full()))

    # non-square row bandmat * ndarray vector => ndarray vector
    A = BandMat(size=(4,5), bandwidth=2)
    A.set_row(0,0,[1,2])
    A.set_row(1,1,[2,3])
    A.set_row(2,2,[3,4])
    A.set_row(3,3,[4,5])
    B = np.array([1,2,3,4,5])
    np.testing.assert_equal(dot(A, B), np.dot(A.full(), B))
    # ndarray vector * non-square row bandmat => ndarray vector
    np.testing.assert_equal(dot(B, A.T), np.dot(B, A.T.full()))

    # non-square row bandmat * ndarray matrix => ndarray matrix
    B = np.random.random((5,2))
    np.testing.assert_equal(dot(A, B), np.dot(A.full(), B))
    # ndarray matrix * non-square row bandmat => ndarray matrix
    np.testing.assert_equal(dot(B.T, A.T), np.dot(B.T, A.T.full()))

    # non-square transposed row bandmat * ndarray vector => ndarray vector
    B = np.array([1,2,3,4])
    np.testing.assert_equal(dot(A.T, B), np.dot(A.T.full(), B))
    # ndarray vector * non-square transposed row bandmat => ndarray vector
    np.testing.assert_equal(dot(B, A), np.dot(B, A.full()))

    # non-square transposed row bandmat * ndarray matrix => ndarray matrix
    B = np.random.random((4,2))
    np.testing.assert_equal(dot(A.T, B), np.dot(A.T.full(), B))
    # ndarray matrix * non-square transposed row bandmat => ndarray matrix
    np.testing.assert_equal(dot(B.T, A), np.dot(B.T, A.full()))

    # non-square column bandmat * ndarray vector => ndarray vector
    A = BandMat(size=(4,5), bandwidth=2, store_as_rows=False)
    A.set_column(0,0,[1,0])
    A.set_column(1,0,[2,2])
    A.set_column(2,1,[3,3])
    A.set_column(3,2,[4,4])
    A.set_column(4,3,[5,5])
    B = np.array([1,2,3,4,5])
    np.testing.assert_equal(dot(A, B), np.dot(A.full(), B))
    # ndarray vector * non-square column bandmat => ndarray vector
    np.testing.assert_equal(dot(B, A.T), np.dot(B, A.T.full()))

    # non-square column bandmat * ndarray matrix => ndarray matrix
    B = np.random.random((5,2))
    np.testing.assert_equal(dot(A, B), np.dot(A.full(), B))
    # ndarray matrix * non-square column bandmat => ndarray matrix
    np.testing.assert_equal(dot(B.T, A.T), np.dot(B.T, A.T.full()))

    # ... transposed
    B = np.array([1,2,3,4])
    np.testing.assert_equal(dot(A.T, B), np.dot(A.T.full(), B))
    np.testing.assert_equal(dot(B.T, A), np.dot(B.T, A.full()))

    B = np.random.random((4,2))
    np.testing.assert_equal(dot(A.T, B), np.dot(A.T.full(), B))
    np.testing.assert_equal(dot(B.T, A), np.dot(B.T, A.full()))

    # square bandmat * square bandmat => square bandmat
    R = dot(BandMat(size=0), BandMat(size=0))
    assert type(R) == BandMat
    assert R.shape == (0,0)

    A = BandMat(size=3, l=1, u=1)
    A.set_row(0,0,[1,2])
    A.set_row(1,0,[2,3,4])
    A.set_row(2,1,[3,4])
    B = BandMat(size=3, l=1, u=0)
    B.set_row(0,0,[1])
    B.set_row(1,0,[2,3])
    B.set_row(2,1,[3,4])
    np.testing.assert_equal(dot(A, B).full(), np.dot(A.full(), B.full()))

    # non-square bandmat * square bandmat => non-square bandmat
    R = dot(BandMat(size=(0,0), bandwidth=1), BandMat(size=0))
    assert type(R) == BandMat
    assert R.shape == (0,0)
    assert R.bandwidth == 1

    A = BandMat(size=(2,4), bandwidth=2)
    A.set_row(0,0,[1,2])
    A.set_row(1,2,[2,3])
    B = BandMat(size=4, l=1, u=1)
    B.set_row(0,0,[1,2])
    B.set_row(1,0,[2,3,4])
    B.set_row(2,1,[3,4,5])
    B.set_row(3,2,[5,6])
    # non-square row bandmat * square bandmat => non-square bandmat
    np.testing.assert_equal(dot(A, B).full(), np.dot(A.full(), B.full()))
    # non-square transposed row bandmat * square bandmat => non-square bandmat
    np.testing.assert_equal(dot(A.T.copy().T, B).full(), np.dot(A.full(), B.full()))
    # non-square row bandmat * transposed square bandmat => non-square bandmat
    np.testing.assert_equal(dot(A, B.T).full(), np.dot(A.full(), B.T.full()))
    # non-square column bandmat * square bandmat => non-square bandmat
    np.testing.assert_equal(dot(A.copy(switch_storage_format=True), B).full(), np.dot(A.full(), B.full()))
    # square bandmat * non-square bandmat => non-square bandmat
    np.testing.assert_equal(dot(B.T, A.T).full(), np.dot(B.T.full(), A.T.full()))

    # non-square bandmat * non-square bandmat => square bandmat
    A = BandMat(size=(2,4), bandwidth=2)
    A.set_row(0,0,[1,2])
    A.set_row(1,2,[2,3])
    B = dot(A, A.T)
    assert B.square == True and B.u == 0 and B.l == 0 and B.size == 2
    np.testing.assert_equal(B.full(), np.dot(A.full(), A.T.full()))
    B = dot(A.T, A)
    assert B.square == True and B.u == 1 and B.l == 1 and B.size == 4
    np.testing.assert_equal(B.full(), np.dot(A.T.full(), A.full()))

    A = BandMat(size=(3,4), bandwidth = 2)
    A.set_row(0,0,[1,2])
    A.set_row(1,1,[3,4])
    A.set_row(2,3,[6])
    B = BandMat(size=(4,3), bandwidth=2)
    B.set_row(0,0,[1,2])
    B.set_row(1,1,[3,0])
    B.set_row(2,1,[3,4])
    B.set_row(3,2,[4])
    C = dot(A,B)
    assert C.square == True and C.size == 3
    np.testing.assert_equal(C.full(), np.array([[1,8,0],[0,21,16],[0,0,24]]))

    # non-square bandmat * non-square bandmat => non-square bandmat
    A = BandMat(size=(3,4), bandwidth = 2)
    A.set_row(0,0,[1,2])
    A.set_row(1,1,[3,4])
    A.set_row(2,3,[6])
    B = BandMat(size=(4,2), bandwidth=2, store_as_rows=False)
    B.set_column(0,0,[1,2])
    B.set_column(1,2,[2,3])
    C = dot(A, B)
    np.testing.assert_equal(C.full(), np.dot(A.full(), B.full()))

def test_cho_solve():
    L = BandMat(3, l=2, u=0, data=np.array([[1,2,3],[4,5,0],[6,0,0]], dtype=np.float64))
    b = np.atleast_3d(np.array([[1,2],[3,4],[5,6]], dtype=np.float64))
    x1 = cho_solve(L, b)
    x2 = cho_solve(L.full(), b)
    x3 = scipy.linalg.cho_solve((L.full(), True), b)
    np.testing.assert_almost_equal(x1, x3)
    np.testing.assert_almost_equal(x2, x3)

def test_cholesky():
    A = BandMat(size=(3,4), bandwidth = 2)
    A.set_row(0,0,[1,2])
    A.set_row(1,1,[3,4])
    A.set_row(2,3,[6])
    M = dot(A, A.T)
    Lb = cholesky(M)
    L = scipy.linalg.cholesky(M.full(), lower=True)
    np.testing.assert_almost_equal(Lb.full(), L)