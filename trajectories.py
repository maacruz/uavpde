import numpy as np
from scipy import interpolate
from scipy import integrate

import splinewavelets as sw
import utils
from populations import DefaultPopulation


def rotation_matrix3D(v, xy_plane=False):
    """
    Returns a 3D rotation matrix for a new coordinate system where the x axis has the same direction as the given vector v
    Input:
        v: float[3]
        xy_plane=True: boolean # If true rotation is confined to the xy plane
    Output:
        float[3,3]
    """
    # find the rotation in the xy plane
    v_xy = np.linalg.norm(v[[0, 1]], axis=0) # v projection length on xy
    if v_xy > 0:
        cos_phi = v[0] / v_xy
        sin_phi = v[1] / v_xy
    else: # handle the special case of vertical v
        cos_phi = 1.
        sin_phi = 0.
    R = np.array([[cos_phi, -sin_phi, 0.], # only xy plane rotation
                  [sin_phi, cos_phi, 0.],
                  [0., 0., 1.]])
    if not(xy_plane):
        # find the rotation in the x'z plane
        v_length = np.linalg.norm(v, axis=0)
        cos_theta = v_xy / v_length
        sin_theta = -v[2] / v_length
        # compose both
        R = np.dot(R,
                   np.array([[cos_theta, 0, sin_theta],
                             [0, 1, 0],
                             [-sin_theta, 0, cos_theta]]))
    return R


def _wri_wpextra_points(popsize, wp, sigma, degree):
    """
    Internal function for wavelet_random_initialization()
    Returns an array of waypoints and as many random intermediate points as necessary to fully define a population of interpolatory splines of the given degree
    Intermediate points are created with a gaussian distribution centered in the straigth segment between two waypoints and are placed in such a way as to try to divide those segments in roughly similar pieces
    Input:
        popsize: int # population size
        wp: float[3,:] # waypoints (each row a coordinate in x,y,z order, each column a waypoint)
        sigma: float[3] # standard deviation along the segment, in a horizontal line perpendicular to the segment and along the perpendicular to the plane defined by the previous two
        degree: int # spline's degree
    Output:
        tuple of
            float[3,:] # points array
            int[:] # indexes of the actual waypoints in the output array
    """
    if wp.shape[-1] > degree:
        wpextra_points = np.repeat(wp[None,...], popsize, axis=0)
        wp_idx = range(wp.shape[-1])
    else:
        # if the number of waypoints is not enough to define a interpolating spline of degree k extra points are needed
        # to generate extra points the trajectory is divided into approximately equal segments
        # and each new point is perturbated
        vlegs = np.diff(wp)  # leg column vectors
        llegs = np.linalg.norm(vlegs, axis=0)  # legs length
        # determine how many pieces each leg needs to be cut into so the length of those pieces is closer to the average length
        pieces_leg = np.floor(degree * llegs / np.sum(llegs)).astype(int)  # just distribute segments proportionally
        short_legs = pieces_leg == 0
        if np.any(short_legs):
            pieces_leg[short_legs] = 1
            not_short = np.logical_not(short_legs)
            pieces_leg[not_short] = np.floor((degree - np.count_nonzero(short_legs)) * llegs[not_short] / np.sum(llegs[not_short])).astype(int)
        sorted_idx = np.argsort(llegs - np.sum(llegs) * pieces_leg / degree)[::-1]  # allocate the remainder
        for i in xrange(degree - int(np.sum(pieces_leg))):
            pieces_leg[sorted_idx[i]] += 1

        # generate a population of control points including waypoints and extra points
        wpextra_points = np.zeros((popsize, wp.shape[0], degree + 1))
        i = 0
        for j in xrange(pieces_leg.size):
            wpextra_points[:, :, i] = wp[:, j]
            i += pieces_leg[j]
        wpextra_points[:, :, -1] = wp[:, -1]

        i = 0
        wp_idx = [i]
        for j in xrange(pieces_leg.size):
            i += 1
            R = rotation_matrix3D(vlegs[:, j])
            for c in xrange(1, int(pieces_leg[j])):
                m = wp[:, j] + c * vlegs[:, j] / pieces_leg[j]
                s2 = np.diag(
                    llegs[j] / pieces_leg[j] * sigma)**2
                cp = np.random.multivariate_normal(np.zeros(3), s2, popsize)
                wpextra_points[:, :, i] = np.dot(cp, R.T) + m
                i += 1
            wp_idx.append(i)
    return wpextra_points, wp_idx


def _wri_wavelet_scale(degree, ncoefs, u_wp, wp):
    """
    Internal function for wavelet_random_initialization()
    Returns a scale factor for each b-spline wavelet related to distance between each pair of waypoints and the weight (influence) of the wavelet in that segment
    Input:
        degree: int # b-spline degree
        ncoefs: int # number of b-spline coefs in the base trajectory
        u_wp: float[:] # waypoints parameter value
        wp: float[:,:] # waypoints (each row a coordinate in x,y,z order, each column a waypoint)
    Output:
        float[ncoefs-degree] # scale factor for each wavelet
    """
    wp_dist = np.linalg.norm(np.diff(wp, axis=1), ord=2, axis=0) # distance between each pair of waypoints
    _, Q, _ = sw.wsplSynthesisMatrices(degree, ncoefs)
    n = Q.shape[0]
    knots = np.empty(n + degree + 1) # knots in the high dimension b-spline space where the wavelet is defined
    knots[:degree] = 0 # repeated knots at the start for clamping
    knots[n+1:] = 1 # repeated knots at the end for clamping
    knots[degree:n+1] = np.linspace(0, 1, n - degree + 1) # uniformly spaced knots
    u, wp_idx = utils.ordered_insert(np.linspace(0, 1, (n - 1)*10), u_wp)
    u = np.hstack((0,u)) # add a repeated extra 0 to make the wavelet integral array the same size as the wavelet value array
    wv_scale = np.empty(Q.shape[1])
    for i in xrange(Q.shape[1]):
        tck = (knots, Q[:,i], degree) # wavelet spline in tck format
        wvint = integrate.cumtrapz(np.abs(interpolate.splev(u, tck)), x=u)
        wv_scale[i] = np.dot(wp_dist, np.diff(wvint[wp_idx])/wvint[-1])
    return wv_scale


def wavelet_random_initialization(popsize, waypoints, degree=3, wavelet_scale=1.0, wavelet_level=1, sigma=np.array([0.2,2,0.1])):
    """
    Initializes a population of trajectories passing through a list of fixed waypoints
    Each trajectory is composed by a list of points in 3D space representing points of a interpolating spline
    Input:
        popsize: int # population size
        waypoints: float[3,:] or list of tuple(float,float,float) # waypoints (x,y,z) coordinates
        degree=3: int
        wavelet_scale=1.0: float
        wavelet_level=1: int
        sigma=[0.2,2,0.1]: float[3]
    Output:
        tuple of
            tuple(float[:], float[popsize,3,:], int) # trajectories as BSplines in tck (knot, coefs, degree) format
            float[len(waypoints)] # parameter value of each waypoint on the splines
    """
    if type(waypoints) == list:
        wp = np.array(waypoints).T # waypoint coordinates (columns)
    else:
        wp = waypoints
    if wp.shape[-1] < degree+1:
        # if there are not enough waypoints to fully define an interpolating b-spline of the given degree, insert enough random points
        interp_control_points, wp_idx = _wri_wpextra_points(popsize, wp, sigma, degree)
        # population of interpolating b-splines
        tck, u = sw.splmake(interp_control_points, degree)
        scalingcoefs = tck[1]
        u_wp = u[wp_idx]
    else:
        # if there are enough waypoints, construct one base spline repeated to fill the population
        tck, u = sw.splmake(wp, degree)
        scalingcoefs = np.repeat(tck[1][None,...], popsize, 0)
        u_wp = u
    # increase dimensionality using wavelets
    knots = tck[0]
    constraints = [(0, u_i) for u_i in u_wp]
    SWT = sw.SplineWaveletTransform(degree, constraints)
    for i in xrange(1, wavelet_level+1):
        wv_local_scale = _wri_wavelet_scale(degree, scalingcoefs.shape[-1], u_wp, wp)
        detailcoefs = (1./i)*wavelet_scale*wv_local_scale*np.random.normal(0., 0.5, (popsize, wp.shape[0], scalingcoefs.shape[-1] - degree ))
        scalingcoefs = SWT.reconstruct(scalingcoefs, detailcoefs)
        knots = sw.double_knots(knots)
    return (knots, scalingcoefs, degree), u_wp


def legacyUAV_get_npoints(wp):
    """
    Return a list of parameters to generate random points between each pair of waypoints
    Input:
        wp: float[:,:] # waypoint coordinates, each column is a waypoint
    Output:
        tuple of
            list of tuple of
                int     # number 'n' of points to generate
                float   # lower limit for the random radius 'r_min' from the previous waypoint
                float   # upper limit for the random radius 'r_max' from the previous waypoint
                float   # lower limit for the random angle displacements 'theta_min' from the direction at the previous waypoint
                float   # upper limit for the random angle displacements 'theta_max' from the direction at the previous waypoint
            list of int # list of indexes of wp
    """
    delta_theta = np.pi/5
    wp_idx_list = [0] # list of indexes of wp in the resulting control point array
    nw = [] # points to generate between wp
    for i in xrange(wp.shape[-1] - 1):
        wp_vec = wp[:,i + 1] - wp[:,i]
        wp_dist = np.sqrt(np.sum(wp_vec**2))
        wp_theta = np.arctan2(wp_vec[1], wp_vec[0])
        if wp_dist < 90000:
            n = 1 if wp.shape[-1] > 2 else 2
            d = wp_dist/2
        elif wp_dist <= 160000:
            n = 2
            d = wp_dist/2
        elif wp_dist <= 240000:
            n = int(np.round(wp_dist/80000))
            d = 80000.
        else:
            n = int(np.round(wp_dist/120000))
            d = 120000.
        wp_idx_list.append(wp_idx_list[-1] + n + 1)
        nw.append((n, d/4, d*1.2, -delta_theta, delta_theta))
    return nw, wp_idx_list


def legacyUAV_random_initialization(popsize, waypoints, limits):
    """
    Initializes a population of trajectories passing through a list of fixed wp within a limiting box
    Each trajectory is defined by a list of points in 3D space representing points of an interpolating spline
    Input:
        popsize: int # population size
        waypoints: float[3,:] or list of tuple(float,float,float) # waypoint (x,y,z) coordinates
        limits: [(float, float), (float, float), (float, float)] # list of 3 tuples of minimum and maximum possible values for each of x, y, z generated points
    Output:
        tuple of
            np.array((popsize,3,N), dtype=float) # array of popsize trajectories of (x, y, z) coordinates composed of N points,
            list of int # indexes (3rd index, from 0 to N-1) of the waypoint coordinates in the array of trajectories
    """
    # generate a list of points to generate between wp as
    if type(waypoints) == list:
        wp = np.array(waypoints).T
    else:
        wp = waypoints
    nw, wp_idx_list = legacyUAV_get_npoints(wp)
    npoints = wp.shape[-1] + sum(p[0] for p in nw)
    # generate random r-theta relative displacements on the xy plane
    # the shape of the matrix is population popsize, number of coordinates, number of points in the trajectory
    rtheta = np.random.random((popsize, 3, npoints))
    i = 1
    for n, r_min, r_max, theta_min, theta_max in nw:
        rtheta[:,0,i:i+n] = rtheta[:,0,i:i+n]*(r_max-r_min) + r_min
        rtheta[:,1,i:i+n] = rtheta[:,1,i:i+n]*(theta_max-theta_min) + theta_min
        i += n+1 # move index the number of points generated plus the next waypoint
    # generate absolute z coordinates
    z_min, z_max = limits[2]
    rtheta[:,2,:] = rtheta[:,2,:]*(z_max-z_min) + z_min
    # transform to cartesian xyz
    control_points = rtheta2cartesian(rtheta, wp_idx_list, wp, limits)
    return control_points, wp_idx_list


def rtheta2cartesian(points, wp_idx, wp, limits=None, starting_orientation=None):
    assert points.ndim == 3
    D = points.shape[1] # embedding space dimensionality
    if limits is not None:
        x_min, x_max = limits[0]
        y_min, y_max = limits[1]
    # initialize previous coordinates to first waypoint
    x = np.repeat(float(wp[0, 0]), len(points))
    y = np.repeat(float(wp[1, 0]), len(points))
    if starting_orientation:
        theta = np.repeat(float(starting_orientation), len(points))
    else:
        theta = np.copy(points[:,1,0])
    next_wp = 1 # waypoint counter
    # initialize first point to first waypoint
    for j in range(D):
        points[:,j,0] = wp[j, 0]
    # loop over all points in the trajectory
    for i in xrange(1, points.shape[-1]):
        if i == wp_idx[next_wp]:
            # if the current point is a waypoint copy its coordinates and reset previous coordinates and orientation
            for j in range(D):
                points[:,j,i] = wp[j, next_wp]
            theta = np.arctan2(wp[1, next_wp] - y, wp[0, next_wp] - x)
            x = np.repeat(float(wp[0, next_wp]), len(points))
            y = np.repeat(float(wp[1, next_wp]), len(points))
            next_wp += 1 # increase waypoint counter
        else:
            # if the current point is not a waypoint
            r = points[:,0,i] # get the current distance to jump
            theta += points[:,1,i] # get the current orientation
            x += r*np.cos(theta)
            y += r*np.sin(theta)
            # check against coordinate range limits
            if limits is not None:
                np.clip(x, x_min, x_max, x)
                np.clip(y, y_min, y_max, y)
            # modify in place
            points[:,0,i] = x
            points[:,1,i] = y
    return points



class TrajectoryPopulationISplines(DefaultPopulation):
    """
    Class implementing a population of trajectories codified as interpolating splines
    """
    def __init__(self, uav_objective, waypoints, alpha=0, degree=3):
        """
        Input:
            uav_objective: UAVObjective()
            alpha=0: float # alpha parameterization of the interpolating splines (alpha=0 -> uniform, alpha=1 -> chordal, ....)
            degree=3: int # spline degree
        """
        super(TrajectoryPopulationISplines, self).__init__(uav_objective.objective_function, uav_objective.nobjectives, 0)
        self.degree = degree
        self.alpha = alpha
        self.waypoints = waypoints
        self.wp_idx_list = []


    def repair(self):
        pass


    def random_initialize(self, popsize, limits):
        control_points, wp_idx_list = legacyUAV_random_initialization(popsize, self.waypoints, limits)
        self.init_fromdata(control_points, wp_idx_list)


    def init_fromdata(self, control_points, wp_idx_list):
        """
        Initialize a population from the given array
        Input:
            control_points: float[:,:,:] # population array, first index addresses the individual, 2nd index coordinate, 3rd index point
            wp_idx_list: list of int # list of waypoint indexes (3rd index)
        """
        assert control_points.shape[-1] > self.degree
        self.size = control_points.shape[0]
        self.space_dimension = control_points.shape[1]
        self.vector_dimension = np.prod(control_points.shape[1:])
        self.individuals = control_points.reshape((self.size, self.vector_dimension))
        self.wp_idx_list = wp_idx_list
        self.valid_objective_values = False


    def _call_objective_function(self):
        return self.objective_function(self)


    def discretize(self, i, discretization_d=1000.0):
        """
        Returns a discretized trajectory. The number of points deppends on trajectory length
        Input:
            i: int # individual trayectory index in the population
            discretization_d: float # distance on the curve between discretized points
        Output:
            tuple of
                float[:,:], # discretized trajectory where the rows are the coordinates, and the columns are the discrete points
                list of int # list of waypoint indexes in the discretized trajectory
        """
        #Create an interpolating spline (s=0) with the selected parameterization
        control_points = self.individuals[i].reshape((self.space_dimension,-1))
        if self.alpha == 1: #chordal
            tck, u = interpolate.splprep(control_points, s=0)
        elif self.alpha == 0: #uniform
            u = np.linspace(0,1,control_points.shape[-1])
            tck, u = interpolate.splprep(control_points, u=u, s=0)
        else: #alpha
            u = np.zeros(control_points.shape[-1])
            u[1:] = np.cumsum(np.sqrt(np.sum(np.diff(control_points)**2, 0))**self.alpha)
            u[:] /= u[-1]
            tck, u = interpolate.splprep(control_points, u=u, s=0)
        discretized_trajectory, wp_idx_list = sw.spldiscretize(tck, discretization_d, u[self.wp_idx_list])
        return discretized_trajectory, wp_idx_list



class TrajectoryPopulationBSplines(DefaultPopulation):
    """
    Class implementing a population of trajectories codified as uniform B-splines
    """
    def __init__(self, uav_objective, waypoints, degree=3, init_algo='legacy'):
        """
        Input:
            uav_objective: UAVObjective()
            waypoints: float[:,:] # waypoints coordinates, each column is a waypoint
            degree=3: int # spline degree
            init_algo='legacy': string # random initialization algoritm ('legacy' or 'wavelet')
        """
        super(TrajectoryPopulationBSplines, self).__init__(uav_objective.objective_function, uav_objective.nobjectives, 0)
        # transform the population of control points into interpolating uniform B-splines
        self.degree = degree
        self.init_algo = init_algo
        if type(waypoints) == list:
            self.wp_coords = np.array(waypoints).T
        else:
            self.wp_coords = waypoints


    def repair(self):
        """
        Move the B-spline so it keeps passing through waypoints
        Control points are displaced keeping the sum of squared displacements minimal. Only degree+1 control points are affected
        """
        wp_coords = self.wp_coords[:,1:-1] # all the waypoints except the first and last one
        if len(self.u_wp) > 2: # if there are any intermediate waypoints besides start and end
            for i in xrange(self.size):
                control_points = self.individuals[i].reshape((self.space_dimension,-1))
                # build tck tuple
                tck = (self.knots, control_points, self.degree)
                # compute the trajectory coords at the waypoint parameter value
                curve_wp_coords = np.array(interpolate.splev(self.u_wp[1:-1], tck))
                # compute error vector
                delta = wp_coords - curve_wp_coords
                delta[np.abs(delta) < 1e-6] = 0 # avoid FP errors
                # for each coordinate
                for j in xrange(self.space_dimension):
                    # compute and apply correction
                    correction = np.dot(self.A, delta[j])
                    control_points[j] += correction


    def random_initialize(self, popsize, *args, **kwargs):
        """
        Initialize a population of random trajectories that go through the waypoints using the algorithm selected at initialization
        See self.random_wavelet_init() and self.random_legacy_init()
        Input:
            popsize: int # number of individuals to initialize
            *args
            **kwargs
        """
        if self.init_algo.startswith('wavelet'):
            self.random_wavelet_init(popsize, *args, **kwargs)
        else:
            self.random_legacy_init(popsize, *args, **kwargs)
        # setup basis spline function values at waypoints (fixed position constraints) matrix


    def random_wavelet_init(self, popsize, *args, **kwargs):
        """
        Initialize a population of random trajectories that go through the waypoints using the wavelet algorithm
        Input:
            popsize: int # number of individuals to initialize
            *args
            **kwargs
        """
        n = sum(p[0] for p in legacyUAV_get_npoints(self.wp_coords)[0]) + self.wp_coords.shape[-1] # number of interpolating points generated by the legacy algorithm
        m = max(self.degree+1, self.wp_coords.shape[-1]) # number of points at wavelet_level 0
        wavelet_level = kwargs['wavelet_level'] if kwargs.has_key('wavelet_level')\
            else args[0] if args \
            else max(1, int(round(np.log2(float(n) / m))))
        tck, u_wp = wavelet_random_initialization(popsize, self.wp_coords, degree=self.degree, wavelet_level=wavelet_level, **kwargs)
        self.init_fromdata(tck, u_wp)


    def random_legacy_init(self, popsize, limits):
        """
        Input:
            popsize: int # number of individuals to initialize
            limits: [(float, float), (float, float), (float, float)] # list of 3 tuples of minimum and maximum possible values for each of x, y, z generated points
        """
        interp_points, fixed_wp_idx = legacyUAV_random_initialization(popsize, self.wp_coords, limits)
        self.init_fromdatapoints(interp_points, fixed_wp_idx)


    def init_fromdatapoints(self, interp_points, fixed_wp_idx):
        """
        Initialize a population interpolating the points from the given array
        Input:
            control_points: float[:,:,:] # point array, first index addresses the individual, 2nd index coordinate, 3rd index point
            wp_idx_list: list of int # list of waypoint indexes (3rd index)
        """
        if interp_points.shape[-1] <= self.degree:
            raise Exception("Not enough points to initialize a %s degree spline" % self.degree)
        assert np.all(interp_points[...,fixed_wp_idx] - self.wp_coords < 1e-6)
        # build uniform interpolating B-splines using universal parameterization method
        tck, u = sw.splmake(interp_points, self.degree)
        self.init_fromdata(tck, u[fixed_wp_idx])


    def init_fromdata(self, tck, u_wp):
        """
        Initialize a population of trajectories from the given n dimensional coordinates B-splines
        Input:
            tck: (float[:], float[:,:,:], int) #  B-Splines
            u_wp: float[:] or list of float # parameter values where the waypoints are
        """
        self.degree = tck[2]
        self.size = tck[1].shape[0]
        self.space_dimension = tck[1].shape[1]
        self.knots = tck[0] # knot vector
        self.individuals = tck[1].reshape((self.size,-1)) # store nD spline as 1D vector
        self.vector_dimension = self.individuals.shape[1]
        self.u_wp = np.array(u_wp) # parameter value at waypoints
        self._set_correction_matrix()
        self.valid_objective_values = False


    def _set_correction_matrix(self):
        """
        Compute correction matrix
        Must be called after population initialization
        """
        n_wp = self.wp_coords.shape[-1]
        self.A = np.array([[]])
        if n_wp > 2:
            B = np.zeros((n_wp-2, len(self.knots)-(self.degree+1)))
            for i in xrange(n_wp-2):
                base_knot, B_i = sw.splBasis(self.degree, len(self.knots)-(self.degree+1), self.u_wp[i+1])
                B[i,base_knot:base_knot+self.degree+1] = B_i
            self.A = np.linalg.pinv(B)


    def _call_objective_function(self):
        """
        Call the objective function set at initialization with appropriate arguments and return its result
        """
        return self.objective_function(self)


    def discretize(self, i, discretization_d=1000.0):
        """
        Returns a discretized trajectory
        A discretized trajectory is a 2D array where the rows are the coordinates, and the columns are the discrete points.
        The number of points varies with trajectory length
        Input:
          i: int # individual trayectory index in the population
          discretization_d: float # distance on the curve between discretized points
        Output:
          (float[:,:],list of int) #(discretized trajectory, list of waypoint indexes in the discretized trajectory)
        """
        control_points = self.individuals[i].reshape((self.space_dimension,-1))
        # build tck tuple
        tck = (self.knots, control_points, self.degree)
        discretized_trajectory, wp_idx_list = sw.spldiscretize(tck, discretization_d, self.u_wp)
        return discretized_trajectory, wp_idx_list

    # doubles the trajectory resolution
    def refine(self):
        new_spline = np.empty((self.size, self.vector_dimension*2 - self.space_dimension*self.degree))
        for i in xrange(self.size):
            tck = sw.splrefine((self.knots, self.individuals[i].reshape((self.space_dimension,-1)), self.degree))
            new_spline[i] = np.concatenate(tck[1])
        self.individuals = new_spline
        self.knots = tck[0]
        self.vector_dimension = self.individuals.shape[-1]
        #self.fixed_wp_idx = [2*i for i in self.fixed_wp_idx]




class TrajectoryPopulationWBSplines(DefaultPopulation):
    """
    Class implementing a population of trajectories codified as uniform B-splines
    """

    def __init__(self, uav_objective, waypoints, degree=3):
        """
        Input:
            uav_objective: UAVObjective()
            waypoints: float[:,:] # waypoints coordinates, each column is a waypoint
            degree=3: int # spline degree
        """
        super(TrajectoryPopulationWBSplines, self).__init__(uav_objective.objective_function, uav_objective.nobjectives, 0)
        # transform the population of control points into interpolating uniform B-splines
        self.degree = degree
        if type(waypoints) == list:
            self.wp_coords = np.array(waypoints).T
        else:
            self.wp_coords = waypoints
        self.nscalingcoefs = 0
        self.maxlevel = 0 # maximum detail level
        self.minlevel = 0


    def repair(self):
        """
        Move the B-spline so it keeps passing through waypoints
        Control points are displaced keeping the sum of squared displacements minimal. Only degree+1 control points are affected at the lowest resolution level
        """
        d = self.individuals[:, 0] # get a view of the fractional dimension
        # fix fractional dimension
        d_idx = d < 0
        d[d_idx] *= -1
        d_idx = d > self.maxlevel
        d[d_idx] = 2*self.maxlevel - d[d_idx]
        np.clip(d, 0, self.maxlevel, d)
        wp_coords = self.wp_coords[:, 1:-1]  # all the waypoints except the first and last one
        if len(self.u_wp) > 2:  # if there are any intermediate waypoints besides start and end
            knots = sw.uniform_knot_array(self.nscalingcoefs, self.degree)  # knot vector
            for i in xrange(self.size):
                control_points = self.individuals[i][1:].reshape((self.space_dimension, -1))[..., :self.nscalingcoefs] # get a view of the lowest resolution coefficients
                # build tck tuple
                tck = (knots, control_points, self.degree)
                # compute the trajectory coords at the waypoint parameter value
                curve_wp_coords = np.array(interpolate.splev(self.u_wp[1:-1], tck))
                # compute error vector
                delta = wp_coords - curve_wp_coords
                delta[np.abs(delta) < 1e-6] = 0  # avoid FP errors
                # for each coordinate
                for j in xrange(self.space_dimension):
                    # compute and apply correction
                    correction = np.dot(self.A, delta[j])
                    control_points[j] += correction


    def random_initialize(self, popsize, *args, **kwargs):
        """
        Initialize a population of random trajectories that go through the waypoints using the algorithm selected at initialization
        See self.random_wavelet_init() and self.random_legacy_init()
        Input:
            popsize: int # number of individuals to initialize
            *args
            **kwargs
        """
        n = sum(p[0] for p in legacyUAV_get_npoints(self.wp_coords)[0]) + self.wp_coords.shape[-1]  # number of interpolating points generated by the legacy algorithm
        m = max(self.degree + 1, self.wp_coords.shape[-1])  # number of points at wavelet_level 0
        self.maxlevel = kwargs['wavelet_level'] if kwargs.has_key('wavelet_level') \
            else args[0] if args \
            else max(1, int(round(np.log2(float(n) / m)))) + 1 # add 1 to allow one level of effective dimension
        tck, u_wp = wavelet_random_initialization(popsize, self.wp_coords, degree=self.degree, wavelet_level=self.maxlevel, **kwargs)
        d = np.random.random(popsize)*self.maxlevel
        self.init_fromdata(d, tck, u_wp)


    def init_fromdata(self, d, tck, u_wp):
        """
        Initialize a population of trajectories from the given n dimensional B-splines
        Input:
            d: float[:] # fractional dimensionality
            tck: (float[:], float[:,:,:], int) #  B-splines
            u_wp: float[:] or list of float # parameter values where the waypoints are
        """
        self.size = tck[1].shape[0]
        self.space_dimension = tck[1].shape[1]
        self.degree = tck[2]
        self.swt = sw.SplineWaveletTransform(self.degree, constraints=[(0, u) for u in u_wp])
        splw, nlevels = self.swt.direct(self.swt.array_to_splw(tck[1]))
        self.maxlevel = nlevels
        splw, currentlevel = self.swt.inverse(splw, self.minlevel)
        self.currentlevel = currentlevel
        splwarray, _ = self.swt.splw_to_array(splw)
        self.nscalingcoefs = splw[0].shape[-1]
        self.individuals = np.hstack((d[None,...], splwarray.reshape((self.size, -1))))  # store nD spline as 1D vector
        self.vector_dimension = self.individuals.shape[1]
        self.u_wp = np.array(u_wp)  # parameter value at waypoints
        self._set_correction_matrix()
        self.valid_objective_values = False


    def _set_correction_matrix(self):
        """
        Compute correction matrix
        Must be called after population initialization
        """
        n_wp = self.wp_coords.shape[-1]
        self.A = np.array([[]])
        if n_wp > 2:
            B = np.zeros((n_wp - 2, self.nscalingcoefs))
            for i in xrange(n_wp - 2):
                base_knot, B_i = sw.splBasis(self.degree, self.nscalingcoefs, self.u_wp[i + 1])
                B[i, base_knot:base_knot + self.degree + 1] = B_i
            self.A = np.linalg.pinv(B)


    def _call_objective_function(self):
        """
        Call the objective function set at initialization with appropriate arguments and return its result
        """
        return self.objective_function(self)


    def discretize(self, i, discretization_d=1000.0):
        """
        Returns a discretized trajectory
        A discretized trajectory is a 2D array where the rows are the coordinates, and the columns are the discrete points.
        The number of points varies with trajectory length
        Input:
          i: int # individual trayectory index in the population
          discretization_d: float # distance on the curve between discretized points
        Output:
          (float[:,:],list of int) #(discretized trajectory, list of waypoint indexes in the discretized trajectory)
        """
        d = self.individuals[i, 0]
        splw = self.swt.array_to_splw(self.individuals[i, 1:].reshape((self.space_dimension, -1)), self.maxlevel - self.currentlevel)
        splw = splw[:int(np.ceil(d)) + 1] # cut wavelet coefs over the effective dimension
        if d > int(d):
            splw[-1] *= d - int(d)
        control_points, _ = self.swt.splw_to_array(self.swt.inverse(splw)[0])
        # build tck tuple
        knots = sw.uniform_knot_array(control_points.shape[-1], self.degree)
        tck = (knots, control_points, self.degree)
        discretized_trajectory, wp_idx_list = sw.spldiscretize(tck, discretization_d, self.u_wp)
        return discretized_trajectory, wp_idx_list

    # doubles the trajectory resolution
    def refine(self):
        new_spline = np.empty((self.size, self.vector_dimension * 2 - self.space_dimension * self.degree))
        for i in xrange(self.size):
            tck = sw.splrefine((self.knots, self.individuals[i].reshape((self.space_dimension, -1)), self.degree))
            new_spline[i] = np.concatenate(tck[1])
        self.individuals = new_spline
        self.knots = tck[0]
        self.vector_dimension = self.individuals.shape[-1]
        # self.fixed_wp_idx = [2*i for i in self.fixed_wp_idx]



