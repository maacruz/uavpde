import numpy as np
import differentialevolution as de
from differentialevolution import *
import mock

def mock_randint0(s, e=None, size=None):
    if e is None:
        e = s
        s = 0
    if size is None:
        return s
    return s*np.ones(size, dtype=int)

class MockRandinti(object):
    def __init__(self):
        self.c = 0
    def mock_randinti(self, e):
        r = self.c % e
        self.c += 1
        return r

def mock_choicei(a, n):
    i = np.arange(n) % len(a)
    return a[i]

def mock_binomial0(n,p,size):
    return np.zeros(size, dtype=int)

def mock_binomial1(n,p,size):
    return np.ones(size, dtype=int)

def mock_normal1(m,s,size):
    return m+s*np.ones(size)

def mock_multinomialmax(ns, p, n=None):
    m = (p == p.max())
    r = np.zeros_like(p, dtype=int)
    r[m.argmax()] = 1
    if n is not None:
        return np.repeat(r[None, ...], n, axis = 0)
    else:
        return r

def mock_rand0(s=0., e=1., size=None):
    if isinstance(s, np.ndarray):
        assert size is None
        size = s.size
    if size is None:
        return s
    return s * np.ones(size, dtype=int)

def mock_cauchymax(loc, scale, low, high, size=None):
    if isinstance(loc, np.ndarray):
        return loc
    elif size is None:
            return loc
    return loc * np.ones(size)

@mock.patch('differentialevolution.cauchy_sample')
@mock.patch('differentialevolution.np.random.uniform')
@mock.patch('differentialevolution.np.random.multinomial')
def test_FPdf(mock_multinomial, mock_uniform, mock_cauchy):
    mock_multinomial.side_effect = mock_multinomialmax

    mock_uniform.side_effect = mock_rand0
    F = FPdf('DE/whatever', F_min=0.1, learning_rate=0.5)
    np.testing.assert_almost_equal(F.sample(2), np.array([[0.1,0.1]]).T)
    F.update(np.array([[0.6]]))
    np.testing.assert_equal(F.sample(1), np.array([[0.1]]))

    mock_cauchy.side_effect = mock_cauchymax
    F = FPdf('JADE/whatever', learning_rate=0.5)
    np.testing.assert_almost_equal(F.sample(2), np.array([[0.55,0.55]]).T)
    F.update(np.array([[0.65]]))
    np.testing.assert_almost_equal(F.sample(1), np.array([[0.6]]))

    F = FPdf('SHADE/whatever', learning_rate=0.5)
    np.testing.assert_almost_equal(F.sample(2), np.array([[0.55,0.55]]).T)
    F.update(np.array([[0.65]]))
    np.testing.assert_almost_equal(F.sample(1), np.array([[0.65]]))

    F = FPdf('MADE/whatever', F_max=0.9, learning_rate=0.5)
    np.testing.assert_equal(F.sample(2), np.array([[0.1,0.1]]).T)
    F.update(np.array([[0.6]]))
    np.testing.assert_equal(F.sample(1), np.array([[0.6]]))

    F = FPdf('MADE/whatever', F_min=0.34, F_max=1., learning_rate=0.5, conv_kernel_scale=0.0, nbins=3)
    F.update(np.array([[0.75]]))
    mbins = np.array([0., 0.5/2, (0.5+1.)/2]) # check multinomial update
    np.testing.assert_almost_equal(F.mbins, mbins, 4)

    F = FPdf('MADE/whatever', F_min=0.34, F_max=1., learning_rate=0.5, conv_kernel_scale=0.0, nbins=3)
    F.update(np.array([[0.2],[0.75]]), w_Sf=np.array([0.,1.]))
    mbins = np.array([0., 0.5/2, (0.5+1.)/2]) # check weighted update
    np.testing.assert_almost_equal(F.mbins, mbins, 4)

    F = FPdf('MSHADE/whatever', learning_rate=0.5)
    np.testing.assert_equal(F.sample(2), np.array([[0.55,0.55]]).T)
    F.update(np.array([[0.65]]))
    np.testing.assert_equal(F.sample(1), np.array([[0.6]]))

    F = FPdf('MSHADE/whatever', learning_rate=0.5)
    F.update(np.array([[0.2], [0.65]]), w_Sf=np.array([0.,1.])) # check weighted update
    np.testing.assert_equal(F.sample(1), np.array([[0.6]]))


@mock.patch('differentialevolution.np.random.multinomial')
def test_KPdf(mock_multinomial):
    mock_multinomial.side_effect = mock_multinomialmax

    K = KPdf('JADE/current-rand-to-pbest', learning_rate=0.5, conv_kernel_scale=0.0, nbins=4)
    assert K._bin_idx(Kr=0,Kb=1) == 0
    assert K._bin_idx(Kr=0,Kb=0) == 6
    assert K._bin_idx(Kr=1,Kb=0) == 9

    F = np.array([[0.333],[0.]])
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kb, np.array([[0.333],[0.]]))
    np.testing.assert_equal(Kr, np.array([[0.],[0.]]))
    K.update(Kb[0], Kb[0])
    F = np.array([[0.333]])
    Kr, Kb = K.sample(F)
    np.testing.assert_almost_equal(Kr, np.array([[0.333]]), 3)
    np.testing.assert_almost_equal(K.mbins[K._bin_idx(Kb, Kb)], (0.333+1)/2, 3) # check 1D multinomial update

    K = KPdf('JADE/current-rand-to-pbest', learning_rate=0.5, conv_kernel_scale=0.0, nbins=4)
    K.update(Kb, Kb, w_Sk=np.array([1.,0.]))
    F = np.array([[0.333]])
    Kr, Kb = K.sample(F)
    np.testing.assert_almost_equal(Kr, np.array([[0.333]]), 3)
    np.testing.assert_almost_equal(K.mbins[K._bin_idx(Kb, Kb)], (0.333+1)/2, 3) # check 1D multinomial update

    F = np.array([[0.4], [0.5]])
    K = KPdf('JADE/current-to-rand')
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, F)
    np.testing.assert_equal(Kb, [[0.], [0.]])
    K.update(F, F) # no effect
    K = KPdf('JADE/current-to-pbest')
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, [[0.], [0.]])
    np.testing.assert_equal(Kb, F)
    K = KPdf('JADE/rand-to-pbest')
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, 1 - F)
    np.testing.assert_equal(Kb, F)
    K = KPdf('DE/rand')
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, [[1.],[1.]])
    np.testing.assert_equal(Kb, [[0.],[0.]])
    K = KPdf('DE/best')
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, [[0.],[0.]])
    np.testing.assert_equal(Kb, [[1.],[1.]])
    K = KPdf('DE/current-to-rand', K_min=0.1, K_max=0.9)
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, [[0.1],[0.1]])
    np.testing.assert_equal(Kb, [[0.],[0.]])
    K = KPdf('DE/current-to-pbest', K_min=0.1, K_max=0.9)
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, [[0.],[0.]])
    np.testing.assert_equal(Kb, [[0.9],[0.9]])
    K = KPdf('DE/rand-to-pbest', K_min=0.1, K_max=0.9)
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, [[0.1],[0.1]])
    np.testing.assert_equal(Kb, [[0.9],[0.9]])

    K = KPdf('MADE/universal', nbins=21)
    Kr, Kb = K.sample(F)
    np.testing.assert_equal(Kr, [[0.],[0.]])
    np.testing.assert_equal(Kb, [[1.],[1.]])
    Kr = np.array([[0.5]])
    Kb = np.array([[0.2]])
    K.update(Kr, Kb)
    Kr, Kb = K.sample(F)
    np.testing.assert_almost_equal(Kr, [[0.5],[0.5]])
    np.testing.assert_almost_equal(Kb, [[0.2],[0.2]])

    K = KPdf('MADE/universal', nbins=21)
    Kr = np.array([[0.2],[0.5]])
    Kb = np.array([[0.8],[0.2]])
    K.update(Kr, Kb, w_Sk=np.array([0.,1.]))
    Kr, Kb = K.sample(F)
    np.testing.assert_almost_equal(Kr, [[0.5],[0.5]])
    np.testing.assert_almost_equal(Kb, [[0.2],[0.2]])


@mock.patch('differentialevolution.np.random.multinomial')
@mock.patch('differentialevolution.np.random.normal')
def test_CrPdf(mock_normal, mock_multinomial):
    mock_normal.side_effect = mock_normal1

    Cr = CrPdf('DE/whatever', muCr=0.4, sigmaCr=0.0)
    np.testing.assert_equal(Cr.sample(2), np.array([0.4, 0.4]))

    Cr = CrPdf('JADE/whatever', muCr=0.4, sigmaCr=0.0, learning_rate=0.1)
    np.testing.assert_equal(Cr.sample(2), np.array([0.4, 0.4]))

    Cr.sigmaCr = 0.1
    np.testing.assert_almost_equal(Cr.sample(2), np.array([0.5, 0.5]))

    Cr.update(np.array([0.5, 0.6]))
    assert Cr.muCr == 0.1*0.55 + 0.9*0.4

    mock_multinomial.side_effect = mock_multinomialmax
    Cr = CrPdf('SHADE/whatever', muCr=0.4, sigmaCr=0.0, learning_rate=0.1)
    np.testing.assert_equal(Cr.sample(2), np.array([0.4, 0.4]))
    Cr.update(np.array([0.5, 0.6]), w_Scr=np.array([0.,1.]))
    assert Cr.SHM_i == 1
    assert Cr.SHM.size == int(round((1-0.1)/0.1)) + 1
    np.testing.assert_equal(Cr.SHM[:2], [0.6, 0.4])
    np.testing.assert_equal(Cr.sample(1), np.array([0.6]))


    Cr = CrPdf('MSHADE/whatever', muCr=0.4, sigmaCr=0.0, learning_rate=2)
    np.testing.assert_equal(Cr.sample(2), np.array([0.4, 0.4]))
    Cr.update(Scr=np.array([0.5, 0.6]), w_Scr=np.array([0.5,0.5]))
    np.testing.assert_equal(Cr.sample(1), np.array([0.55]))


    Cr = CrPdf('MADE/whatever', muCr=0.4, sigmaCr=0.0, learning_rate=2, conv_kernel_scale=0.)
    np.testing.assert_equal(Cr.sample(2), np.array([0.4, 0.4]))
    Cr.update(Scr=np.array([0.5, 0.6]), w_Scr=np.array([0.5,0.5]))
    np.testing.assert_equal(Cr.sample(1), np.array([0.5]))


@mock.patch('differentialevolution.np.random.randint')
def test_sample_vectors(mock_randint):
    xi = np.array([[1,1]])
    xa = np.array([[2,2]])
    mock_randint.side_effect = mock_randint0
    xr1, xr2, xr3 = sample_3vectors(xi, xa)
    assert len(xr1) == len(xr2) == len(xr3) == len(xi)
    np.testing.assert_equal(xr1[0], [1, 1])
    np.testing.assert_equal(xr2[0], [1, 1])
    np.testing.assert_equal(xr3[0], [2, 2])

    xi = np.array([[1,1],[2,2],[3,3],[4,4],[5,5]])
    xa = np.array([[6,6]])
    mock_randint.side_effect = mock_randint0
    xr1, xr2, xr3 = sample_3vectors(xi, xa)
    assert len(xr1) == len(xr2) == len(xr3) == len(xi)
    np.testing.assert_equal(xr1[0],[2,2])
    np.testing.assert_equal(xr2[0],[3,3])
    np.testing.assert_equal(xr3[0],[4,4])


@mock.patch('differentialevolution.np.random.choice')
@mock.patch('differentialevolution.np.random.randint')
def test_sample_pbest(mock_randint, mock_choice):
    # p*len(xi) == 1 group to chose from
    xi = np.array([[1,1],[2,2],[3,3],[4,4],[5,5]])
    ranklist = [[0],[1],[2],[3],[4]]
    mock_randint.side_effect = MockRandinti().mock_randinti
    mock_choice.side_effect = mock_choicei
    p = 0.1
    xpb = sample_pbest(xi, p, ranklist)
    assert len(xpb) == len(xi)
    np.testing.assert_equal(xpb[0],[1,1])
    np.testing.assert_equal(xpb[1],[1,1])

    # p*len(xi) == 2 groups to chose from
    xi = np.array([[i,i] for i in xrange(10)])
    ranklist = [[i] for i in xrange(10)]
    mock_randint.side_effect = MockRandinti().mock_randinti
    mock_choice.side_effect = mock_choicei
    p = 0.2
    xpb = sample_pbest(xi, p, ranklist)
    assert len(xpb) == len(xi)
    np.testing.assert_equal(xpb[0], [0, 0])
    np.testing.assert_equal(xpb[1], [1, 1])
    np.testing.assert_equal(xpb[2], [0, 0])
    np.testing.assert_equal(xpb[3], [1, 1])

    xi = np.array([[i,i] for i in xrange(100)])
    ranklist = [[i] for i in xrange(100)]
    mock_randint.side_effect = MockRandinti().mock_randinti
    mock_choice.side_effect = mock_choicei
    p = 0.2
    xpb = sample_pbest(xi, p, ranklist)
    assert len(xpb) == len(xi)
    np.testing.assert_equal(xpb[0], [0, 0])
    np.testing.assert_equal(xpb[1], [1, 1])
    np.testing.assert_equal(xpb[2], [2, 2])
    np.testing.assert_equal(xpb[3], [3, 3])


@mock.patch('differentialevolution.np.random.binomial')
@mock.patch('differentialevolution.np.random.randint')
def test_apply_crossover(mock_randint, mock_binomial):
    xi = np.array([[1,1],[2,2]])
    v = np.array([[3,3],[4,4]])
    Cr = np.ones(len(xi))
    mock_randint.side_effect = mock_randint0
    mock_binomial.side_effect = mock_binomial1
    v1, Cr1 = apply_crossover(xi, v, Cr)
    np.testing.assert_equal(v, v1)
    np.testing.assert_equal(Cr, [1., 1.])

    xi = np.array([[1,1,1],[2,2,2]])
    v = np.array([[3,3,3],[4,4,4]])
    Cr = np.zeros(len(xi))
    mock_randint.side_effect = mock_randint0
    mock_binomial.side_effect = mock_binomial1
    v1, Cr1 = apply_crossover(xi, v, Cr)
    np.testing.assert_equal(v, [[3,1,1],[4,2,2]])
    np.testing.assert_almost_equal(Cr1, [1./3, 1./3])

    xi = np.array([[1,1],[2,2]])
    v = np.array([[3,3],[4,4]])
    Cr = 0.5*np.ones(len(xi))
    mock_randint.side_effect = mock_randint0
    mock_binomial.side_effect = mock_binomial1
    v, Cr = apply_crossover(xi, v, Cr)
    np.testing.assert_equal(v, [[3,1],[4,2]])
    np.testing.assert_equal(Cr, [0.5, 0.5])

    xi = np.array([[1,1],[2,2]])
    v = np.array([[3,3],[4,4]])
    Cr = 0.5*np.ones(len(xi))
    mock_randint.side_effect = mock_randint0
    mock_binomial.side_effect = mock_binomial0
    v, Cr = apply_crossover(xi, v, Cr)
    np.testing.assert_equal(v, [[3,3],[4,4]])
    np.testing.assert_equal(Cr, [1., 1.])


def test_crowding_meassure():
    d = np.array([0.5,1,1.5,2])
    assert crowding_meassure(d, alpha=0) == 5
    np.testing.assert_allclose(crowding_meassure(d, alpha=1), np.sum(np.log(d)))
    np.testing.assert_allclose(crowding_meassure(d, alpha=2), 1.0/np.sum(1.0/d))
    np.testing.assert_allclose(crowding_meassure(d, alpha=3), 1.0/np.sqrt(np.sum(1.0/d**2)))
    
    d[3] = np.inf
    assert crowding_meassure(d, alpha=0) == np.inf
    assert crowding_meassure(d, alpha=1) == np.inf
    np.testing.assert_allclose(crowding_meassure(d, alpha=2), 1.0/np.sum(1.0/d[:3]))
    np.testing.assert_allclose(crowding_meassure(d, alpha=3), 1.0/np.sqrt(np.sum(1.0/d[:3]**2)))


def test_remove_crowded():
    x = np.array([[0,0],[0,1]])
    c = remove_crowded(x,S=np.arange(len(x)),k=2,n=1,alpha=2)
    assert len(c) == 1

    x = np.array([[0,0],[0,1],[0.5,0.5]])
    c = remove_crowded(x,S=np.arange(len(x)),k=2,n=2,alpha=2)
    assert len(c) == 2
    c = remove_crowded(x,S=np.arange(len(x)),k=2,n=1,alpha=2)
    assert len(c) == 1

    x = np.array([[0,0],[0,1],[1,1],[1,0],[0.5,0.5],[0.25,0.25]])
    c = remove_crowded(x,S=np.arange(len(x)),k=2,n=1,alpha=2)
    assert c == [5]
    
    c = remove_crowded(x,S=np.arange(len(x)),k=2,n=2,alpha=2)
    assert c == [4,5]

    c = remove_crowded(x,S=np.arange(len(x)),k=2,n=2,alpha=2,prio_dfactor=0.1,priorities=[(0,1),(1,2)])
    assert c == [0,3]


def test_compare_objectives():
    x = np.array([[0],[1],[1]])
    y = np.array([[1],[1],[0]])
    np.testing.assert_equal(compare_objectives(x,y,None), np.array([1,0,-1]))

    x = np.array([[0,0],[0,1],[1,1]])
    y = np.array([[1,1],[1,0],[0,0]])
    np.testing.assert_equal(compare_objectives(x,y,None), np.array([1,0,-1]))


def test_rank():
    x = np.array([2,1])
    np.testing.assert_equal(rank(x), [[1],[0]])

    x = np.array([[0,0],[0,1],[1,1]])
    np.testing.assert_equal(rank(x), [[0], [1], [2]])

    x = np.array([[1,0],[0,1],[1,1]])
    assert sorted(rank(x)[0]) == [0,1]

    x = np.array([[0.,0.],[0.5,0.9],[0.9,0.5],[0.2,0.2],[0.,4.]])
    assert all(sorted(i) == j for i,j in zip(rank(x), [[0],[3,4],[1,2]]))


@mock.patch('differentialevolution.apply_crossover')
def test_DE(mock_apply_crossover):

    def f1(x):
        return np.abs(x)

    def mock_apply_crossover_fake(xi, v, Cr):
        v = xi - 0.5
        v[-1] = xi[-1]
        return v, Cr

    import populations
    pop = populations.DefaultPopulation(f1, objective_dimension=2, vector_dimension=2)
    de = DE(pop)
    # selection by rank

    ranklist = rank(np.array([[0.,0.],[1.,0],[0.,1.],[1.,1.]]))
    ranklist, remove_list = de.selection_byrank(popsize=3, ranklist=ranklist)
    assert remove_list == [3]
    assert sum(len(l) for l in ranklist) == 3

    # selection by crowding distance
    x = np.array([[1.,0],[0.,1.],[0.5,0.5]])
    ranklist = rank(x)
    ranklist, remove_list = de.selection_bycrowding(popsize=2, ranklist=ranklist, xi_obj=x)
    assert remove_list == [2]
    assert sum(len(l) for l in ranklist) == 2

    mock_apply_crossover.side_effect = mock_apply_crossover_fake
    pop.init_fromdata(np.array([[0.,0.],[0.5,0.9],[0.9,0.5],[0.2,0.2],[0.,4.]]))
    # [0,0] -> should stay in the population
    # [0.5,0.9], [0.9,0.5] -> superseeded by their children in the first selection, should go to archive
    # [0.2, 0.2] -> should be removed by crowding density, should not go to archive
    # [0., 4.] -> should be removed for having the same objective value than its child, should not go to archive
    arch = populations.DefaultArchive(maxsize=5, population=pop)
    de = DE(pop, archive=arch)
    ranklist = de.run_generation(3)
    assert pop.size == 3
    assert arch.size == 2
    assert ranklist[0] == [0] and sorted(ranklist[1]) == [1,2]
    np.testing.assert_almost_equal(pop.get_vectors(), np.array([[0., 0.],[0., 0.4],[0.4, 0.]]))
    np.testing.assert_equal(arch.get_vectors(), np.array([[0.5,0.9], [0.9,0.5]]))


