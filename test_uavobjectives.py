import numpy as np
from uav import UAV
from uavobjectives import *
from uavobjectives import _UAV_load, _UAV_slope, _UAV_climbing_limit, _UAV_diving_limit, _UAV_fuel, _UAV_hlength, _UAV_length, _UAV_max_load

### Unit tests for objectives.py

def test_UAV_load():
    traj = np.array([[0,0],[1,1],[2,0]]).T
    n = _UAV_load(traj, 2)
    np.testing.assert_allclose(n, np.array([np.sqrt(1+16/9.81**2)]))
    
def test_UAV_slope():
    traj = np.array([[0,0,0],[1,1,1]]).T
    s = _UAV_slope(traj)
    np.testing.assert_allclose(s, np.array([1/np.sqrt(2)]))

    traj = np.array([[0,0,0],[1,0,0.5]]).T
    s = _UAV_slope(traj)
    np.testing.assert_allclose(s, np.array([0.5]))
    
def test_UAVcost_turning_limit():
    traj = np.array([[0,0,0],[1,1,1],[2,0,0]]).T
    n_max = _UAV_max_load(traj[2])[1]
    V = np.sqrt(9.81*np.sqrt(n_max**2-1))
    assert UAVcost_turning_limit(traj,V) == 0
    assert UAVcost_turning_limit(traj,V+1e-6) == 1
    
def test_UAVcost_slope_limits():
    traj = np.array([[0,0,0],[1,0,1],[2,0,0]]).T
    climbing_limit = _UAV_climbing_limit(traj[2])
    diving_limit = _UAV_diving_limit(traj[2])
    assert UAVcost_slope_limits(traj) == 2
    traj = np.array([[0,0,0],[1,0,0.3],[2,0,0]]).T
    assert UAVcost_slope_limits(traj) == 0
    traj = np.array([[0,0,0],[1,0,0.5],[2,0,0.25]]).T
    assert UAVcost_slope_limits(traj) == 1
    traj = np.array([[0,0,0.25],[1,0,0.5],[2,0,0]]).T
    assert UAVcost_slope_limits(traj) == 1
    
def test_UAVcost_fuel_limit():
    traj = np.array([[0,0,0],[1,0,0],[2,0,0]]).T
    np.testing.assert_almost_equal(UAVcost_fuel_limit(traj, fuel_limit=1, V=0.001), 29.5-1, decimal=5)

def test_UAVcost_flight_altitude():
    import maps
    traj = np.array([[0,0,1],[1,0,2],[2,0,3]]).T
    dtedmap = maps.DTEDmap((3,3))
    assert UAVcost_flight_altitude(traj,dtedmap,1.0) == (0, 2)
    
def test_UAVcost_map_limit():
    import maps
    traj = np.array([[0,0,1],[1,3,2],[2,0,3]]).T
    dtedmap = maps.DTEDmap((4,4))
    assert UAVcost_map_limit(traj,dtedmap) == 2
    
def test_UAVcost_map_nfz():
    traj = np.array([[0,1,1],[1,1,2],[2,1,3]]).T
    NFZlist=[(0,2,0,3)]
    assert UAVcost_map_nfz(traj,NFZlist) == 1
    NFZlist=[(0,2,1,3)]
    assert UAVcost_map_nfz(traj,NFZlist) == 0

def test_UAVcost_path_length():
    traj = np.array([[0,1,1],[1,1,2],[2,1,3]]).T
    assert UAVcost_path_length(traj,1.5) == np.round(2*np.sqrt(2)*2000/1.5)/2000.0

def test_UAVcost_PDetectKillADU():
    import maps
    from adus import ADU
    traj = np.array([[0,1,1],[0.5,1,1],[1,1,1],[1.5,1,1],[2,1,1]]).T
    uav = UAV(0,3,2,1)
    dtedmap = maps.DTEDmap((3,3))
    for adu_type in [10, 15, 20]:
        adu0 = ADU(0,adu_type,1,1,0)
        dtedmap.setPOI(adu0.idn,adu0.x,adu0.y,adu0.z)
        np.testing.assert_allclose(UAVcost_PDetectKillADU(traj, uav.V, dtedmap, adu0, uav),(1.0,0.0))


def fakescenery():
    import uav
    import sceneries
    uav0 = uav.UAV('UAV1', 1, 1, 1)
    uav0.waypoints.extend([("WP", 0., 0., 0.), ("WP", 1., 1., 1.)])
    scenery = sceneries.Scenery()
    scenery.UAVlist.append(uav0)
    return scenery


class FakePopulation(object):
    def __init__(self):
        self.size = 1

    def discretize(self, i, s):
        return np.array([[0., 1.], [0., 1.], [0., 1.]]), [0, 1]


def test_UAVObjective():
    objective = UAVObjective(fakescenery())
    fakepopulation = FakePopulation()
    result = objective.objective_function(fakepopulation)
    assert len(result[0]) == 10