import numpy as np
from utils import jit

@jit
def dominates(x, y, priorities):
    """
    Checks if there is a partial order relationship (pareto dominance) between two vectors optionally using priorities (Fonseca & Fleming 1998 formulation)
    Returns True if x dominates y, False otherwise
    Input:
        x, y: float[:]  # vectors to be compared (1D arrays)
        priorities: list of tuple(int,int)  # empty list for no priorities, otherwise a list of tuples defining the starting and ending+1 vector slice subscripts for each priority on the vector. The rightmost tuple defines the highest priority. For example, the priorities list of a 3D vector [x,y,z] with [x] on the highest priority and [y,z] on the lowest priority would be defined as [(0,1),(1,3)]
    Output:
        boolean
    """
    dom = False
    if priorities:
        for p in xrange(len(priorities)):
            i1, i2 = priorities[p]
            if all(x[i1:i2] <= y[i1:i2]) and any(x[i1:i2] < y[i1:i2]):
                dom = True
                break
            elif any(x[i1:i2] != y[i1:i2]):
                break
    else:
        dom = all(x <= y) and any(x < y)
    return dom



@jit
def _prio_distance(x, y, priorities, dist):
    for i in xrange(dist.size):
        for p in xrange(len(priorities)):
            j1, j2 = priorities[p]
            if all(x[i, j1:j2] <= y[i, j1:j2]) and any(x[i, j1:j2] < y[i, j1:j2]):
                break
            elif any(x[i, j1:j2] != y[i, j1:j2]):
                break
        if j2 - j1 > 1:
            dist[i] = np.linalg.norm(x[i, j1:j2] - y[i, j1:j2], 2)
        else:
            dist[i] = abs(x[i, j1:j2] - y[i, j1:j2])


def prio_distance(x, y, priorities=[]):
    """
    Return the distance between two objective values using priorities
    Input:
        x: float[n,m] or float[n] # row vectors (multi-objective) or floats (mono-objective)
        y: float[x.shape] # row vectors or floats
        priorities=[]: list of tuple(int,int) # empty list for no priorities, mono-objective, otherwise a list of tuples defining the starting and ending+1 vector slice subscripts for each priority on the vector
    Output:
        float[n] # euclidean distance between each pair x[i] y[i]
    """
    if priorities:
        dist = np.empty(x.shape[0])
        _prio_distance(x, y, priorities, dist)
    else:
        if x.ndim > 1 and x.shape[1] > 1:
            dist = np.linalg.norm(x - y, 2, axis=1) # np.linalg.norm causes FP underflow for numbers smaller than 1e-161
        else:
            dist = np.abs(x - y).ravel()
    return dist


def pareto_front_sort_classic(x_obj, priorities=[]):
    """
    Sorts the population in pareto fronts and returns a list of population indexes sets corresponding to each pareto front.
    This function takes O(N^2) time on the population size
    Input parameters:
        x_obj: array_like of float[:] # population objective values
        priorities = []: list of tuple(int,int) # priorities expressed as tuple(starting slice index, ending slice index) - see dominates()
    Output:
        list of [int,..] # list of lists of x_obj indexes
    """
    nd = [0]*len(x_obj) # nd[i] -> number of individuals dominating i
    dom = [list() for i in xrange(len(x_obj))] # dom[i] -> list of individuals dominated by i
    for i in xrange(len(x_obj)):
        for j in xrange(i + 1, len(x_obj)):
            if dominates(x_obj[i],x_obj[j],priorities):
                dom[i].append(j)
                nd[j] += 1
            elif dominates(x_obj[j],x_obj[i],priorities):
                dom[j].append(i)
                nd[i] += 1
    F = [[i for i in xrange(len(x_obj)) if nd[i]==0]] # F[i] -> list of individuals in pareto front i, initialize with pareto front 0
    while F[-1]:
        Fi = F[-1]
        F.append(list())
        for i in Fi:
            for j in dom[i]:
                nd[j] -= 1
                if nd[j] == 0:
                    F[-1].append(j)
    return F[:-1]
            

# This is O(N*log(N))
def pareto_front_sort(x_obj, priorities=[]):
    """
    Sorts the population in pareto fronts and returns an ordered list of population indexes unordered lists corresponding to each pareto front.
    Based on Jensen (2003), Fortin et al. (2013) and Buzdalov et al (2014).
    Input parameters:
        x_obj: float[:]     # population objective values
        priorities = []: list of tuple(int,int) # priorities expressed as tuple(starting slice index, ending slice index) - see dominates()
    Output:
        list of [int,..]    # list of lists of x_obj indexes
    """
    if not(priorities):
        priorities = [(0, x_obj.shape[1])]
    front = np.zeros(len(x_obj), dtype=np.int) # front number
    # Sort the objective values lexicographically starting by the highest priority (leftmost) objective
    S = np.lexsort(x_obj.T[::-1]) # get the set lexicographically ordered
    k = priorities[0][1] - priorities[0][0] - 1
    plevel = 0
    _NDHelperA(S, k, plevel, x_obj, priorities, front) # sort it
    
    # build the pareto fronts
    F = [list() for i in xrange(max(front)+1)]
    for i in xrange(len(S)):
        F[front[S[i]]].append(S[i])
    return F


def _NDHelperA(S, k, plevel, x_obj, priorities, front):
    """
    Private recursive function for pareto_front_sort()
    Update a non-dominated sorting of S on the first k objectives.
    Input:
        S: int[n]           # subset of indexes of x_obj that sort it lexicographically
        k: int              # current objective number at the current priority level (maximum on non-recursive calls)
        plevel: int         # current priority level (from 0 to len(priorities)-1)
        x_obj: float[N,m]   # population objectives to sort. Must be k<=m and n<=M
        priorities: list of tuple(int,int)  # priorities expressed as tuple(starting slice index, ending slice index) - see dominates()
        front: int[N]       # current front number for each population member to be updated
    """
    kmin, kmax = priorities[plevel]
    if len(S) < 2: # less than two individuals, nothing to do
        return
    elif len(S) == 2: # only two individuals, compare them and adjust front number
        s1, s2 = x_obj[S[0]], x_obj[S[1]]
        if dominates(s1, s2, priorities):
            front[S[1]] = max(front[S[1]], front[S[0]] + 1)
    elif k == 1: # only two objectives remaining, only happens if the first m-2 objectives are identical
        _SweepA(S, plevel, x_obj, priorities, front)
    elif k == 0: # only one objective at the current priority level
        # run a linear scan
        # group all individuals with the same objective value and recursively sort at the next priority level
        # assign consecutive front numbers when the objective value differs
        Si0 = S[0]
        i = 0
        while i < len(S):
            # group individuals with the same objective value
            group = []
            while (i < len(S)) and (x_obj[S[i], kmin] == x_obj[Si0, kmin]):
                group.append(S[i])
                front[S[i]] = max(front[S[i]], front[Si0])
                i += 1
                # at the end of the loop i points to the first solution after Si0 with different objective values
            # recursive sort at the next priority level for groups
            if (len(group) > 1) and (plevel < len(priorities) - 1):
                k = priorities[plevel+1][1] - priorities[plevel+1][0] - 1
                _NDHelperA(np.array(group), k, plevel+1, x_obj, priorities, front)
            # update the current solution front
            if i < len(S):
                Si0 = S[i]
                r = np.max(front[group])
                front[Si0] = max(front[Si0], r + 1)
    # the next two sections can only be reached for k >= 2 (at least 3 objectives)
    elif np.all(x_obj[S, kmin+k] == x_obj[S[0], kmin+k]): # all individuals have the same value for objective k
        _NDHelperA(S, k-1, plevel, x_obj, priorities, front) # apply recursion for objectives 0:k-1
    else:
        # More than two individuals
        m = np.median(x_obj[S, kmin+k])
        L, M, H, LM = _SplitBy(S, m, kmin+k, x_obj) # split S around the median of objective k
        _NDHelperA(L, k, plevel, x_obj, priorities, front) # apply recursion for subset L
        _NDHelperB(L, M, k-1, plevel, x_obj, priorities, front) # update fronts for M according to L
        _NDHelperA(M, k, plevel, x_obj, priorities, front) # apply recursion for subset M
        _NDHelperB(LM, H, k-1, plevel, x_obj, priorities, front) # update fronts for H according to L U M
        _NDHelperA(H, k, plevel, x_obj, priorities, front) # apply recursion for subset H


def _SweepA(S, plevel, x_obj, priorities, front):
    """
    Private function for _NDHelperA()
    Update rank number associated to the fitnesses according to two objectives using a geometric sweep procedure.
    Constructs all the fronts simultaneously by sweeping the solutions one by one in a way that guarantees that if solution S[j] is swept after solution S[i] then S[j] can not dominate S[i]
    Input:
        S: int[n]           # subset of indexes of x_obj, x_obj[S] must be lexicographically sorted (ordered from smaller to greater with respect to objective 0, then objective 1,...)
        x_obj: float[N,2]   # population objectives
        priorities: list of tuple(int,int) # priorities expressed as tuple(starting slice index, ending slice index) - see dominates()
        plevel: int         # current priority level (from 0 to len(priorities)-1)
        front: int[N]       # current front number for each population member to be updated
    """
    kmin = priorities[plevel][0] # first objective at the current priority level
    stairs = {front[S[0]]:S[0]} # keep one solution per front, the one with the lowest 2nd coord. Initialize with the leftmost solution in the first front (S[0] due to lexical pre-sorting)
    Sj0 = S[0] # keeps the first solution of the last group
    # sweep all solutions from left to right
    j = 0
    while j < len(S):
        # group individuals with the same objective values at the current priority level
        # (equal values at previous priority levels are guaranteed by construction of _NDHelperA)
        group = [] # list of solutions that make a group
        while j < len(S) and np.all(x_obj[S[j], kmin:kmin+2] == x_obj[Sj0, kmin:kmin+2]):
            front[S[j]] = max(front[Sj0], front[S[j]])
            group.append(S[j])
            j += 1
            # at the end of the loop j points to the first solution with different objective values
        # recursive sort at the next priority level for groups
        if (len(group) > 1) and (plevel < len(priorities) - 1):  # sanity check
            k = priorities[plevel + 1][1] - priorities[plevel + 1][0] - 1
            _NDHelperA(np.array(group), k, plevel + 1, x_obj, priorities, front)
            for Si in group:
                stairs[front[Si]] = Si
        # find the latest dominating front for the current solution
        if j < len(S):
            for i in stairs:
                if x_obj[stairs[i], kmin+1] <= x_obj[S[j], kmin+1]:
                    front[S[j]] = max(front[S[j]], i+1)
            # update the last non dominating front inserting it if necessary
            stairs[front[S[j]]] = S[j]
            Sj0 = S[j]


def _NDHelperB(L, H, k, plevel, x_obj, priorities, front):
    """
    Private recursive function for _NDHelperA()
    Update front numbers to population members in H according to the individuals in L. The individuals in L are assumed to have correct front numbers
    Input:
        L: int[n1]          # subset of indexes of x_obj, x_obj[L] assumed to be lexicographically ordered and already non-dominated sorted through front[L]
        H: int[n1]          # subset of indexes of x_obj, x_obj[H] assumed to be lexicographically greater than x_obj[L] and ordered
        k: int              # current objective number at the current priority level
        plevel: int         # current priority level (from 0 to len(priorities)-1)
        x_obj: float[N,m]   # population objectives to sort. Must be n1+n2<=M
        priorities: list of tuple(int,int)  # priorities expressed as tuple(starting slice index, ending slice index) - see dominates()
        front: int[N] # current front number for each population member to be updated
    """
    # NDHelperB is always called with k >= 1 (at least 2 objectives)
    kmin, kmax = priorities[plevel]
    if len(L) == 0 or len(H) == 0:
        return
    elif len(L) == 1 or len(H) == 1:
        # if one of the sets contains only one member, compare it to the other set
        for h in H:
            for l in L:
                if dominates(x_obj[l], x_obj[h], priorities):
                    front[h] = max(front[h], front[l] + 1)
    elif k == 1:
        # if only two objectives remain
        _SweepB(L, H, plevel, x_obj, priorities, front) # use a sweep algorithm to sort
    # the next two sections can only be reached if k > 1 (at least 3 objectives)
    elif x_obj[L, kmin+k].max() <= x_obj[H, kmin+k].min():
        # if all L members have objective k equal or smaller than all H members
        _NDHelperB(L, H, k-1, plevel, x_obj, priorities, front) # apply recursion for objectives 0:k-1
    elif x_obj[L, kmin+k].min() <= x_obj[H, kmin+k].max():
        # if any (but not all) L member has objective k smaller than some H member
        m = np.median(x_obj[np.hstack((L,H)), kmin+k])
        # split L and H around the median of L U H
        LL, LM, LH, LLM= _SplitBy(L, m, kmin+k, x_obj)
        HL, HM, HH, HLM = _SplitBy(H, m, kmin+k, x_obj)
        _NDHelperB(LL, HL, k, plevel, x_obj, priorities, front) # recursively update "low" H with "low" L for the current objective k
        _NDHelperB(LL, HM, k-1, plevel, x_obj, priorities, front) # recursively update "median" H with "low" L, as we already know that objective k is smaller for "low" L than for "median" H use objective k-1
        _NDHelperB(LM, HM, k-1, plevel, x_obj, priorities, front) # recursively update "median" H with "median" L, as we already know that objective k is the same for "median" L than for "median" H use objective k-1
        _NDHelperB(LLM, HH, k-1, plevel, x_obj, priorities, front) # recursively update "high" H with "low" "median" L, as we already know that objective k is smaller for "low" "median" L than for "high" H use objective k-1
        _NDHelperB(LH, HH, k, plevel, x_obj, priorities, front) # recursively update "high" H with "high" L for the current objective k
    #if all L members have objective k greater than all H members there is nothing to do

@jit
def _SplitBy(S, m, k, x_obj):
    """
    Private function for _NDHelperA() and _NDHelperB()
    Split S according to a given value of the objective k keeping the relative sorting order in each subset
    Input:
        S: int[n]           # subset of indexes of x_obj to be split
        m: int or float     # value used to split S
        k: int              # objective number used to split S
        x_obj: float[N,m]   # population objectives to sort. Must be k<=m and n<=M
    Output:
        int[n1], int[n2], int[n3], int[n1+n2] # L (lower than m), M (equal to m), H (higher than m), LM (lower or equal than m); L+M+H=S (verifies n1 + n2 + n3 == n)
    """
    L = S[x_obj[S,k] < m] # subset L lower than the median
    M = S[x_obj[S,k] == m] # subset M equal to the median
    H = S[x_obj[S,k] > m] # subset H greater than the median
    LM = S[x_obj[S,k] <= m] # subset L + M keeping the lexical sorting
    return L, M, H, LM

@jit
def _SweepB(L, H, plevel, x_obj, priorities, front):
    """
    Private function for _NDHelperB()
    Adjust the rank numbers in H according to those in L on the first two objectives using a sweep procedure.
    Input:
        L: int[n1]          # subset of indexes of x_obj, x_obj[L] assumed to be lexicographically ordered
        H: int[n1]          # subset of indexes of x_obj, x_obj[H] assumed to be lexicographically ordered
        plevel: int         # current priority level (from 0 to len(priorities)-1)
        x_obj: float[N,m]   # population objectives to sort. Must be n1+n2<=M
        priorities: list of tuple(int,int)  # priorities expressed as tuple(starting slice index, ending slice index) - see dominates()
        front: int[N]       # current front number for each population member to be updated
    """
    kmin = priorities[plevel][0] # first objective at the current priority level
    stairs = {} # keep one solution per front, the one with the lowest 2nd coord.
    # sweep all L and H solutions from left to right
    i = 0
    for h in H:
        # update the fronts from L that dominate the current solution h from H, finding the rightmost lowest solution from L in each front
        while i < len(L) and tuple(x_obj[L[i], kmin:kmin+2]) <= tuple(x_obj[h, kmin:kmin+2]):
            l = L[i]
            if not(stairs.has_key(front[l])) or (x_obj[l, kmin+1] <= x_obj[stairs[front[l]], kmin+1]):
                stairs[front[l]] = l
            i += 1
        # find the rightmost dominating front from L and update the current solution front
        for r in stairs:
            if x_obj[stairs[r], kmin+1] <= x_obj[h, kmin+1]:
                front[h] = max(front[h], r+1)

