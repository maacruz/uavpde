import numpy as np
from utils import *
import scipy.stats as stats
import mock

# Unit tests for utils.py

def test_ordered_insert():
    #trivial empty insert
    u1 = np.array([])
    u2 = np.array([])
    np.testing.assert_equal(ordered_insert(u1,u2),(np.array([]),np.array([],int)))
    
    u1 = np.array([1,2])
    u2 = np.array([0.5])
    np.testing.assert_equal(ordered_insert(u1,u2),(np.array([0.5,1,2]),np.array([0],int)))
    
    u2 = np.array([1.5])
    np.testing.assert_equal(ordered_insert(u1,u2),(np.array([1,1.5,2]),np.array([1],int)))

    u2 = np.array([2.5])
    np.testing.assert_equal(ordered_insert(u1,u2),(np.array([1,2,2.5]),np.array([2],int)))
    
    u1 = np.array([0.5])
    u2 = np.array([1,2])
    np.testing.assert_equal(ordered_insert(u1,u2),(np.array([0.5,1,2]),np.array([1,2],int)))
    
    u1 = np.array([1.5])
    np.testing.assert_equal(ordered_insert(u1,u2),(np.array([1,1.5,2]),np.array([0,2],int)))

    u1 = np.array([2.5])
    np.testing.assert_equal(ordered_insert(u1,u2),(np.array([1,2,2.5]),np.array([0,1],int)))


def mock_rand0(s=0., e=1., size=None):
    if isinstance(s, np.ndarray):
        assert size is None
        size = s.size
    if size is None:
        return s
    return s * np.ones(size, dtype=int)

def mock_rand1(s=0., e=1., size=None):
    if isinstance(s, np.ndarray):
        assert size is None
        size = s.size
    if size is None:
        return e
    return e * np.ones(size, dtype=int)

def mock_rand2(s=0., e=1., size=None):
    if isinstance(s, np.ndarray):
        assert size is None
        size = s.size
    if size is None:
        return (s + e)/2.
    return (s + e)/2. * np.ones(size, dtype=int)

@mock.patch('utils.np.random.uniform')
def test_cauchy_sample(mock_uniform):
    mock_uniform.return_value = np.array([0.5])
    x = cauchy_sample()
    np.testing.assert_almost_equal(x,[0.])

    mock_uniform.return_value = np.array([0.25])
    x = cauchy_sample()
    np.testing.assert_almost_equal(x, stats.cauchy.ppf(0.25))

    x = cauchy_sample(loc=-1, scale=2)
    np.testing.assert_almost_equal(x, 2.*stats.cauchy.ppf(0.25)-1.)

    mock_uniform.side_effect = mock_rand2
    x = cauchy_sample(loc=np.array([0.,1.]))
    np.testing.assert_almost_equal(x,[0,1.])

    mock_uniform.side_effect = mock_rand0
    x = cauchy_sample(low=0.5)
    np.testing.assert_almost_equal(x,[0.5])

    mock_uniform.side_effect = mock_rand1
    x = cauchy_sample(high=0.5)
    np.testing.assert_almost_equal(x,[0.5])


def test_convolve():
    np.testing.assert_almost_equal(convolve(np.array([1.,2.]),np.array([1.,2.,1.])), np.array([4., 5.]))

    np.testing.assert_almost_equal(convolve(np.array([0.,1.,2.,3.]),np.array([1.,2.,1.])), np.array([1., 4., 8., 8.]))


def test_convolution_kernel_1D():
    k = convolution_kernel_1D(1.,0)
    np.testing.assert_equal(k, [1.])

    k = convolution_kernel_1D(0.5, 4)
    np.testing.assert_almost_equal(k.sum(), 1.)
    assert k.size == 5
    g = stats.norm.pdf(np.linspace(-0.5, 0.5, 5),scale=0.5)
    np.testing.assert_almost_equal(k, g/g.sum())

    k = convolution_kernel_1D(0.5, 3, 'cauchy')
    np.testing.assert_almost_equal(k.sum(), 1.)
    assert k.size == 3
    g = stats.cauchy.pdf(np.linspace(-0.5, 0.5, 3),scale=0.5)
    np.testing.assert_almost_equal(k, g/g.sum())
