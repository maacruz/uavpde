import numpy as np
from scipy.interpolate import splev
import trajectories
from trajectories import _wri_wavelet_scale, _wri_wpextra_points
import splinewavelets as sw
import mock

def test_rotation_matrix():
    v = np.array([1,0,0])
    R = trajectories.rotation_matrix3D(v)
    np.testing.assert_equal(np.eye(3),R)

    v = np.array([0,1,0])
    R = trajectories.rotation_matrix3D(v)
    np.testing.assert_equal(np.array([[0,-1,0],[1,0,0],[0,0,1]]),R)

    v = np.array([0, 0, 1])
    R = trajectories.rotation_matrix3D(v)
    np.testing.assert_equal(np.array([[0, 0, -1], [0, 1, 0], [1, 0, 0]]), R)


def test_wri_wavelet_scale():
    degree = 1
    ncoefs = 2
    u_wp = np.array([0.,1.])
    wp = np.array([[0.,1.]])
    assert _wri_wavelet_scale(degree, ncoefs, u_wp, wp) == 1.

    degree = 1
    ncoefs = 4
    u_wp = np.array([0.,0.3,0.7,1.])
    wp = np.array([[0.,0.2,0.5,1.]])
    result = np.dot(np.array([0.2,0.3,0.5]), np.array([[0.70611961,0.29388039,0.], [0.14140508,0.71718983,0.14140508], [0.,0.29388039,0.70611961]]).T)
    np.testing.assert_almost_equal(_wri_wavelet_scale(degree, ncoefs, u_wp, wp),result, 3)


def test_wri_wpextra_points():

    wp = np.array([[0.,0.,0.],[1.,1.,1.]]).T
    wpextra_points, wp_idx = _wri_wpextra_points(1,wp,sigma=np.array([0,0,0]),degree=2)
    np.testing.assert_equal(wp_idx, [0,2])
    np.testing.assert_almost_equal(wpextra_points, np.array([[0.,0.,0.],[0.5,0.5,0.5],[1.,1.,1.]]).T[None,...])

    # enough waypoints to fully define a b-spline
    wp = np.array([[0.,0.,0.],[0.6,0.6,0.6],[1.,1.,1.]]).T
    wpextra_points, wp_idx = _wri_wpextra_points(1,wp,sigma=np.array([0,0,0]),degree=2)
    np.testing.assert_equal(wp_idx, [0,1,2])
    np.testing.assert_almost_equal(wpextra_points, np.array([[0.,0.,0.],[0.6,0.6,0.6], [1.,1.,1.]]).T[None,...])

    # One waypoint segment, arbitrary degree, point clouds should be in planes perpendicular to segment
    wp = np.array([[0.,0.,0.],[1.,1.,1.]]).T
    degree = 4
    n = 10
    wpextra_points, wp_idx = _wri_wpextra_points(n,wp,sigma=np.array([0,1,1]),degree=degree)
    np.testing.assert_equal(wp_idx, [0,degree])
    v_wp = np.diff(wp)
    v_wp /= np.linalg.norm(v_wp, 2, axis=0)
    p_idx = range(1, degree)
    projected_on_wp = np.dot(v_wp.T[None,...], wpextra_points[...,p_idx])[0,0]
    expected = np.linalg.norm(np.diff(wp), 2, axis=0)*np.linspace(0., 1., degree+1)[1:-1]
    np.testing.assert_almost_equal(projected_on_wp, np.repeat(expected[None,...], n, axis=0))

    # two waypoint segments, 3rd degree, should insert extra point in the 2nd segment
    wp = np.array([[0.,0.,0.],[0.4,0.4,0.4],[1.,1.,1]]).T
    wpextra_points, wp_idx = _wri_wpextra_points(1,wp,sigma=np.array([0,0,0]),degree=3)
    np.testing.assert_equal(wp_idx, [0,1,3])
    np.testing.assert_almost_equal(wpextra_points, np.array([[0.,0.,0.],[0.4,0.4,0.4],[0.7,0.7,0.7],[1.,1.,1.]]).T[None,...])

    # two waypoint segments, 4rd degree, should insert extra point in each segment
    wp = np.array([[0.,0.,0.],[0.4,0.4,0.4],[1.,1.,1]]).T
    wpextra_points, wp_idx = _wri_wpextra_points(1,wp,sigma=np.array([0,0,0]),degree=4)
    np.testing.assert_equal(wp_idx, [0,2,4])
    np.testing.assert_almost_equal(wpextra_points, np.array([[0.,0.,0.],[0.2,0.2,0.2],[0.4,0.4,0.4],[0.7,0.7,0.7],[1.,1.,1.]]).T[None,...])

    # two waypoint segments, 4rd degree, should insert 2 extra points in 2nd segment
    wp = np.array([[0.,0.,0.],[0.1,0.1,0.1],[1.,1.,1]]).T
    wpextra_points, wp_idx = _wri_wpextra_points(1,wp,sigma=np.array([0,0,0]),degree=4)
    np.testing.assert_equal(wp_idx, [0,1,4])
    np.testing.assert_almost_equal(wpextra_points, np.array([[0.,0.,0.],[0.1,0.1,0.1],[0.4,0.4,0.4],[0.7,0.7,0.7],[1.,1.,1.]]).T[None,...])

def mock_random_normal_0(m, s, shape):
    return m+np.zeros(shape)

def mock_random_normal_1(m, s, shape):
    return m+np.ones(shape)

@mock.patch('trajectories._wri_wpextra_points')
@mock.patch('trajectories.np.random.normal')
def test_wavelet_random_initialization(mockrandom, mock_extrapoints):

    # Enough waypoints to completely specify an interpolating spline

    # Degree 1
    degree = 1

    wp = [(0.,0.,0.),(1.,0.,0.)]
    mockrandom.return_value = np.array([[[0.], [0.], [0.]]])
    tck, u = trajectories.wavelet_random_initialization(1,wp,degree)
    np.testing.assert_almost_equal(u, [0.,1.])
    np.testing.assert_almost_equal(splev(u,tck), np.array(wp).T[None,...])
    np.testing.assert_almost_equal(splev(0.5,tck), np.array([[0.5,0.,0.]]))

    wp = [(0.,0.,0.),(1.,1.,1.)]
    detail_coefs = np.array([[[1.], [1.], [1.]]])
    mockrandom.return_value = detail_coefs
    tck, u = trajectories.wavelet_random_initialization(1,wp,degree)
    np.testing.assert_almost_equal(u, [0.,1.])
    np.testing.assert_almost_equal(splev(u,tck), np.array(wp).T[None,...])
    SW = sw.SplineWaveletTransform(degree,constraints=[(0,0.),(0,1.)])
    lores_coefs = sw.splmake(np.array(wp).T,degree)[0][1]
    scale = np.linalg.norm(np.diff(np.array(wp).T,axis=1),2,axis=0)
    c1 = SW.reconstruct(lores_coefs, scale*detail_coefs)
    np.testing.assert_almost_equal(tck[1], c1[None,...])

    wp = [(0.,0.,0.),(1.,1.,1.)]
    detail_coefs = [np.array([[[0.], [0.], [0.]]]), np.array([[0.,0.,0.],[0.,0.,0.]]).T[None,...]]
    mockrandom.side_effect = mock_random_normal_0
    tck, u = trajectories.wavelet_random_initialization(1,wp,degree,wavelet_level=2)
    np.testing.assert_almost_equal(u, [0.,1.])
    np.testing.assert_almost_equal(splev(u,tck), np.array(wp).T[None,...])
    SW = sw.SplineWaveletTransform(degree,constraints=[(0,0.),(0,1.)])
    lores_coefs = sw.splmake(np.array(wp).T,degree)[0][1]
    scale = np.linalg.norm(np.diff(np.array(wp).T,axis=1),2,axis=0)
    c1 = SW.reconstruct(SW.reconstruct(lores_coefs, scale*detail_coefs[0]), scale*detail_coefs[1])
    np.testing.assert_almost_equal(tck[1], c1[None,...])

    wp = [(0.,0.,0.),(1.,1.,1.)]
    detail_coefs = [np.array([[[1.], [1.], [1.]]]), np.array([[1.,1.,1.],[1.,1.,1.]]).T[None,...]]
    mockrandom.side_effect = mock_random_normal_1
    tck, u = trajectories.wavelet_random_initialization(1,wp,degree,wavelet_level=2)
    np.testing.assert_almost_equal(u, [0.,1.])
    np.testing.assert_almost_equal(splev(u,tck), np.array(wp).T[None,...])
    SW = sw.SplineWaveletTransform(degree,constraints=[(0,0.),(0,1.)])
    lores_coefs = sw.splmake(np.array(wp).T,degree)[0][1]
    scale = np.linalg.norm(np.diff(np.array(wp).T,axis=1),2,axis=0)
    c1 = SW.reconstruct(SW.reconstruct(lores_coefs, scale*detail_coefs[0]), 0.5*scale*detail_coefs[1])
    np.testing.assert_almost_equal(tck[1], c1[None,...])

    # Degree 2
    degree = 2

    wp = [(0.,0.,0.), (1.,0.,0.), (1.,1.,1.)]
    detail_coefs = np.array([[[1.], [1.], [1.]]])
    mockrandom.side_effect = mock_random_normal_1
    tck, u = trajectories.wavelet_random_initialization(1,wp,degree)
    np.testing.assert_almost_equal(u, [0.,0.5,1.])
    np.testing.assert_almost_equal(splev(u,tck), np.array(wp).T[None,...])
    SW = sw.SplineWaveletTransform(degree,constraints=[(0,0.),(0,0.5),(0,1.)])
    lores_coefs = sw.splmake(np.array(wp).T,degree)[0][1]
    scale = _wri_wavelet_scale(degree, lores_coefs.shape[-1], u, np.array(wp).T)
    c1 = SW.reconstruct(lores_coefs, scale*detail_coefs)
    np.testing.assert_almost_equal(tck[1], c1[None,...])

    wp = [(0.,0.,0.), (1.,0.,0.), (1.,1.,1.)]
    detail_coefs = [np.array([[[1.], [1.], [1.]]]), np.array([[1.,1.,1.],[1.,1.,1.]]).T[None,...]]
    mockrandom.side_effect = mock_random_normal_1
    tck, u = trajectories.wavelet_random_initialization(1,wp,degree,wavelet_level=2)
    np.testing.assert_almost_equal(u, [0.,0.5,1.])
    np.testing.assert_almost_equal(splev(u,tck), np.array(wp).T[None,...])
    SW = sw.SplineWaveletTransform(degree,constraints=[(0,0.),(0,0.5),(0,1.)])
    lores_coefs = sw.splmake(np.array(wp).T,degree)[0][1]
    scale1 = _wri_wavelet_scale(degree, lores_coefs.shape[-1], u, np.array(wp).T)
    c1 = SW.reconstruct(lores_coefs, scale1*detail_coefs[0])
    scale2 = _wri_wavelet_scale(degree, c1.shape[-1], u, np.array(wp).T)
    c2 = SW.reconstruct(c1, 0.5*scale2*detail_coefs[1])
    np.testing.assert_almost_equal(tck[1], c2[None,...])

    # Not enough waypoints to completely specify an interpolating spline

    # Degree 2
    degree = 2
    wp = [(0.,0.,0.),(1.,1.,1.)]
    mock_extrapoints.return_value = (np.array([[0.,0.,0.],[0.5,0.5,0.5],[1.,1.,1.]]).T[None, ...], [0,2])
    mockrandom.side_effect = mock_random_normal_1
    tck, u = trajectories.wavelet_random_initialization(1,wp,degree)
    np.testing.assert_almost_equal(u, [0.,1.])
    np.testing.assert_almost_equal(splev(u,tck), np.array(wp).T[None, ...])
    SW = sw.SplineWaveletTransform(degree,constraints=[(0,0.),(0,1.)])
    lores_coefs = sw.splmake(mock_extrapoints.return_value[0],degree)[0][1]
    detail_coefs = np.array([[[1.], [1.], [1.]]])
    scale = _wri_wavelet_scale(degree, lores_coefs.shape[-1], u, np.array(wp).T)
    c1 = SW.reconstruct(lores_coefs, scale*detail_coefs)
    np.testing.assert_almost_equal(tck[1], c1)


def test_legacy_random_initialization():
    limits = [(0,1),(1,2),(2,3)]
    wp = [(0,1,2),(1,2,3)]
    points, wp_list = trajectories.legacyUAV_random_initialization(1, wp, limits)
    assert points.shape == (1,3,4)
    assert wp_list == [0,3]
    assert np.all(points[0,:,wp_list] == wp)
    assert all(np.all(points[0,i,:] >= limits[i][0]) for i in xrange(3))
    assert all(np.all(points[0,i,:] <= limits[i][1]) for i in xrange(3))


class Objective(object):
    def __init__(self):
        self.nobjectives = 1
    def objective_function(self, population):
        d = population.degree*np.arange(population.size)
        return d

@mock.patch('trajectories.legacyUAV_random_initialization')
def test_TrajectoryPopulationISplines(lri):
    objective = Objective()
    lri.return_value = (np.array([[[0., 0.25, 0.75, 1.], [0., 0.25, 0.75, 1.], [0., 0.25, 0.75, 1.]]]), [0, 3])
    wp = [(0.,0.,0.), (1.,1.,1.)]
    traj = trajectories.TrajectoryPopulationISplines(objective, wp)
    traj.random_initialize(popsize=1, limits=[(0.,1.)]*3)
    d = np.sqrt(3)/4
    discretized, wp_idx = traj.discretize(0, discretization_d=d)
    assert all(wp_idx == [0, 4])
    np.testing.assert_almost_equal(discretized, np.repeat(np.array([[0., 0.25, 0.5, 0.75, 1.]]), 3, axis=0), 2)
    np.testing.assert_almost_equal(traj.get_objectives(), traj.degree*np.arange(traj.size)[None,...])


@mock.patch('trajectories.wavelet_random_initialization')
@mock.patch('trajectories.legacyUAV_random_initialization')
def test_TrajectoryPopulationBSplines(lri, wri):
    objective = Objective()
    lri.return_value = (np.array([[[0., 0.25, 0.75, 1.], [0., 0.25, 0.75, 1.], [0., 0.25, 0.75, 1.]]]), [0, 3])
    wp = [(0.,0.,0.), (1.,1.,1.)]
    traj = trajectories.TrajectoryPopulationBSplines(objective, wp)
    traj.random_initialize(popsize=1, limits=[(0., 1.)] * 3)
    d = np.sqrt(3)/5
    discretized, wp_idx = traj.discretize(0, discretization_d=d)
    assert all(wp_idx == [0, 5])
    np.testing.assert_almost_equal(discretized, np.repeat(np.array([[0., 0.2, 0.4, 0.6, 0.8, 1.]]), 3, axis=0), 2)
    np.testing.assert_almost_equal(traj.get_objectives(), traj.degree*np.arange(traj.size)[None,...])

    objective = Objective()
    wri.return_value = (sw.splmake(np.array([[[0., 0.25, 0.75, 1.], [0., 0.25, 0.75, 1.], [0., 0.25, 0.75, 1.]]]))[0], [0., 0.5, 1.])
    wp = np.array([[0., 0.5, 1.], [0., 0.5, 1.], [0., 0.5, 1.]])
    traj = trajectories.TrajectoryPopulationBSplines(objective, wp, init_algo='wavelet')
    traj.random_initialize(popsize=1)
    d = np.sqrt(3)/5
    discretized, wp_idx = traj.discretize(0, discretization_d=d)
    assert all(wp_idx == [0, 3, 6])
    np.testing.assert_almost_equal(discretized, np.repeat(np.array([[0., 0.2, 0.4, 0.5, 0.6, 0.8, 1.]]), 3, axis=0), 2) # 0.5 comes from the center waypoint
    np.testing.assert_almost_equal(traj.get_objectives(), traj.degree*np.arange(traj.size)[None,...])

    traj.individuals[0][1] = 0.26 #small perturbation to move the b-spline away from the center waypoint
    traj.repair()
    discretized, wp_idx = traj.discretize(0, discretization_d=d)
    np.testing.assert_almost_equal(discretized[:,wp_idx[1]], np.array([0.5, 0.5, 0.5]))


@mock.patch('trajectories.np.random.random')
@mock.patch('trajectories.wavelet_random_initialization')
@mock.patch('trajectories.legacyUAV_get_npoints')
def test_TrajectoryPopulationWBSplines(lgn, wri, rand):

    objective = Objective()
    rand.return_value = np.array([0.]) # effective dimension will be 0
    lgn.return_value = ([(1,)], [0,3])
    wp = np.array([[0., 0.5, 1.], [0., 0.5, 1.], [0., 0.5, 1.]])
    u_wp =  [0., 0.5, 1.]
    swt = sw.SplineWaveletTransform(constraints=[(0, u) for u in u_wp])
    _, c, _ = sw.splmake(np.array([[[0., 0.25, 0.75, 1.], [0., 0.25, 0.75, 1.], [0., 0.25, 0.75, 1.]]]))[0]
    splw = [c, np.array([[[1.]]]), np.array([[[1.,-1.]]])] # noisy wavelets for d > 0 should cause no effect
    tck = (sw.uniform_knot_array(7, 3), swt.inverse(splw)[0][0], 3)
    wri.return_value = (tck, np.array(u_wp))
    traj = trajectories.TrajectoryPopulationWBSplines(objective, wp)
    traj.random_initialize(popsize=1)
    d = np.sqrt(3)/5
    discretized, wp_idx = traj.discretize(0, discretization_d=d)
    assert all(wp_idx == [0, 3, 6])
    np.testing.assert_almost_equal(discretized, np.repeat(np.array([[0., 0.2, 0.4, 0.5, 0.6, 0.8, 1.]]), 3, axis=0), 2) # 0.5 comes from the center waypoint
    np.testing.assert_almost_equal(traj.get_objectives(), traj.degree*np.arange(traj.size)[None,...])

    splw = [c, np.array([[[0.]]]), np.array([[[1.,-1.]]])] # noisy wavelets for d > 1 should cause no effect
    tck = (sw.uniform_knot_array(7, 3), swt.inverse(splw)[0][0], 3)
    traj = trajectories.TrajectoryPopulationWBSplines(objective, wp)
    traj.init_fromdata(np.array([1.]),tck, u_wp)
    d = np.sqrt(3)/5
    discretized, wp_idx = traj.discretize(0, discretization_d=d)
    assert all(wp_idx == [0, 3, 6])
    np.testing.assert_almost_equal(discretized, np.repeat(np.array([[0., 0.2, 0.4, 0.5, 0.6, 0.8, 1.]]), 3, axis=0), 2) # 0.5 comes from the center waypoint

    traj.individuals[0][2] = 0.26 #small perturbation to move the b-spline away from the center waypoint
    traj.repair()
    discretized, wp_idx = traj.discretize(0, discretization_d=d)
    np.testing.assert_almost_equal(discretized[:,wp_idx[1]], np.array([0.5, 0.5, 0.5]))

    splw = [c, np.array([[[0.]]]), np.array([[[0.,0.]]])]
    tck = (sw.uniform_knot_array(7, 3), swt.inverse(splw)[0][0], 3)
    traj = trajectories.TrajectoryPopulationWBSplines(objective, wp)
    traj.init_fromdata(np.array([2.]),tck, u_wp)
    d = np.sqrt(3)/5
    discretized, wp_idx = traj.discretize(0, discretization_d=d)
    assert all(wp_idx == [0, 3, 6])
    np.testing.assert_almost_equal(discretized, np.repeat(np.array([[0., 0.2, 0.4, 0.5, 0.6, 0.8, 1.]]), 3, axis=0), 2) # 0.5 comes from the center waypoint
