import numpy as np
from nondominatedsorting import *
from nondominatedsorting import _SplitBy, _NDHelperA, _NDHelperB, _SweepA, _SweepB
    
def test_dominates():
    x0 = np.array([1,0])
    x1 = np.array([0,1])
    x2 = np.array([2,2])
    assert dominates(x0,x1,[]) == False
    assert dominates(x1,x0,[]) == False
    assert dominates(x0,x2,[]) == True
    assert dominates(x1,x2,[]) == True
    assert dominates(x2,x1,[]) == False
    
    prio = [(0,1),(1,2)]
    assert dominates(x0,x1,prio) == False
    assert dominates(x1,x0,prio) == True
    assert dominates(x0,x2,prio) == True
    assert dominates(x1,x2,prio) == True

    prio = [(0,1),(1,3)]
    x0 = np.array([1,0,0])
    x1 = np.array([0,0,1])
    x2 = np.array([0,2,2])
    assert dominates(x0,x1,prio) == False
    assert dominates(x1,x0,prio) == True
    assert dominates(x0,x2,prio) == False
    assert dominates(x1,x2,prio) == True
    assert dominates(x2,x0,prio) == True

def test_prio_distance():
    # mono-objective
    x = np.array([0,1])
    y = np.array([1,3])
    np.testing.assert_equal(prio_distance(x,y), np.array([1,2]))

    # multi-objective
    x = np.array([[0,0]])
    y = np.array([[1,1]])
    np.testing.assert_almost_equal(prio_distance(x,y), np.array([np.sqrt(2)]))

    # priorities
    x = np.array([[0,0,0]])
    y = np.array([[0,1,2]])
    prio = [(0,2),(2,3)]
    np.testing.assert_almost_equal(prio_distance(x,y,prio), np.array([1]))

    x = np.array([[0,0,0]])
    y = np.array([[0,0,2]])
    prio = [(0,2),(2,3)]
    np.testing.assert_almost_equal(prio_distance(x,y,prio), np.array([2]))


def test_SplitBy():
    S = np.array([0])
    x = np.array([[0]])
    m = 0
    k = 0
    L, M, H, LM = _SplitBy(S, m, k, x)
    assert M == np.array([0])

    S = np.array([3, 2, 1, 0])
    x = np.array([[0,0], [0,1], [0,2], [1,3]])
    m = 1
    k = 1
    L, M, H, LM = _SplitBy(S, m, k, x)
    assert L == np.array([0])
    assert M == np.array([1])
    np.testing.assert_array_equal(LM,np.array([1,0]))
    np.testing.assert_array_equal(H,np.array([3,2]))


def test_SweepB():
    # example from fig 4 Jensen et al (2003)
    x = np.array([[0,2], [1,8], [2,6], [4,1], [4,5], [6,3], [7,2], [3,4], [3,7], [5,2], [5,8], [7,7]])
    L = np.array([0,1,2,3,4,5,6])
    H = np.array([7,8,9,10,11])
    front = np.array([1,4,2,1,2,4,2,1,2,2,3,2])
    priorities = [(0, x.shape[1])]
    plevel = 0
    _SweepB(L, H, plevel, x, priorities, front)
    np.testing.assert_array_equal(front, np.array([1,4,2,1,2,4,2, 2,3,2,5,5]))


def test_NDHelperB():
    # empty case
    _NDHelperB(np.array([]),np.array([]),0,0,np.array([]),[(0,0)],np.array([]))

    # one set contains only one member
    x = np.array([[0,0,1],[0,1,0],[0,1,1]])
    L = np.array([0,1])
    H = np.array([2])
    front = [1,2,0]
    priorities = [(0,x.shape[1])]
    plevel = 0
    k = 2
    _NDHelperB(L,H,k,plevel,x,priorities,front)
    np.testing.assert_array_equal(front, np.array([1,2,3]))

    # only two objectives remain
    x = np.array([[0,0,1],[0,1,0],[0,1,1],[0,2,2]])
    L = np.array([0,1])
    H = np.array([2,3])
    front = [1,2,0,0]
    priorities = [(0,1), (1,x.shape[1])]
    plevel = 1
    k = 1
    _NDHelperB(L,H,k,plevel,x,priorities,front)
    np.testing.assert_array_equal(front, np.array([1,2,3,3]))

    # all L members have objective k smaller than all H members
    x = np.array([[1,0,0],[0,1,0],[1,1,1],[2,2,1]])
    L = np.array([0,1])
    H = np.array([2,3])
    front = [1,2,0,0]
    priorities = [(0,x.shape[1])]
    plevel = 0
    k = 2
    _NDHelperB(L,H,k,plevel,x,priorities,front)
    np.testing.assert_array_equal(front, np.array([1,2,3,3]))

    # some (but not all) L member has objective k smaller than any H member
    x = np.array([[1,0,0],[0,1,1],[0,0,2],[1,1,0],[1,1,1],[2,2,2]])
    L = np.array([0,1,2])
    H = np.array([3,4,5])
    front = [1,2,3,0,0,0]
    priorities = [(0,x.shape[1])]
    plevel = 0
    k = 2
    _NDHelperB(L,H,k,plevel,x,priorities,front)
    np.testing.assert_array_equal(front, np.array([1,2,3,2,3,4]))


def test_SweepA():
    # basic test
    x = np.array([[2,5], [1,4], [0,5]])
    S = np.array([2,1,0])
    front = np.array([0,0,0])
    plevel = 0
    priorities = [(0,x.shape[1])]
    _SweepA(S, plevel, x, priorities, front)
    np.testing.assert_array_equal(front, np.array([1,0,0]))

    # slightly more complicated test
    x = np.array([[0,0,5], [0,1,4], [0,2,5], [0,3,4], [0,4,5]])
    S = np.array([0, 1, 2, 3, 4])
    front = np.array([1, 2, 3, 0, 0])
    plevel = 1
    priorities = [(0, 1),(1, x.shape[1])]
    _SweepA(S, plevel, x, priorities, front)
    np.testing.assert_array_equal(front, np.array([1, 2, 3, 3, 4]))

    # test recursion basic case (all on the same point)
    x = np.array([[0,0,0,5], [0,0,1,4], [0,0,2,5], [0,0,3,4], [0,0,4,5]])
    S = np.array([0, 1, 2, 3, 4])
    front = np.array([1, 2, 3, 0, 0])
    plevel = 0
    priorities = [(0, 2),(2, x.shape[1])]
    _SweepA(S, plevel, x, priorities, front)
    np.testing.assert_array_equal(front, np.array([1, 2, 3, 3, 4]))

    # test how recursion affects the next pareto front
    x = np.array([[0,0,0,5], [0,0,1,4], [0,0,2,5], [0,0,3,4], [0,0,4,5], [1,1,0,0]])
    S = np.array([0, 1, 2, 3, 4, 5])
    front = np.array([1, 2, 3, 0, 0, 0])
    plevel = 0
    priorities = [(0, 2),(2, x.shape[1])]
    _SweepA(S, plevel, x, priorities, front)
    np.testing.assert_array_equal(front, np.array([1, 2, 3, 3, 4, 5]))


def test_NDHelperA():
    # empty case
    _NDHelperA(np.array([]),0,0,np.array([]),[(0,0)],np.array([]))

    # just two solutions
    x = np.array([[0,0,1],[0,1,1]])
    S = np.array([0,1])
    front = [1,0]
    priorities = [(0,x.shape[1])]
    plevel = 0
    k = 2
    _NDHelperA(S,k,plevel,x,priorities,front)
    np.testing.assert_array_equal(front, np.array([1,2]))

    # one objective at the current priority, including recursion
    x = np.array([[0,0,5], [0,1,4], [0,2,5], [0,3,4], [0,4,5], [1,0,0]])
    S = np.array([0, 1, 2, 3, 4, 5])
    front = np.array([0, 0, 0, 0, 0, 0])
    priorities = [(0, 1),(1, x.shape[1])]
    plevel = 0
    k = 0
    _NDHelperA(S,k,plevel,x,priorities,front)
    np.testing.assert_array_equal(front, np.array([0, 0, 1, 1, 2, 3]))

    # all individuals have the same objective k value
    x = np.array([[0,5,0], [1,4,0], [2,5,0], [3,4,0], [4,5,0]])
    S = np.array([0, 1, 2, 3, 4])
    front = np.array([1, 2, 3, 0, 0])
    plevel = 0
    priorities = [(0, x.shape[1])]
    k = 2
    _NDHelperA(S,k,plevel,x,priorities,front)
    np.testing.assert_array_equal(front, np.array([1, 2, 3, 3, 4]))

    x = np.array([[1,0,0], [2,0,0], [1,0,1], [2,0,1], [1,0,2], [2,0,2]])
    S = np.lexsort(x.T[::-1])
    front = np.array([0,0,0,0,0,0])
    plevel = 0
    priorities = [(0, x.shape[1])]
    k = 2
    _NDHelperA(S,k,plevel,x,priorities,front)
    np.testing.assert_array_equal(front, np.array([0, 1, 1, 2, 2, 3]))


def test_pareto_front_sort():
    x = np.array([[0,0],[0,1],[1,1],[1,0],[0.5,0.5],[0.25,0.25]])
    l = pareto_front_sort(x)
    assert [set(i) for i in l] == [set(i) for i in [[0],[1,3,5],[4],[2]]]

    x = np.array([[0,0,0],[0,1,0],[1,0,1],[1,1,0],[0,1,1],[0,0.5,0.5],[0,0.25,0.25]])
    l = pareto_front_sort(x)
    assert [set(i) for i in l] == [set(i) for i in [[0],[1,2,6],[3,5],[4]]]

    prio = [(0,1),(1,3)]
    x = np.array([[0,0,0],[0,-0.1,1],[0,1,1],[1,1,0],[1,0.5,0.5],[1,0.25,0.25]])
    l = pareto_front_sort(x, prio)
    assert [set(i) for i in l] == [set(i) for i in [[0,1],[2],[3,5],[4]]]

    prio = [(0,2),(2,4)]
    x = np.array([[0,0,0,0],[0,0,0,1],[0,0,1,0],[0,1,1,0],[1,0,1,1],[1,1,0.5,0.5],[1,1,0.25,0.25]])
    l = pareto_front_sort(x, prio)
    assert [set(i) for i in l] == [set(i) for i in [[0],[1,2],[3,4],[6],[5]]]

    prio = [(0,1),(1,4)]
    l = pareto_front_sort(x, prio)
    assert [set(i) for i in l] == [set(i) for i in [[0],[1,2],[3],[4,6],[5]]]

    prio = [(0,3),(3,4)]
    l = pareto_front_sort(x, prio)
    assert [set(i) for i in l] == [set(i) for i in [[0], [1], [2, 6], [3, 4, 5]]]

    np.random.seed(0)

    for i in xrange(10):
        x = np.random.random((100,2+i))
        l1 = pareto_front_sort(x)
        l2 = pareto_front_sort_classic(x)
        assert [set(f) for f in l1] == [set(f) for f in l2]

        p = np.random.randint(1,2+i)
        l1 = pareto_front_sort(x,[(0,p),(p,2+i)])
        l2 = pareto_front_sort_classic(x,[(0,p),(p,2+i)])
        assert [set(f) for f in l1] == [set(f) for f in l2]

        x = np.random.randint(0,10,(100,2+i))
        l1 = pareto_front_sort(x)
        l2 = pareto_front_sort_classic(x)
        assert [set(f) for f in l1] == [set(f) for f in l2]

        prio = [(0,p),(p,2+i)]
        l1 = pareto_front_sort(x,prio)
        l2 = pareto_front_sort_classic(x,prio)
        assert [set(f) for f in l1] == [set(f) for f in l2]

    for i in xrange(10):
        x = np.random.random((100,5+i))
        p1 = np.random.randint(1,5+i-2)
        p2 = np.random.randint(p1+1,5+i)
        assert [set(f) for f in pareto_front_sort(x,[(0,p1),(p1,p2),(p2,5+i)])] == [set(f) for f in pareto_front_sort_classic(x,[(0,p1),(p1,p2),(p2,5+i)])]

        x = np.random.randint(0,10,(100,5+i))
        assert [set(f) for f in pareto_front_sort(x,[(0,p1),(p1,p2),(p2,5+i)])] == [set(f) for f in pareto_front_sort_classic(x,[(0,p1),(p1,p2),(p2,5+i)])]

    x = np.array([[    0.    ,     1.384 ,     0.    ,  2242.6584],
                  [    0.75  ,     1.126 ,     1.    ,  2442.7367],
                  [    0.75  ,     1.112 ,     1.    ,  2598.5485],
                  [    0.    ,     1.249 ,     1.    ,  2549.5598],
                  [    0.    ,     1.249 ,     1.    ,  3067.8085]])
    prio = [(0,2),(2,4)]
    l1 = pareto_front_sort_classic(x, prio)
    l2 = pareto_front_sort(x, prio)
    assert [set(i) for i in l1] == [set(i) for i in l2]

