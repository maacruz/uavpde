import numpy as np
import mock
import populations

def f1(x):
    return np.sum(x*x, 1)


class MockRand:
    def __init__(self):
        self.r = 0.0
    def mockrand(self, s):
        self.r += 0.1
        return np.linspace(self.r, self.r+0.1, s)

@mock.patch('populations.np.random.rand', MockRand().mockrand)
def test_DefaultPopulation():
    vdim = 2
    NP = 3
    limits = [(-100, 100)] * vdim
    # object creation and initialization
    pop = populations.DefaultPopulation(f1, 1, vdim)
    pop.random_initialize(NP, limits, valid_range=limits)

    assert pop.size == NP

    mockrand = MockRand().mockrand
    vecs = -100 + 200 * np.array([mockrand(NP) for i in xrange(vdim)]).T
    np.testing.assert_allclose(pop.get_vectors(), vecs)

    np.testing.assert_allclose(pop.get_objectives().ravel(), f1(vecs))

    vecs = vecs / 2
    pop.set_vectors(vecs.copy())
    np.testing.assert_allclose(pop.get_vectors(), vecs)

    pop.join(pop)
    assert pop.size == NP * 2
    assert np.all(pop.get_vectors()[:NP] == pop.get_vectors()[NP:])

    x = np.array([False] * pop.size)
    x[0:NP] = True
    pop.remove(x)
    assert pop.size == NP
    assert np.any(np.all(pop.get_vectors() == vecs[0], 1))
    assert np.any(np.all(pop.get_vectors() == vecs[1], 1))
    assert np.any(np.all(pop.get_vectors() == vecs[2], 1))

    pop.set_vectors(vecs.copy())
    pop.remove([0])
    assert pop.size == NP - 1
    assert np.all(np.any(pop.get_vectors() != vecs[0], 1))

    pop.set_vectors(vecs[:NP - 1].copy())
    pop.remove(np.array([0]))
    assert np.all(np.any(pop.get_vectors() != vecs[0], 1))

    v = pop.get_vectors()
    v[0] = -1000, 1000
    pop.repair()
    assert v[0, 0] >= -100 and v[0, 1] <= 100 and v[0, 0] <= 100 and v[0, 1] >= -100


def test_Archive():
    vdim = 2
    NP = 3
    pop = populations.DefaultPopulation(f1,1, vdim)
    pop.random_initialize(NP,[(-100,100)]*vdim)
    arch = populations.DefaultArchive(NP+1,pop)
    for i in xrange(NP):
        arch.append(pop, [i])
        assert arch.size == i+1
    arch.append(pop,[0, 1])
    assert arch.size == NP+1
    np.testing.assert_equal(arch.get_vectors()[0], pop.get_vectors()[1])

