import scipy.io as scio
import numpy as np
from utils import jit

R_earth = 6371.0e3 #earth radius in m
abignumber = 1e30

# DTED map class
class DTEDmap(object):
    """
    Holds a dted map, its geographical coordinates, and distance between points in both axis (in m)
    """
    def __init__(self,size=(1,1)):
        """
        Input: 
            size=(0,0): (int,int) # height and width of the initial dted data array
        """
        self.dted = np.zeros(size, dtype=np.float32) #dted data array
        self.dx = 1.0 #horizontal step m per grid point
        self.dy = 1.0 #vertical step m per grid point
        self.gridcellsDeg = 2*np.pi*R_earth/360.0 #?? Number of grid cells in a degree
        self.Lat = 0.0 #?? Map Latitude
        self.Long = 0.0 #?? Map Longitude
        # geographical coordinates in m
        self.x_west = 0.0 
        self.x_east = self.dted.shape[1]*self.dx
        self.y_south = 0.0
        self.y_north = self.dted.shape[0]*self.dy
        # limits for UAVs leaving a 0.5% wide band around
        self.x_westlimit = self.x_west + self.dted.shape[1]*self.dx/200
        self.x_eastlimit = self.x_east - self.dted.shape[1]*self.dx/200
        self.y_southlimit = self.y_south + self.dted.shape[0]*self.dy/200
        self.y_northlimit = self.y_north - self.dted.shape[0]*self.dy/200
        self.poi = {} #dictionary to store POI
        
    def loadMat(self, fname):
        """
        Load DTED data from a matlab file
        Input:
            fname: string #file path
        """
        matdata = scio.loadmat(fname)
        self.dted = np.nan_to_num(matdata['map'].astype(np.float32)) #DTED (in m)
        self.dx = float(matdata['dx'][0]) #horizontal step in m between data points
        self.dy = float(matdata['dy'][0]) #vertical step in m between data points
        if matdata['mapLegend'].size > 0:
            #FIXME: Nothing below this point makes much sense. Geographical coordinates aren't used anyway
            self.gridcellsDeg = float(matdata['mapLegend'][0][0]) #?? Number of grid cells in a degree
            self.Lat = float(matdata['mapLegend'][0][1])-float(matdata['tamLatg'][0]) #?? Map Latitude = northern Latitude limit - ???
            self.Long = float(matdata['mapLegend'][0][2])-float(matdata['tamLong'][0]) #?? Map Longitude = western Longitude limit - ???
        # feign cartesian coordinates
        self.x_west = 0.0
        self.x_east = self.dted.shape[1]*self.dx
        self.y_south = 0.0
        self.y_north = self.dted.shape[0]*self.dy
        # limits for UAVs leaving a 0.5% wide band around
        self.x_westlimit = self.x_west + self.dted.shape[1]*self.dx/200
        self.x_eastlimit = self.x_east - self.dted.shape[1]*self.dx/200
        self.y_southlimit = self.y_south + self.dted.shape[0]*self.dy/200
        self.y_northlimit = self.y_north - self.dted.shape[0]*self.dy/200
        
    def setPOI(self, poi_id, poi_x, poi_y, poi_z=None, poi_viewlimit=np.inf):
        """
        Register a POI in this map. This will trigger the calculation of a viewshed for the POI
        Input:
            poi_id: POIid #POI identificator
            poi_x, poi_y: float #POI coordinates in m
            poi_z=None: float #POI absolute height in m. If None it is the map height at the POI coordinates
            poi_viewlimit=np.inf: float #POI view range in m
        """
        if not(poi_z):
            poi_z = self.getMapHeight(poi_x, poi_y)
        viewshed, x1, x2, y1, y2 = compute_viewshed(self, poi_x, poi_y, poi_z, poi_viewlimit)
        self.poi[poi_id] = (poi_x, poi_y, poi_z, poi_viewlimit, viewshed, x1, x2, y1, y2)
    
    def isVisibleFromPOI(self, x, y, z, poi_id):
        """
        Checks if the given point is visible from the given POI
        Input:
            x, y, z: float[n] or float #point(s) coordinates
            poi_id: POIid #POI identificator
        Output:
            boolean[n] or boolean # True if visible
        """
        poi_x, poi_y, poi_z, poi_viewlimit, viewshed, x1, x2, y1, y2 = self.poi[poi_id]
        if isinstance(x, np.ndarray):
            #d2 = (x-poi_x)**2+(y-poi_y)**2+(z-poi_z)**2
            #inrange_idx = np.nonzero((d2<=poi_viewlimit**2) & (self.x_west<=x) & (x<self.x_east) & (self.y_south<=y) & (y<self.y_north))
            #vheight = bilinear_interpolate(viewshed, (x[inrange_idx]-self.x_west)/self.dx-x1, (y[inrange_idx]-self.y_south)/self.dy-y1)
            #los = np.zeros(x.size, dtype=np.bool)
            #los[inrange_idx] = (z[inrange_idx]>=vheight)
            los = _isVisibleFromPOI(x, y, z, poi_x, poi_y, poi_z, poi_viewlimit, viewshed, x1, x2, y1, y2, self.x_west, self.x_east, self.y_south, self.y_north, self.dx, self.dy)
        else:
            d2 = (x-poi_x)**2+(y-poi_y)**2+(z-poi_z)**2
            los = ((d2<=poi_viewlimit**2) & (self.x_west<=x) & (x<self.x_east) & (self.y_south<=y) & (y<self.y_north)) & (z>=bilinear_interpolate(viewshed, (x-self.x_west)/self.dx-x1, (y-self.y_south)/self.dy-y1))
        return los
    
    def getMapHeight(self, x, y):
        """
        Return the height at the given coordinates
        Input:
            x, y: float[n] or float #coordinates in m
        Output:
            float[n] or float
        """
        return bilinear_interpolate(self.dted, (x-self.x_west)/self.dx, (y-self.y_south)/self.dy)

    def getGridCoordinates(self, x, y):
        """
        Return the given coordinates transformed to grid cell coordinates
        Input:
            x, y: float[n] or float #coordinates in m
        Output:
            (float[n],float[n]) or (float,float) # (x,y) coordinates on the grid
        """
        return (x-self.x_west)/self.dx, (y-self.y_south)/self.dy

@jit
def _isVisibleFromPOI(x, y, z, poi_x, poi_y, poi_z, poi_viewlimit, viewshed, x1, x2, y1, y2, x_west, x_east, y_south, y_north, dx, dy):
    d2 = (x-poi_x)**2+(y-poi_y)**2+(z-poi_z)**2
    inrange_idx = np.nonzero((d2<=poi_viewlimit**2) & (x_west<=x) & (x<x_east) & (y_south<=y) & (y<y_north))[0]
    vheight = z[inrange_idx]
    los = np.zeros_like(x, dtype=np.bool8)
    _bilinear_interpolate(viewshed, (x[inrange_idx]-x_west)/dx-x1, (y[inrange_idx]-y_south)/dy-y1, vheight)
    los[inrange_idx] = (z[inrange_idx] >= vheight)
    return los

def bilinear_interpolate(im, x, y):
    """
    Bilinear interpolation of a point on a 2D sampled surface
    Input:
        im: float[:,:] # 2D array
        x,y: float[n] or float # point coordinates
    Output:
        float[n] #interpolated value
    """
    if im.size == 0:
        return np.nan
    x = np.atleast_1d(x)
    y = np.atleast_1d(y)
    z = np.empty_like(x)
    _bilinear_interpolate(im, x, y, z)
    return z

try:
    @jit(nopython=True)
    def _bilinear_interpolate(im, x, y, z):
        # prevent out of bounds array access
        Ia = Ib = Ic = Id = 0.0
        for i in xrange(x.size):
            x0 = min(max(0, np.int32(np.floor(x[i]))), im.shape[1]-1)
            x1 = min(x0 + 1, im.shape[1]-1)
            y0 = min(max(0, np.int32(np.floor(y[i]))), im.shape[0]-1)
            y1 = min(y0 + 1, im.shape[0]-1)
            # grid values around the point to be interpolated
            Ia = im[y0, x0]
            Ic = im[y0, x1]
            Ib = im[y1, x0]
            Id = im[y1, x1]
            # bilinear coeficients
            wa = (x1-x[i]) * (y1-y[i])
            wb = (x1-x[i]) * (y[i]-y0)
            wc = (x[i]-x0) * (y1-y[i])
            wd = (x[i]-x0) * (y[i]-y0)
            z[i] = wa*Ia + wb*Ib + wc*Ic + wd*Id
        return
except:
    def _bilinear_interpolate(im, x, y, z):
        # grid coordinates around the point to be interpolated
        x0 = np.floor(x).astype(int)
        x1 = x0 + 1
        y0 = np.floor(y).astype(int)
        y1 = y0 + 1
        
        # prevent out of bounds array access
        np.clip(x0, 0, im.shape[1]-1, x0);
        np.clip(x1, 0, im.shape[1]-1, x1);
        np.clip(y0, 0, im.shape[0]-1, y0);
        np.clip(y1, 0, im.shape[0]-1, y1);
        
        # grid values around the point to be interpolated
        Ia = im[y0, x0]
        Ib = im[y1, x0]
        Ic = im[y0, x1]
        Id = im[y1, x1]

        # bilinear coeficients
        wa = (x1-x) * (y1-y)
        wb = (x1-x) * (y-y0)
        wc = (x-x0) * (y1-y)
        wd = (x-x0) * (y-y0)

        z[:] = wa*Ia + wb*Ib + wc*Ic + wd*Id
        return
    
def _R2_compute_viewshed_y(poi_gridx, poi_gridy, poi_h, dted_dh, grid_ar, viewshed, R2d):
    """
    Internal function for compute_viewshed()
    Compute part of the viewshed sweeping the vertical axis
    Input:
        poi_gridx, poi_gridy: float # poi coordinates in grid units
        dted_dh: float[n,m] #dted data array already curvature corrected
        grid_ar: float #grid aspect ratio x/y
        viewshed: float[n,m] #work array to hold the viewshed height. First initialization needs to be -abignumber
        R2d: float[n,m] #work array to hold the distance of the point used to aproximate the corresponding viewshed grid point. First initialization needs to be np.inf
    Result:
        R2d is updated
    """
    tgalpha_max = np.empty(viewshed.shape[1]) # temporary array to hold the max slope of the rays
    xp_edge = np.arange(0,viewshed.shape[1], dtype=np.float32)
    for sweepdir in (-1,1): #loop over both directions of the sweep
        tgalpha_max[...] = -np.inf
        #loop over rows of grid points from the poi to a dted view limit edge
        if sweepdir == -1: # from poi to lower edge
            y_edge = np.float32(0)
            y0 = int(np.ceil(poi_gridy-1.0))
            y1 = -1
        else: # from poi to upper edge
            y_edge = np.float32(viewshed.shape[0] - 1)
            y0 =  int(np.floor(poi_gridy+1.0))
            y1 = viewshed.shape[0]
        if y0!=y1: # avoid a division by 0 warning
            m = (xp_edge - poi_gridx)/(y_edge - poi_gridy) # direction of the rays
        for y in xrange(y0,y1,sweepdir):
            # update the max slope of the rays up to the current row
            x = poi_gridx + m*(y - poi_gridy) # intersection coord of the rays with the dted row
            h = np.interp(x, xp_edge, dted_dh[y,:]-poi_h) # interpolated elevation at the intersection point
            d = np.sqrt(((x - poi_gridx)*grid_ar)**2 + ((y - poi_gridy))**2)
            tgalpha_max = np.maximum(tgalpha_max, h/d)
            hv = tgalpha_max*d + poi_h #minimum line of sight height
            # find the closest ray to each grid point and assign its line of sight height to the grid point
            xg_1 = np.int32(np.floor(x)) # grid point at the left of each ray
            xg_2 = np.int32(np.ceil(x)) # grid point at the right of each ray
            # check each ray against the one at the left
            closest = np.empty(x.size, dtype=np.bool)
            dx_1 = np.abs(x - xg_1) #distance from each ray to the closest left grid point
            closest[1:] = (dx_1[1:] < np.abs(x[:-1] - xg_1[1:])) #check if the current ray is closer to its left grid point than the ray just at its left
            closest[0] = True # leftmost ray
            closest &= (dx_1*grid_ar < R2d[y,xg_1])
            viewshed[y,:][xg_1[closest]] = hv[closest] #if so assign the line of sight height of the current ray to the left grid point
            R2d[y,:][xg_1[closest]] = grid_ar*dx_1[closest] #store the assigned rays distance to the grid point
            # check each ray against its right
            dx_2 = np.abs(x - xg_2) #distance from each ray to the closest right grid point
            closest[:-1] = (dx_2[:-1] < np.abs(x[1:] - xg_2[:-1])) #check if the current ray is closer to its right grid point than the ray just at its right
            closest[-1] = True
            closest &= (dx_2*grid_ar < R2d[y,xg_2])
            viewshed[y,:][xg_2[closest]] = hv[closest]
            R2d[y,:][xg_2[closest]] = grid_ar*dx_2[closest]

def _R2_compute_viewshed_x(poi_gridx, poi_gridy, poi_h, dted_dh, grid_ar, viewshed, R2d):
    """
    Internal function for compute_viewshed()
    Compute part of the viewshed sweeping the horizontal axis
    """
    tgalpha_max = np.empty(viewshed.shape[0]) # temporary array to hold the max slope of the rays
    yp_edge = np.arange(0,viewshed.shape[0], dtype=np.float32)
    for sweepdir in (-1,1): #loop over both directions of the sweep
        tgalpha_max[...] = -np.inf
        #loop over rows of grid points from the poi to a dted view limit edge
        if sweepdir == -1: # from poi to left edge
            x_edge = np.float32(0)
            x0 = int(np.ceil(poi_gridx-1.0))
            x1 = -1
        else: # from poi to right edge
            x_edge = np.float32(viewshed.shape[1] - 1)
            x0 =  int(np.floor(poi_gridx+1.0))
            x1 = viewshed.shape[1]
        if x0!=x1: # avoid a division by 0 warning
            m = (yp_edge - poi_gridy)/(x_edge - poi_gridx) # direction of the rays
        for x in xrange(x0,x1,sweepdir):
            # update the max slope of the rays up to the current row
            y = poi_gridy + m*(x - poi_gridx) # intersection coord of the rays with the dted row
            h = np.interp(y, yp_edge, dted_dh[:,x]-poi_h) # interpolated elevation at the intersection point
            d = np.sqrt(((x - poi_gridx)*grid_ar)**2 + (y - poi_gridy)**2)
            tgalpha_max = np.maximum(tgalpha_max, h/d)
            hv = tgalpha_max*d+poi_h #minimum line of sight height
            # find the closest ray to each grid point and assign its line of sight height to the grid point
            yg_1 = np.int32(np.floor(y)) # grid point below each ray
            yg_2 = np.int32(np.ceil(y)) # grid point at the top of each ray
            # check each ray against the one below it
            closest = np.empty(y.size, dtype=np.bool)
            dy_1 = np.abs(y - yg_1) #distance from each ray to the closest lower grid point
            closest[1:] = (dy_1[1:] < np.abs(y[:-1] - yg_1[1:])) #check if the current ray is closer to its lower grid point than the ray just below it
            closest[0] = True
            closest &= (dy_1 < R2d[yg_1,x]) 
            viewshed[:,x][yg_1[closest]] = hv[closest] #if so assign the line of sight height of the current ray to the lower grid point
            R2d[:,x][yg_1[closest]] = dy_1[closest] #store the the assigned rays distance to the grid point
            # check each ray against the one above it
            dy_2 = np.abs(y - yg_2) #distance from each ray to the closest upper grid point
            closest[:-1] = (dy_2[:-1] < np.abs(y[1:] - yg_2[:-1])) #check if the current ray is closer to its upper grid point than the ray just above it
            closest[-1] = True
            closest &= (dy_2 < R2d[yg_2,x]) 
            viewshed[:,x][yg_2[closest]] = hv[closest]
            R2d[:,x][yg_2[closest]] = dy_2[closest]

def compute_viewshed(dtedmap, poi_x, poi_y, poi_h=None, viewlimit=np.inf):
    """
    Compute the viewshed height on a dted map given the viewpoint coordinates and an optional view limit radius
    The viewshed height is an approximation computed using an R2 algorithm
    Input:
        dtedmap: DTEDmap() # a dtedmap object
        poi_x, poi_y, poi_z: float # viewpoint coordinates
        viewlimit=np.inf: float # view limit radius in m
    Output:
        np.array[:,:] # dted submap
        x1, x2, y1, y2: int #dted grid bounding rectangle for the submap (where the submap is on the dted map)
    """
    if not(poi_h):
        poi_h = dtedmap.getMapHeight(poi_x, poi_y)
    dx, dy = float(dtedmap.dx), float(dtedmap.dy)
    # calculate the position of the poi on the grid
    poi_gridx, poi_gridy = dtedmap.getGridCoordinates(poi_x, poi_y)

    # calculate the grid rectangle bounding the poi view
    x1 = int(max(0, np.floor(poi_gridx - viewlimit/dx)))
    x2 = int(min(dtedmap.dted.shape[1], np.ceil(poi_gridx + viewlimit/dx) + 1))
    y1 = int(max(0, np.floor(poi_gridy - viewlimit/dy)))
    y2 = int(min(dtedmap.dted.shape[0], np.ceil(poi_gridy + viewlimit/dy) + 1))
    xy = np.mgrid[y1:y2,x1:x2]
    d = np.sqrt(((xy[0]-poi_gridy)*dy)**2 + ((xy[1]-poi_gridx)*dx)**2) #distance from poi to each grid point
    dh = (1/np.cos(d/R_earth) - 1)*R_earth #Earth curvature elevation correction
    dted_dh = dtedmap.dted[y1:y2,x1:x2] - dh #visible dted corrected by Earth curvature
    viewshed = np.empty(dted_dh.shape, dtype=np.float32) # array to hold the result
    viewshed[...] = -abignumber #first initialization
    R2d = np.empty(dted_dh.shape, dtype=np.float32) # temporary array to store the ray distance to the grid point in the R2 algorithm
    R2d[...] = np.inf #first initialization
    _R2_compute_viewshed_y(poi_gridx - x1, poi_gridy - y1, poi_h, dted_dh, dx/dy, viewshed, R2d) #R2 x sweep
    _R2_compute_viewshed_x(poi_gridx - x1, poi_gridy - y1, poi_h, dted_dh, dx/dy, viewshed, R2d) #R2 y sweep
    viewshed += dh #include curvature correction in the viewshed so trajectories don't need to be corrected
    return viewshed, x1, x2, y1, y2
