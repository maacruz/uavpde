import numpy as np
import differentialevolution as de
from populations import DefaultArchive, DefaultPopulation
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

def sphere(x):
    """Sphere test objective function. Continuous unimodal
    Initial range [-100,100]
    """
    return np.sum(x**2, 1)

def schwefel2_22(x):
    """Schefel 2.22. Continuous unimodal
    Initial range [-10, 10]
    """
    xabs = np.abs(x)
    return np.sum(xabs, 1) + np.prod(xabs, 1)
    
def schwefel1_2(x):
    """Schefel 1.2. Continuous unimodal
    Initial range [-100, 100]
    """
    f = np.zeros(len(x))
    for i in xrange(x.shape[1]):
        f += np.sum(x[:,:i+1],1)**2
    return f
    
def schwefel2_21(x):
    """Schefel 2.21. Continuous unimodal
    Initial range [-100, 100]
    """
    return np.max(x,1)
    
def rosenbrock(x):
    """Rosenbrock test objective function. Unimodal for dim(x) = 2 or 3
    Initial range [-30, 30]
    """
    x1 = x[:,:-1]
    x2 = x[:,1:]
    return np.sum(100 * (x1**2 - x2)**2 + (1. - x1)**2, 1)

def step(x):
    """Step  test objective function. Discontinuous
    Initial range [-100, 100]
    """
    return np.sum(np.floor(x + 0.5)**2, 1)
    
def noisy_quartic(x):
    """Noisy quartic objective function.
    Initial range [-1.28, 1.28]
    """
    return np.sum(np.arange(1,x.shape[1]+1)*x**4, 1) + np.random.random()
    
def cigar(x):
    """Cigar test objective function.
    """
    return x[:,0]**2 + 1e6 * np.sum(x**2, 1)

def h1(x):
    """ Simple two-dimensional function containing several local maxima.
    From: The Merits of a Parallel Genetic Algorithm in Solving Hard 
    Optimization Problems, A. J. Knoek van Soest and L. J. R. Richard 
    Casius, J. Biomech. Eng. 125, 141 (2003)
    """
    num = (np.sin(x[:,0] - x[:,1] / 8))**2 + (np.sin(x[:,1] + x[:,0] / 8))**2
    denum = ((x[:,0] - 8.6998)**2 + (x[:,1] - 6.7665)**2)**0.5 + 1
    return num / denum

 # 
# Multimodal
def ackley(x):
    """Ackley test objective function.
    """
    N = x.shape[1]
    return 20 - 20 * np.exp(-0.2*np.sqrt(1.0/N * np.sum(x**2, 1))) \
            + np.e - np.exp(1.0/N * np.sum(cos(2*np.pi*x), 1))
            
def bohachevsky(x):
    """Bohachevsky test objective function.
    """
    x1 = x[:,:-1]
    x2 = x[:,1:]
    return np.sum(x1**2 + 2*x2**2 - 0.3*np.cos(3*np.pi*x1) - 0.4*np.cos(4*np.pi*x2) + 0.7, 1)

def griewank(x):
    """Griewank test objective function.
    """
    i = np.arange(x.shape[1])
    return 1.0/4000.0 * np.sum(x**2, 1) - np.prod(np.cos(x/np.sqrt(i+1.0)), 1) + 1
            
def rastrigin(x):
    """Rastrigin test objective function.
    """     
    return 10 * x.shape[1] + np.sum(x**2 - 10*np.cos(2*np.pi*x), 1)

def rastrigin_scaled(x):
    """Scaled Rastrigin test objective function.
    """
    N = x.shape[1]
    i = np.arange(N)
    return 10*N + np.sum((10**(i/(N-1))*x)**2 - 10*np.cos(2*np.pi*10**(i/(N-1))*x), 1)

def rastrigin_skew(x):
    """Skewed Rastrigin test objective function.
    """
    N = x.shape[1]
    return 10*N + np.sum((10*x if x > 0 else x)**2 
                    - 10*cos(2*pi*(10*x if x > 0 else x)) for x in individual),
def schaffer(x):
    """Schaffer test objective function.
    """
    x1 = x[:,:-1]
    x2 = x[:,1:]
    return np.sum((x1**2+x2**2)**0.25 * ((np.sin(50*(x1**2+x2**2)**0.1))**2+1.0), 1)

def schwefel(x):
    """Schwefel test objective function.
    """    
    N = x.shape[1]
    return 418.9828872724339*N-np.sum(x*np.sin(np.sqrt(np.abs(x))), 1)



# Multiobjective benchmark functions

def zdt1(x):
    """
    ZDT1 multiobjective function.
    Input:
        x: np.array[n,:] # with 0<=x<=1
    Output:
        np.array[n,2] # limited to be within the intervals [0,1],[0,10]
    """
    g  = 1.0 + 9.0*np.sum(x[:,1:],1)/(x.shape[1]-1)
    f1 = x[:,0]
    f2 = g * (1 - np.sqrt(f1/g))
    return np.vstack((f1, f2)).T

def zdt2(x):
    """
    ZDT2 multiobjective function.
    Input:
        x: np.array[n,:]
    Output:
        np.array[n,2]
    """
    g  = 1.0 + 9.0*sum(x[:,1:])/(x.shape[1]-1)
    f1 = x[:,0]
    f2 = g * (1 - (f1/g)**2)
    return np.vstack((f1, f2)).T
    
def zdt3(x):
    """
    ZDT3 multiobjective function.
    Input:
        x: np.array[n,:]
    Output:
        np.array[n,2]
    """

    g  = 1.0 + 9.0*sum(x[:,1:])/(x.shape[1]-1)
    f1 = x[:,0]
    f2 = g * (1 - np.sqrt(f1/g) - f1/g * np.sin(10*np.pi*f1))
    return np.vstack((f1, f2)).T

def zdt4(x):
    """
    ZDT4 multiobjective function.
    Input:
        x: np.array[n,:]
    Output:
        np.array[n,2]
    """
    g  = 1 + 10*(x.shape[1]-1) + np.sum(x[:,1:]**2 - 10*np.cos(4*np.pi*x[:,1:]), 1)
    f1 = x[:,0]
    f2 = g * (1 - np.sqrt(f1/g))
    return np.vstack((f1, f2)).T
    
def zdt6(x):
    """
    ZDT6 multiobjective function.
    Input:
        x: np.array[n,:]
    Output:
        np.array[n,2]
    """
    g  = 1 + 9 * (x[:,1:].sum(1) / (x.shape[1]-1))**0.25
    f1 = 1 - np.exp(-4*x[:,0]) * np.sin(6*np.pi*x[:,0])**6
    f2 = g * (1 - (f1/g)**2)
    return np.vstack((f1, f2)).T

def dtlz1(x, n):
    """DTLZ1 mutliobjective function. It returns a tuple of n values. 
    The input vector x must have at least n elements.
    From: K. Deb, L. Thiele, M. Laumanns and E. Zitzler. Scalable Multi-Objective 
    Optimization Test Problems. CEC 2002, p. 825 - 830, IEEE Press, 2002.
    """
    xi = x[:,n-1:]
    g = 100 * (xi.shape[1] + np.sum((xi-0.5)**2 - np.cos(20*np.pi*(xi-0.5)), 1))
    f = [0.5 * x[:,:n-1].prod(1) * (1 + g)]
    f.extend(0.5 * x[:,:m].prod(1) * (1 - x[:,m]) * (1 + g) for m in reversed(xrange(n-1)))
    return np.vstack(f).T

def dtlz2(x, n):
    """DTLZ2 mutliobjective function. It returns a tuple of n values. 
    The input vector x must have at least n elements.
    From: K. Deb, L. Thiele, M. Laumanns and E. Zitzler. Scalable Multi-Objective 
    Optimization Test Problems. CEC 2002, p. 825 - 830, IEEE Press, 2002.
    """
    xc = x[:,:n-1]
    xm = x[:,n-1:]
    g = np.sum((xm-0.5)**2, 1)
    f = [(1.0+g) *  np.prod(np.cos(0.5*xc*np.pi), 1)]
    f.extend((1.0+g) * np.prod(np.cos(0.5*xc[:,:m]*np.pi), 1) * np.sin(0.5*xc[:,m]*np.pi) for m in range(n-2, -1, -1))
    return np.vstack(f).T

def dtlz3(x, n):
    """DTLZ3 mutliobjective function. It returns a tuple of n values. 
    The input vector x must have at least n elements.
    From: K. Deb, L. Thiele, M. Laumanns and E. Zitzler. Scalable Multi-Objective 
    Optimization Test Problems. CEC 2002, p. 825 - 830, IEEE Press, 2002.
    """
    xc = x[:,:n-1]
    xm = x[:,n-1:]
    g = 100 * (xm.shape[1] + np.sum((xm-0.5)**2 - np.cos(20*np.pi*(xm-0.5)), 1))
    f = [(1.0+g) *  np.prod(np.cos(0.5*xc*np.pi), 1)]
    f.extend((1.0+g) * np.prod(np.cos(0.5*xc[:,:m]*np.pi), 1) * np.sin(0.5*xc[:,m]*np.pi) for m in range(n-2, -1, -1))
    return np.vstack(f).T

def dtlz4(x, n, alpha):
    """DTLZ4 mutliobjective function. It returns a tuple of n values. The
    input vector x must have at least n elements. The *alpha* parameter allows
    for a meta-variable mapping in :func:`dtlz2` :math:`x_i \\rightarrow
    x_i^\\alpha`, the authors suggest :math:`\\alpha = 100`.
    From: K. Deb, L. Thiele, M. Laumanns and E. Zitzler. Scalable Multi-Objective 
    Optimization Test Problems. CEC 2002, p. 825 - 830, IEEE Press, 2002.
    """
    xc = x[:,:n-1]
    xm = x[:,n-1:]
    g = np.sum((xm-0.5)**2, 1)
    f = [(1.0+g) *  np.prod(np.cos(0.5*xc**alpha*np.pi), 1)]
    f.extend((1.0+g) * np.prod(np.cos(0.5*xc[:,:m]**alpha*np.pi), 1) * np.sin(0.5*xc[:,m]**alpha*np.pi) for m in range(n-2, -1, -1))
    return np.vstack(f).T

def graph2D(o, pf):
    def update_plt(frame, scat, data, pf):
        #print frame, data[frame], pf[frame]
        c = ['b']*len(data[frame])
        s = np.ones(len(data[frame]))*30
        for i in pf[frame][0]:
            c[i] = 'r'
            s[i] = 60
        scat.set_offsets(data[frame])
        scat.set_color(c)
        scat._sizes = s
        return scat,
    fig = plt.figure()
    p = plt.scatter(o[:,0], o[:,1], c=['b']*len(o), animated=True)
    plt.axis([0,1,0,10])
    ani = FuncAnimation(fig, update_plt, len(o), fargs = (p, o, pf), interval=300, blit=True)
    plt.show()

def run_test_mono(D, NP, f, f_range, ngen, strategy, archsize, muCr=0.5, sigmaCr=0.1):
    pop = DefaultPopulation(NP, D, f, [f_range]*D)
    pop.random_initialize()
    arch = DefaultArchive(archsize, pop)
    opt = de.DE(pop, archive=arch, mutation_strategy=strategy, muCr=muCr, sigmaCr=sigmaCr, c=0.1, alpha=2, pareto_ranked=True, F_min=0.4, F_max=1.0, isSHADE=False)
    obj_mean = []
    obj_median = []
    obj_best = []
    muCr = []
    muF = []
    pf = []
    F_val = np.linspace(0,1,opt.F_multinomial_nbins)
    for i in xrange(ngen):
        pf = opt.run_generation(pf)
        objval = opt.population.get_objectives()
        obj_mean.append(np.mean(objval))
        obj_median.append(np.median(objval))
        obj_best.append(np.min(objval))
        muCr.append(opt.muCr)
        muF.append(np.sum(opt.Fbins*F_val)/opt.Fbins.sum())
    #print opt.Kbins
    return obj_best, obj_mean, obj_median, muCr, muF

def save_figure_mono(nfig, f, flim, D, NP, archsize, nruns, strategy, muCr, sigmaCr, ngen, title, label):
    plt.figure(nfig)
    print title
    for s in strategy:
        for n in NP:
            for a in archsize:
                obj_mean = np.zeros(ngen)
                for i in xrange(nruns):
                    obj_mean += np.array(run_test_mono(D, n, f, flim, ngen, s, archsize=int(a*n), muCr=muCr, sigmaCr=sigmaCr))[0]
                obj_mean /= nruns
                plt.plot(np.log10(obj_mean), label=label(D,n,a,s))
                plt.text(ngen, np.log10(obj_mean[-1]), label(D,n,a,s))
                print "%e %s" % (obj_mean[-1], label(D,n,a,s))
    plt.title(title)
    plt.xlabel('Generation')
    plt.ylabel('Log10(f)')
    plt.legend(loc=3)
    fname = '../../figures/%s.png' % title.replace(' ','_').replace(',','_')
    #plt.savefig(fname)

def graph_population_effect_1D():
    """
    Show the existence of a minimum population to avoid premature convergence
    Objective function is sphere. 
    Crossover rate Cr = 1 causes premature convergence much faster
    """
    nruns = 1
    
    #np.random.seed(0)
    #save_figure_mono(1, sphere, (-100,100), D=30, NP=[100, 200, 300, 400, 500], archsize=[0], nruns=nruns, strategy = ["DE/rand"], muCr=1.0, sigmaCr=0, ngen=1000, title='DE/rand\nPremature convergenve vs population size (no archive, D=30)', label=lambda d,n,a,s:'NP=%i' % n)
    
    #save_figure_mono(2, sphere, (-100,100), D=30, NP=[100, 500, 1000, 1200], archsize=[0], nruns=nruns, strategy = ["JADE/rand-to-pbest"], muCr=1.0, sigmaCr=0, ngen=500, title='JADE/rand-to-pbest\nPremature convergenve vs population size (no archive, D=30)', label=lambda d,n,a,s:'NP=%i' % n)
    
    #save_figure_mono(3, sphere, (-100,100), D=30, NP=[100, 500, 750, 1000], archsize=[0], nruns=nruns, strategy = ["JADE/current-to-pbest"], muCr=1.0, sigmaCr=0, ngen=500, title='JADE/current-to-pbest\nPremature convergenve vs population size (no archive, D=30)', label=lambda d,n,a,s:'NP=%i' % n)
    
    #save_figure_mono(4, sphere, (-100,100), D=30, NP=[500], archsize=[0,0.5,0.75,1,2,3], nruns=nruns, strategy = ["JADE/rand-to-pbest"], muCr=1.0, sigmaCr=0, ngen=2000, title='JADE/rand-to-pbest\nPremature convergenve vs archive size (NP=500, D=30)', label=lambda d,n,a,s:'A=%.1f' % a)
    
    #save_figure_mono(5, sphere, (-100,100), D=60, NP=[1000, 1500, 2000], archsize=[0], nruns=nruns, strategy = ["DE/rand"], muCr=1.0, sigmaCr=0, ngen=2000, title='DE/rand\nPremature convergenve vs population size (no archive, D=60)', label=lambda d,n,a,s:'NP=%i' % n)
    
    #save_figure_mono(6, sphere, (-100,100), D=10, NP=[100, 200, 400, 800], archsize=[0], nruns=nruns, strategy = ["DE/rand"], muCr=0.5, sigmaCr=0.1, ngen=1000, title='', label=lambda d,n,a,s:'NP=%i' % n)

    #save_figure_mono(6, sphere, (-100,100), D=10, NP=[100], archsize=[1,3,7], nruns=nruns, strategy = ["DE/rand"], muCr=0.5, sigmaCr=0.1, ngen=1000, title='DE/rand\nConvergenve speed vs population and archive size (D=10)', label=lambda d,n,a,s:'A=%.1f' % a)

    #save_figure_mono(7, sphere, (-100,100), D=30, NP=[100], archsize=[1], nruns=nruns, strategy = ["DE/rand", "DE/best", "JADE/current-to-pbest", "JADE/rand-to-pbest", "JADE/current-rand-to-pbest"], muCr=0.5, sigmaCr=0.1, ngen=500, title='Convergence speed vs DE strategy (D=30, NP=100)\nf=sphere', label=lambda d,n,a,s:s)

    #save_figure_mono(8, schwefel2_22, (-10,10), D=30, NP=[100], archsize=[1], nruns=nruns, strategy = ["DE/rand", "DE/best", "JADE/current-to-pbest", "JADE/rand-to-pbest", "JADE/current-rand-to-pbest"], muCr=0.5, sigmaCr=0.1, ngen=2000, title='Convergence speed vs DE strategy (D=30, NP=100)\nf=schwefel2_22', label=lambda d,n,a,s:s)

    save_figure_mono(9, schwefel1_2, (-100,100), D=30, NP=[100], archsize=[1], nruns=nruns, strategy = ["DE/rand", "DE/best", "JADE/current-to-pbest", "JADE/rand-to-pbest", "JADE/current-rand-to-pbest"], muCr=0.5, sigmaCr=0.1, ngen=5000, title='Convergence speed vs DE strategy (D=30, NP=100)\nf=schwefel1_2', label=lambda d,n,a,s:s)

    #save_figure_mono(10, rosenbrock, (-30,30), D=30, NP=[100], archsize=[1], nruns=nruns, strategy = ["DE/rand", "DE/best", "JADE/current-to-pbest", "JADE/rand-to-pbest", "JADE/current-rand-to-pbest"], muCr=0.5, sigmaCr=0.1, ngen=3000, title='Convergence speed vs DE strategy (D=30, NP=100)\nf=rosenbrock', label=lambda d,n,a,s:s)
    plt.show()
        
    #save_figure_mono(11, noisy_quartic, (-1.28,1.28), D=30, NP=[100], archsize=[1], nruns=nruns, strategy = ["DE/rand", "DE/best", "JADE/current-to-pbest", "JADE/rand-to-pbest", "JADE/current-rand-to-pbest"], muCr=0.5, sigmaCr=0.1, ngen=3000, title='Convergence speed vs DE strategy (D=30, NP=100)\nf=noisy_quartic', label=lambda d,n,a,s:s)
    plt.show()

def test_mono(D, NP, NRUNS):
    NRUNS = 50
    D = 30
    NP = 100
    obj_f = [(sphere, (-100, 100), (1500, 2000))]
    np.random.seed(0)
    for i in xrange(NRUNS):    
        for f in obj_f:
            run_test(f[0], f[1], f[2], "JADE/current-to-pbest")
    D = 100
    NP = 400

def run_test2D_grap():
    o = opt.population.get_objectives()
    pf = [de.pareto_front_sort(o)]
    o = [o]
    for i in xrange(100):
        pf.append(opt.run_generation(pf[-1]))
        o.append(opt.population.get_objectives())
        #print o[-1], pf[-1]

if __name__ == '__main__':
    pass    


