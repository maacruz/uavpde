import sys
import os.path
import numpy as np
from scipy.stats import mannwhitneyu
import matplotlib.pyplot as plt
import matplotlib.colors
import scipy.io as scio
from nondominatedsorting import dominates
import main

epsilon = 1e-12

def get_dominance_matrices(obj_front1, obj_front2):
    o1_lo_o2 = np.zeros((len(obj_front1),len(obj_front2)), dtype=bool)
    o2_lo_o1 = np.zeros((len(obj_front2),len(obj_front1)), dtype=bool)
    o1_eq_o2 = np.zeros((len(obj_front1),len(obj_front2)), dtype=bool)
    for o1 in xrange(len(obj_front1)):
        for o2 in xrange(len(obj_front2)):
            if dominates(obj_front1[o1], obj_front2[o2], priorities=None):
                o1_lo_o2[o1,o2] = True 
            elif dominates(obj_front2[o2], obj_front1[o1], priorities=None):
                o2_lo_o1[o2,o1] = True
            elif np.all(np.abs(obj_front1[o1] - obj_front2[o2]) < epsilon):
                o1_eq_o2[o1,o2] = True
    return o1_lo_o2, o2_lo_o1, o1_eq_o2
    
def SFDRP(objective_set1, objective_set2):
    Nr = len(objective_set1)
    assert Nr == len(objective_set2)
    dominated1 = np.zeros(Nr)
    dominated2 = np.zeros(Nr)
    for i in xrange(Nr):
        for j in xrange(Nr):
            o1_lo_o2, o2_lo_o1, o1_eq_o2 = get_dominance_matrices(objective_set1[i], objective_set2[j])
            d = 0
            # if for all o2 exists o1 s.t. o1 <= o2 and there is no o2 s.t. o2 < o1
            if np.all(np.any(o1_lo_o2|o1_eq_o2,1)) and not(np.any(o2_lo_o1)):
                # objective population j from objective_set2 is weakly dominated by objective population i from objective_set1
                dominated2[j] += 1
                d = d + 1
            #if for all o1 exists o2 s.t. o2 <= o1 and there is no o1 s.t. o1 < o2
            if np.all(np.any(o2_lo_o1|o1_eq_o2.T)) and not(np.any(o1_lo_o2)):
                # objective population i from objective_set1 is dominated by objective population j from objective_set2
                dominated1[i] += 1
                d = d + 1
            # if both are weakly dominant there is no real dominance and they are not comparable
            if d == 2:
                # delete the previous accumulated count
                dominated1[i] -= 1
                dominated2[j] -= 1
    p = mannwhitneyu(dominated1, dominated2)[1]*2
    if p < 0.05:
        if np.average(dominated1) < np.average(dominated2):
            sfdrp = 1
        else:
            sfdrp = 2
    else:
        sfdrp = 0
    return sfdrp

def IRP(objective_set1, objective_set2):
    Nr = len(objective_set1)
    assert Nr == len(objective_set2)
    dominates1 = dominates2 = 0
    for i in xrange(Nr):
        o1_lo_o2, o2_lo_o1, o1_eq_o2 = get_dominance_matrices(objective_set1[i], objective_set2[i])
        d = 0
        if np.all(np.any(o1_lo_o2|o1_eq_o2,1)) and not(np.any(o2_lo_o1)):
            # objective population i from objective_set1 weakly dominates objective population i from objective_set2
            dominates1 += 1
            d = d + 1
        #if for all o1 exists o2 s.t. o2 <= o1 and there is no o1 s.t. o1 < o2
        if np.all(np.any(o2_lo_o1|o1_eq_o2.T)) and not(np.any(o1_lo_o2)):
            # objective population i from objective_set1 is dominated by objective population j from objective_set2
            dominates2 += 1
            d = d + 1
        # if both are weakly dominant there is no real dominance and they are not comparable
        if d == 2:
            # delete the previous accumulated count
            dominates1 -= 1
            dominates2 -= 1
    return dominates1, dominates2

def draw_matrix(matrix, cmap, vmin, vmax, ax):
    # Set up the matplotlib figure
    sns.heatmap(matrix, cmap=cmap, vmin=vmin, vmax=vmax, linecolor='black', square=True, xticklabels=True, yticklabels=True, linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)

def compare_planners(output_basename, planner_fname_list, Nr):
    def load_experiment_set(planner_fname, Nr):
        objective_set = []
        for i in xrange(Nr):
            fname = "%s_%03i.mat" % (planner_fname, i)
            result = scio.loadmat(fname)
            objective_set.append(result["objectives"])
        return objective_set
    nplanners = len(planner_fname_list)
    sfdrp = np.zeros((nplanners, nplanners))
    irp = np.zeros((nplanners, nplanners))
    objective_set_list = [load_experiment_set(pl, Nr) for pl in planner_fname_list]
    for i in xrange(nplanners):
        for j in xrange(i+1, nplanners):
            d = SFDRP(objective_set_list[i], objective_set_list[j])
            sfdrp[i,j] = d
            if d == 1:
                sfdrp[j,i] = 2
            elif d == 2:
                sfdrp[j,i] = 1
            di, dj = IRP(objective_set_list[i], objective_set_list[j])
            if di > dj:
                dj = -dj
            elif di < dj:
                di = -di
            irp[i,j] = di
            irp[j,i] = dj
    f1, ax1 = plt.subplots()
    draw_matrix(sfdrp, sfdrp_palette, 0, 2, ax1)
    plt.savefig('%s-sfdrp.png' % output_basename)
    f2, ax2 = plt.subplots()
    draw_matrix(irp, irp_palette, -Nr+1, Nr-1, ax2)
    plt.savefig('%s-irp.png' % output_basename)
    plt.show()
    return sfdrp, irp

if __name__ == '__main__':
    import seaborn as sns
    sfdrp_palette = matplotlib.colors.ListedColormap([(0.5,0.5,0.5), (1,1,1), (0,0,0)])
    irp_palette = sns.diverging_palette(220, 10, as_cmap=True)
    sns.set(style="whitegrid", context="paper", font="monospace")
    np.set_printoptions(precision=4, linewidth = 90)
    if len(sys.argv) < 2:
        print "Usage: analyze_experiments configfile"
        exit()
    fname = sys.argv[1] # experiment config files
    config = main.readConfigFile(fname)
    Nr = config["Nr"]
    result_path = config["ResultsPath"]
    output_basename = os.path.join(result_path, os.path.splitext(os.path.basename(fname))[0])
    if isinstance(result_path,str):
        planner_fname_list = [os.path.join(result_path,s) for s in config["ExpName"]]
    else:
        planner_fname_list = [os.path.join(result_path[i],config["ExpName"][i]) for i in xrange(len("ExpName"))]
    sfdrp, irp = compare_planners(output_basename, planner_fname_list, Nr)
    print "SFDRP matrix"
    print sfdrp
    print "\nIRP matrix"
    print irp
    
    
