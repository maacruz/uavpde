import sys
import numpy as np
import scipy.io as scio
import sceneries
import maps
import trajectories
import uav
import adus
import main
import differentialevolution
from uavobjectives import *


def objvalue(chrom, scenery):
    dtedmap = scenery.dtedmap
    minimum_height = scenery.uav_height_safetymargin
    NFZlist = scenery.NFZlist
    ADUlist = scenery.ADUlist
    uav0 = scenery.UAVlist[0]
    objvaltmp = np.zeros(value.shape)
    for j in xrange(len(chrom)):
        discretized_trajectory = chrom[j][:3]
        objvaltmp[j,0:2] = UAVcost_flight_altitude(discretized_trajectory, dtedmap, minimum_height)
        objvaltmp[j,2:3] = UAVcost_slope_limits(discretized_trajectory)
        objvaltmp[j,3:4] = UAVcost_map_limit(discretized_trajectory, dtedmap)
        objvaltmp[j,4:5] = UAVcost_turning_limit(discretized_trajectory, uav0.V)
        objvaltmp[j,5:6] = UAVcost_map_nfz(discretized_trajectory, NFZlist)
        objvaltmp[j,6:7] = UAVcost_fuel_limit(discretized_trajectory, uav0.fuel, uav0.V, refueling_waypoint_list=[])
        objvaltmp[j,7:8] = np.round(UAVcost_path_length(discretized_trajectory)/objective.lmin*1000)/1000 #DISCRETIZATION NOT IN THE PAPER
        objvaltmp[j,8:10] = UAVcost_PDetectKill(discretized_trajectory, uav0.V, dtedmap, ADUlist, uav0)
    return objvaltmp

def makeTrajectoryPopulation(pop, waypoints, uav_objective):
    pop = pop.reshape((len(pop),3,-1))
    control_points = np.zeros((pop.shape[0], 3, pop.shape[-1]+len(waypoints)))
    fixed_wp_idx = [0]
    k = 1
    l = 0
    for j in xrange(1,len(waypoints)-1):
        pass #FIXME
    # copy the points between the two last waypoints
    while l < pop.shape[-1]:
        control_points[:,:,k] = pop[:,:,l]
        k += 1
        l += 1
    control_points[:,0:2,:] *= 1000
    fixed_wp_idx.append(control_points.shape[-1]-1)
    for j in xrange(len(waypoints)):
        for k in xrange(3): 
            control_points[:,k,fixed_wp_idx[j]] = waypoints[j][k+1]
    traj = trajectories.TrajectoryPopulationISplines(uav_objective, waypoints)
    traj.init_fromdata(control_points, fixed_wp_idx)
    return traj
    
np.set_printoptions(linewidth=160, precision=4)
pconf, sceneconf, trayconf, algconf = uav.readLegacyxmlConfig(sys.argv[1])
scenery = sceneries.Scenery(pconf, sceneconf)
uav0 = scenery.UAVlist[0]
objective = uavobjectives.UAVObjective(scenery, pconf, trayconf, 0)
spline_discretization_step = 5000 #float(trayconf['UAV1']['MaxSeparation'])*1000.0

# Suelo  Pend   Mapa  Radios  NFZ  Consumo Elevacion Longitud  Derribo Detecc
objmap = np.array([0, 2, 3, 4, 5, 6, 1, 7, 9, 8])
value_priorities = [1, 1, 1, 1, 1, 1, 3, 2, 2, 3]
prioorder = np.argsort(value_priorities)
priorities = [(0,6),(6,8),(8,10)]


matdata = scio.loadmat(sys.argv[2])['DataSave'][0]
for i in xrange(len(matdata)):
    print "Checking iteration",i
    oldpop,oldchrom,oldvalue,newpop,newchrom,newvalue,pop,chrom,value,fitness,t = matdata[i]
    chrom = chrom.reshape(len(chrom))

    ## check objective values with the matlab discretized trajectory
    ## this finds differences in objective calulation
    #objvaltmp = objvalue(chrom, scenery)
    #print "Objectives"
    #if not np.all(np.abs(objvaltmp[:,objmap] - value) < 1e-5):
        #print objvaltmp[:,objmap] - value

    ## check fitnesses: cannot work, front sorting criteria is different
    ## Take 2 non-comparable individuals; if, considering only up to priority "p", individual 1 can be put in front "i" while individual 2 can be put in front "i+1", but considering up to priority "p+1" both could be put in the same front, then UAVPDE considers individual 1 higher rank than individual 2 while UPCmatlab assigns the same rank to both
    #fronts = differentialevolution.pareto_front_sort_fast(value[:,prioorder], priorities)
    #frontsfitness = np.zeros(len(value), dtype=int)
    #for j in xrange(len(fronts)):
        #for k in fronts[j]:
            #frontsfitness[k] = (len(fronts)-j-1)*10 + 1

    ## check objective values with the python discretized trajectory
    ## this shows how the different discretization algorithms affects objective values
    #mypop = makeTrajectoryPopulation(pop, uav0.waypoints, objective)
    #mychrom = [mypop.discretize(j, spline_discretization_step)[0] for j in xrange(mypop.size)]
    #objvaltmp = objvalue(mychrom, scenery)
    #print "Discretized trajectory"
    #if not np.all(np.abs(objvaltmp[:,objmap] - value) < np.array([1,1,1,1,1,1e-2,5e1,5e-3,5e-2,1e-3])):
        #print objvaltmp[:,objmap] - value
    
    # check pair selection
    # this shows if the goals have any influence in parent-offspring selection
    c_UPC = np.all(oldpop == pop,1)
    c_UAVPDE = differentialevolution.compare_objectives(oldvalue[:,prioorder],newvalue[:,prioorder],priorities) >= 0
    if not(np.all(c_UPC == c_UAVPDE)):
        print "Selection:", i, np.where(c_UPC != c_UAVPDE)