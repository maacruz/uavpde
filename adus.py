import numpy as np
from utils import jit, vectorize

# missile being able to hit depends on the UAV-ADU relative height (UAV is origin), and on the projection of the UAV-ADU segment on the UAV direction and its perpendicular. For a given height, there is a maximum range, a minimum UAV-ADU segment projected on UAV direction component, and a maximum UAV-ADU segment projected on the UAV direction perpendicular component.

adu_types = set([10,15,20])

@vectorize
def _ADU_T10_killP(d, cos_theta, dh):
    if (dh <= 50) or (dh >= 1000):
        minUAV_ADUonV = np.inf
        maxUAV_ADUonVp = 0.
        killRange = 0.
    else:
        minUAV_ADUonV = 4000.
        maxUAV_ADUonVp = 500.
        killRange = 5000.
    killRange, minUAV_ADUonV, maxUAV_ADUonVp = 1.2*killRange, 0.8*minUAV_ADUonV, 1.2*maxUAV_ADUonVp # ??
    if dh <= 100:
        Pk = 0.7*dh/100.
    elif dh <= 200:
        Pk = 0.7 - 0.4*(dh-100)/100.
    elif dh <= 300:
        Pk = 0.3 - 0.1*(dh-200)/100.
    else:
        Pk = 0.2 - 0.2*(dh-300)/700.
    sin_theta = np.sqrt(1.0-cos_theta**2)
    InPK = (d <= killRange) and (d*cos_theta >= minUAV_ADUonV) and (d*sin_theta <= maxUAV_ADUonVp)
    if not(InPK):
        Pk = 0.0
    return Pk


@vectorize
def _ADU_T15_killP(d, cos_theta, dh):
    if (dh <= 50) or (dh >= 5000):
        minUAV_ADUonV = np.inf
        maxUAV_ADUonVp = 0.
        killRange = 0.
    elif dh <= 100:
        minUAV_ADUonV = 500*dh/100.
        maxUAV_ADUonVp = 3000*dh/100.
        killRange = 6000*dh/100.
    elif dh <= 200:
        minUAV_ADUonV = 500.
        maxUAV_ADUonVp = 3000. + 1500*(dh-100)/100.
        killRange = 6000.
    elif dh <= 300:
        minUAV_ADUonV = 500.
        maxUAV_ADUonVp = 4500.
        killRange = 6000.
    elif dh <= 1000:
        minUAV_ADUonV = 500. + 500*(dh-300)/700.
        maxUAV_ADUonVp = 4500. + 500*(dh-300)/700.
        killRange = 6000.
    elif dh <= 2000:
        minUAV_ADUonV = 1000. + 500*(dh-1000)/1000.
        maxUAV_ADUonVp = 5000. - 500*(dh-1000)/1000.
        killRange = 6000.
    elif dh <= 3000:
        minUAV_ADUonV = 1500. + 500*(dh-2000)/1000.
        maxUAV_ADUonVp = 4500. - 1000*(dh-2000)/1000.
        killRange = 6000. - 500*(dh-2000)/1000.
    elif dh <= 4000:
        minUAV_ADUonV = 2000. + 1000*(dh-3000)/1000.
        maxUAV_ADUonVp = 3500. - 1500*(dh-3000)/1000.
        killRange = 5500. - 1000*(dh-3000)/1000.
    else:
        minUAV_ADUonV = 3000.
        maxUAV_ADUonVp = 2000.
        killRange = 4500. - 500*(dh-4000)/1000.
    killRange, minUAV_ADUonV, maxUAV_ADUonVp = 1.2*killRange, 0.8*minUAV_ADUonV, 1.2*maxUAV_ADUonVp # ??
    if dh <= 100:
        Pk = 0.6*dh/100.
    elif dh <= 200:
        Pk = 0.6 + 0.1*(dh-100)/100.
    elif dh <= 300:
        Pk = 0.7 + 0.05*(dh-100)/100.
    elif dh <= 3000:
        Pk = 0.75
    elif dh <= 4000:
        Pk = 0.75 - 0.25*(dh-3000)/1000.
    else:
        Pk = 0.5 - 0.5*(dh-4000)/1000.
    sin_theta = np.sqrt(1.0-cos_theta**2)
    InPK = (d <= killRange) and (d*cos_theta >= minUAV_ADUonV) and (d*sin_theta <= maxUAV_ADUonVp)
    if not(InPK):
        Pk = 0.0
    return Pk

@vectorize
def _ADU_T20_killP(d, cos_theta, dh):
    if (dh <= 50) or (dh >= 10000):
        minUAV_ADUonV = np.inf
        maxUAV_ADUonVp = 0.
        killRange = 0.
    elif dh <= 100:
        minUAV_ADUonV = -2000*dh/100.
        maxUAV_ADUonVp = 3000*dh/100.
        killRange = 16000*dh/100.
    elif dh <= 200:
        minUAV_ADUonV = -2000. - 2000*(dh-100)/100.
        maxUAV_ADUonVp = 3000. + 3000*(dh-100)/100.
        killRange = 16000.
    elif dh <= 300:
        minUAV_ADUonV = -4000.
        maxUAV_ADUonVp = 6000. + 3000*(dh-200)/100.
        killRange = 16000.
    elif dh <= 1000:
        minUAV_ADUonV = -4000.
        maxUAV_ADUonVp = 9000. + 4000*(dh-300)/700.
        killRange = 16000.
    elif dh <= 3000:
        minUAV_ADUonV = -4000.
        maxUAV_ADUonVp = 13000.
        killRange = 16000.
    elif dh <= 4000:
        minUAV_ADUonV = -4000. + 2000*(dh-3000)/1000.
        maxUAV_ADUonVp = 13000.
        killRange = 16000.
    elif dh <= 5000:
        minUAV_ADUonV = -2000. + 4000*(dh-4000)/1000.
        maxUAV_ADUonVp = 13000.
        killRange = 16000.
    elif dh <= 6000:
        minUAV_ADUonV = 2000. + 2000*(dh-5000)/1000.
        maxUAV_ADUonVp = 13000. - 1000*(dh-5000)/1000.
        killRange = 16000. - 1000*(dh-5000)/1000.
    elif dh <= 7000:
        minUAV_ADUonV = 4000. + 1000*(dh-6000)/1000.
        maxUAV_ADUonVp = 12000.
        killRange = 15000. - 1000*(dh-5000)/1000.
    else:
        minUAV_ADUonV = 5000.
        maxUAV_ADUonVp = 12000.
        killRange = 14000. - 1000*(dh-7000)/3000.
    killRange, minUAV_ADUonV, maxUAV_ADUonVp = 1.2*killRange, 0.8*minUAV_ADUonV, 1.2*maxUAV_ADUonVp # ??
    if dh <= 100:
        Pk = 0.7*dh/100.
    elif dh <= 200:
        Pk = 0.7 + 0.1*(dh-100)/100.
    elif dh <= 300:
        Pk = 0.8
    elif dh <= 1000:
        Pk = 0.8 - 0.05*(dh-300)/700.
    elif dh <= 4000:
        Pk = 0.75;
    elif dh <= 5000:
        Pk = 0.75 - 0.15*(dh-4000)/1000.
    elif dh <= 6000:
        Pk = 0.6 - 0.2*(dh-5000)/1000.
    elif dh <= 7000:
        Pk = 0.4 - 0.3*(dh-6000)/1000.
    else:
        Pk = 0.1 - 0.1*(dh-7000)/3000.
    sin_theta = np.sqrt(1.0-cos_theta**2)
    InPK = (d <= killRange) and (d*cos_theta >= minUAV_ADUonV) and (d*sin_theta <= maxUAV_ADUonVp)
    if not(InPK):
        Pk = 0.0
    return Pk

class ADU(object):
    def __init__(self, idn, adu_type, x, y, z=None, ispopup=False):
        assert adu_type in adu_types
        self.idn = idn
        self.adu_type = adu_type
        self.x = x
        self.y = y
        self.z = z
        self.ispopup = False
        self.viewlimit = 50000. # maximum view distance
        if adu_type == 10:
            self.zeta1 = 1.3548             # Parametro ca para los tipo 10
            self.zeta2 = np.exp(-53.0963)   # Parametro cc para los tipo 10
            self.killRange = 5000.
        elif adu_type == 15:
            self.zeta1 = 1.3548             # Parametro ca para los tipo 15
            self.zeta2 = np.exp(-53.0963)   # Parametro cc para los tipo 15
            self.killRange = 6000.
        else:
            self.zeta1 = 1.5064             # Parametro ca para los tipo 20
            self.zeta2 = np.exp(-63.2171)   # Parametro cc para los tipo 20
            self.killRange = 16000.

    def Pkill(self, d, cos_theta, dh):
        return adu_Pkill(self.adu_type, d, cos_theta, dh)
        
@jit
def adu_Pkill(adu_type, d, cos_theta, dh):
    dh = np.abs(dh)
    Pk = _ADU_T10_killP(d, cos_theta, dh) if adu_type == 10 else _ADU_T15_killP(d, cos_theta, dh) if adu_type == 15 else _ADU_T20_killP(d, cos_theta, dh)
    return Pk
        
