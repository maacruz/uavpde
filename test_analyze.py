import numpy as np
import differentialevolution as de
import de_benchmarks
import analyze_experiments as ae

def test_SFDRP_IRP():
    objective_set1 = [np.array([[0,0],[1,0],[0,1]])]*2
    objective_set2 = [np.array([[0.5,0.5],[1,0],[0,1]])]*2
    assert ae.SFDRP(objective_set1,objective_set2) == 0
    assert ae.IRP(objective_set1,objective_set2) == (2,0)
    assert ae.IRP(objective_set1,objective_set1) == (0,0)

    objective_set1 = [np.array([[0,0],[1,0],[0,1]])]*20+[np.array([[0.5,0.5],[1,0.5],[0.5,1]])]*5
    objective_set2 = [np.array([[0.5,0.5],[1,0.5],[0.5,1]])]*20+[np.array([[0,0],[1,0],[0,1]])]*5
    assert ae.SFDRP(objective_set1,objective_set2) == 1
    assert ae.SFDRP(objective_set2,objective_set1) == 2
    assert ae.SFDRP(objective_set1,objective_set1) == 0
    assert ae.IRP(objective_set1,objective_set1) == (0,0)
