import numpy as np
import scipy
import scipy.stats as stats
import copy
import nondominatedsorting as nds
from populations import EmptyArchive
try:
    from ckdtree import cKDTree as KDTree
except:
    from kdtree import KDTree
from utils import cauchy_sample, convolution_kernel_1D, convolve

# strategy:(archive size, pbest fraction, sigmaCr, use pareto ranking for selection, last front to consider for best individual)
strategies = {'DE/rand':(0,1.0,0.0,False,2),
              'DE/best':(0,0.0,0.0,False,2),
              'JADE/current-to-pbest':(2,0.05,0.1,True,-1),
              'JADE/rand-to-pbest':(2,0.05,0.1,True,-1),
              'JADE/current-rand-to-pbest':(2,0.05,0.1,True,-1)}




# rand/1:             v = xr1 +   F*(xr2 - xr3)
# pbest/1:            v = xpb +   F*(xr2 - xr3)
# current-to-rand/1:  v = xi  +   K*(xr1 - xi)  + F*(xr2 - xr3) =   (1 - K)*xi  +   K*xr1 + F*(xr2 - xr3)
# current-to-pbest/1: v = xi  + Fcb*(xpb - xi)  + F*(xr2 - xr3) = (1 - Fcb)*xi  + Fcb*xpb + F*(xr2 - xr3) # JADE: Fcb = F
# rand-to-pbest/1:    v = xr1 + Frb*(xpb - xr1) + F*(xr2 - xr3) = (1 - Frb)*xr1 + Frb*xpb + F*(xr2 - xr3) # JADE: Frb = F
# UDEE:               v = Kc*xi + Kb*xpb + Kr*xr1 + F(xr2 - xr3) # Kb+Kc+Kr == 1
#     Kc=0->rand-to-pbest/1
#     Kb=0->current-to-rand/1
#     kr=0->current-to-pbest/1
#     kb=1->pbest/1
#     Kr=1->rand/1
# NEW:
# JADE/current-rand-to-pbest/1: v = (1 - F)*((1 - K)*xi + K*xr1)  + F*xpb + F*(xr2 - xr3) = (1 - K[F])*xi + K[F]*xr1 + F*(xr2 - xr3)

class FPdf(object):
    """
    F factor probability distribution function
    """
    def __init__(self, strategy, F_min=0.1, F_max=1.0, learning_rate=0.1, conv_kernel_scale = 0.2, nbins=21):
        """
        Input:
            strategy: string # a mutation strategy string, i.e. "DE/rand"
            F_min=0.1: float  # minimum possible value for F, must be 0 >= Fmin <= 1
            F_max=1.0: float  # maximum possible value for F, must be 0 >= Fmin <= Fmax <= 1
            learning_rate=0.1: float # how much the pdf is updated, must be 0 <= learning_rate <= 1
            conv_kernel_scale=0.2: float # standard deviation for the convolution kernel used to smooth adaptation in the multinomial strategy
            nbins=21: int # number of multinomial bins
        """
        assert 0 <= F_min <= F_max <= 1
        strategy = strategy.split('/')
        if strategy[0] == 'JADE':
            self._update = self._update_JADE
            self._sample = self._sample_JADE
        elif strategy[0] == 'SHADE':
            self._update = self._update_SHADE
            self._sample = self._sample_SHADE
        elif strategy[0] == 'MSHADE':
            self._update = self._update_MSHADE
            self._sample = self._sample_multinomial
        elif strategy[0] == 'MADE':
            self._update = self._update_MADE
            self._sample = self._sample_multinomial
        else:
            self._update = self._update_none
            self._sample = self._sample_uniform
        # F distribution parameters
        self.F_min = F_min
        self.F_max = F_max
        self.muF = (F_min + F_max) / 2.
        # SHADE
        M = int(round((1.0 - learning_rate)/learning_rate)) + 1
        self.SHM = np.ones(M) * (F_min + F_max) / 2.
        self.SHM_i = 0
        # MSHADE/MADE
        # initialize probability mass function
        self.cauchy_pdf = stats.cauchy.pdf(np.linspace(-0.5, 1.5, 2 * nbins - 1), 0.5, 0.1)
        self.nbins = nbins
        # Uniform distribution
        mbins = np.ones(nbins)
        if strategy[0] in ['JADE','MSHADE']:
            # Cauchy distribution
            #mbins = stats.cauchy.pdf(np.linspace(0.,1.,nbins), (F_min + F_max)/2, 0.1)
            mbins = self._get_cauchy_pdf((F_min + F_max) / 2.)
            mbins[0] = 0.0
        i_min = int(round(F_min * (nbins - 1)))
        i_max = int(round(F_max * (nbins - 1))) + 1
        mbins[:i_min] = 0.0
        mbins[i_max:] = 0.0
        mbins /= mbins.sum() # Distribution normalization
        self.mbins = mbins
        self.learning_rate = min(max(learning_rate, 0.), 1.)
        # convolution kernel to prevent the multinomial pmf from becoming deterministic
        self.conv_kernel = convolution_kernel_1D(conv_kernel_scale, nbins, 'cauchy')


    def _get_cauchy_pdf(self, mu):
        c = int(np.round((1. - mu) * (self.nbins - 1)))
        return np.copy(self.cauchy_pdf[c:c + self.nbins])


    def _sample_JADE(self, popsize):
        F = cauchy_sample(self.muF, 0.1, self.F_min, self.F_max, popsize)
        return F[None, ...].T


    def _sample_SHADE(self, popsize):
        muF = self.SHM[np.argmax(np.random.multinomial(1, np.ones(self.SHM.size)/self.SHM.size, popsize),axis=1)]
        F = cauchy_sample(muF, 0.1, self.F_min, self.F_max)
        return F[None, ...].T


    def _sample_uniform(self, popsize):
        F = np.random.uniform(self.F_min, self.F_max, popsize)
        return F[None, ...].T


    def _sample_multinomial(self, popsize):
        F = np.argmax(np.random.multinomial(1, self.mbins, popsize), axis=1) / float(self.nbins - 1)
        return F[None, ...].T


    def sample(self, popsize):
        """
        Return a column vector sample of the given size
        Input:
            popsize: int
        Output:
            float[popsize,1]
        """
        return self._sample(popsize)


    # def _update_JADE(self, Sf, w_Sf):
    #     """
    #     Update F pdf using JADE algorithm
    #     """
    #     F_val = np.linspace(0., 1., self.nbins)
    #     meanF = np.sum(Sf**2) / Sf.sum()
    #     muF = np.sum(F_val * self.mbins) / self.mbins.sum()
    #     muF = (1 - self.learning_rate)*muF + self.learning_rate*meanF
    #     #mbins = stats.cauchy.pdf(F_val, muF, 0.1)
    #     mbins = self._get_cauchy_pdf(muF)
    #     mbins[0] = 0.0
    #     i_min = int(round(self.F_min * (self.nbins - 1)))
    #     i_max = int(round(self.F_max * (self.nbins - 1))) + 1
    #     mbins[:i_min] = 0.0
    #     mbins[i_max:] = 0.0
    #     mbins /= mbins.sum()
    #     self.mbins = mbins

    def _update_JADE(self, Sf, w_Sf):
        # Update F pdf using JADE algorithm
        meanF = np.sum(Sf**2) / Sf.sum()
        self.muF = (1 - self.learning_rate)*self.muF + self.learning_rate*meanF

    def _update_SHADE(self, Sf, w_Sf):
        # Update F pdf using SHADE algorithm
        if w_Sf is None:
            w_Sf = np.ones(Sf.size) / Sf.size
        meanF = np.dot(Sf**2, w_Sf)/np.dot(Sf, w_Sf)
        self.SHM[self.SHM_i] = meanF
        self.SHM_i = (self.SHM_i + 1) % self.SHM.size


    def _update_MSHADE(self, Sf, w_Sf):
        # Update F pdf using MSHADE algorithm
        if w_Sf is None:
            w_Sf = np.ones(Sf.size) / Sf.size
        meanF = np.dot(Sf**2, w_Sf)/np.dot(Sf, w_Sf)
        mbins = self._get_cauchy_pdf(meanF)
        mbins[0] = 0.0
        i_min = int(round(self.F_min * (self.nbins - 1)))
        i_max = int(round(self.F_max * (self.nbins - 1))) + 1
        mbins[:i_min] = 0.0
        mbins[i_max:] = 0.0
        mbins /= mbins.sum()
        dirichlet_M = (1.0 - self.learning_rate)/self.learning_rate
        mbins = (self.mbins * dirichlet_M + mbins) / (dirichlet_M + 1.)
        self.mbins = mbins


    def _update_MADE(self, Sf, w_Sf):
        # Update F pdf using multinomial algorithm
        if w_Sf is None:
            w_Sf = np.ones(Sf.size) / Sf.size
        mbins = np.zeros(self.nbins)
        for i in xrange(Sf.size):
            j = int(round(Sf[i] * (self.nbins - 1)))
            mbins[j] += w_Sf[i]
        mbins = convolve(mbins, self.conv_kernel)
        mbins[0] = 0.0
        i_min = int(round(self.F_min * (self.nbins - 1)))
        i_max = int(round(self.F_max * (self.nbins - 1))) + 1
        mbins[:i_min] = 0.0
        mbins[i_max:] = 0.0
        mbins /= mbins.sum()
        dirichlet_M = (1.0 - self.learning_rate)/self.learning_rate
        mbins = (self.mbins * dirichlet_M + mbins) / (dirichlet_M + 1.)
        self.mbins = mbins


    def _update_none(self, Sf, w_Sf):
        pass


    def update(self, Sf, w_Sf=None):
        """
        Update F pdf according to the initialized strategy
        Input:
            Sf: float[:] # successful factors
            w_Sf=None: float[n] # optional factor weights to be used in strategies with weighted updates (SHADE, MADE)
        """
        if self.learning_rate > 0.:
            Sf = Sf.ravel()
            if len(Sf):
                self._update(Sf, w_Sf)




class KPdf(object):
    """
    K factors probability distribution function
    """
    def __init__(self, strategy, K_min=0.1, K_max=1.0, learning_rate=0.1, conv_kernel_scale = 0.2, nbins=21):
        """
        Input:
            strategy: string # a mutation strategy string, i.e. "DE/rand"
            K_min=0.1: float  # minimum possible value for K in strategies with only one K, must be 0 >= Kmin <= 1
            K_max=1.0: float  # maximum possible value for K in strategies with only one K, must be 0 >= Kmin <= Fmax <= 1
            learning_rate=0.1: float # how much the pdf is updated, must be 0 <= learning_rate <= 1
            conv_kernel_scale=0.2: float # standard deviation for the convolution kernel used to smooth adaptation in the multinomial strategy
            nbins=21: int # number of multinomial bins
        """
        strategy = strategy.split('/')

        # set updating algorithm
        self._update = self._update_none
        self.shade = strategy[0] in ['SHADE', 'MSHADE']
        if strategy[0] in ['JADE', 'SHADE', 'MSHADE']:
            if strategy[1] == 'current-rand-to-pbest':
                self._update = self._update_Jcrb
        elif strategy[0] == 'MADE':
            self._update = self._update_multinomial
        # set sampling algorithm
        self._sample = self._sample_K
        if strategy[0] in ['JADE', 'SHADE', 'MSHADE']:
            if strategy[1] == 'current-to-rand':
                self._sample = self._sample_Jcr
            elif strategy[1] == 'current-to-pbest':
                self._sample = self._sample_Jcb
            elif strategy[1] == 'rand-to-pbest':
                self._sample = self._sample_Jrb
            elif strategy[1] == 'current-rand-to-pbest':
                self._sample = self._sample_Jcrb

        # Multinomial bins holding the distribution
        self.nbins = nbins
        self.mbins = np.zeros((1 + nbins) * nbins / 2)

        # Calculate Kb, Kr and Kc weights for each K bin
        self.Kb = np.zeros(self.mbins.size)
        self.Kr = np.zeros(self.mbins.size)
        for i in xrange(nbins):
            for j in xrange(i + 1):
                self.Kb[i * (i + 1)/2 + j] = 1.0 - float(i) / (nbins - 1)
                self.Kr[i * (i + 1)/2 + j] = float(j) / (nbins - 1)
        self.Kc = 1.0 - self.Kr - self.Kb
        # reverse index
        self.Kb_idx = [np.where(np.round(self.Kb * (nbins - 1)).astype(int) == i)[0] for i in xrange(nbins)]
        self.Kr_idx = [np.where(np.round(self.Kr * (nbins - 1)).astype(int) == i)[0] for i in xrange(nbins)]
        self.Kc_idx = [np.where(np.round(self.Kc * (nbins - 1)).astype(int) == i)[0] for i in xrange(nbins)]
        # initialize probability mass function
        if strategy[1] == 'rand':
            self.mbins[self._bin_idx(1., 0.)] = 1.
        elif strategy[1] == 'best':
            self.mbins[self._bin_idx(0., 1.)] = 1.
        else: # *-to-* strategies
            K_min = round(min(max(K_min, 0.), 1.) * (nbins - 1)) / (nbins - 1)
            K_max = round(max(min(K_max, 1.), K_min) * (nbins - 1)) / (nbins - 1)
            k = np.linspace(K_min, K_max, int((K_max - K_min) * (nbins - 1) + 1))#/(nbins - 1)
            if strategy[1] == 'current-to-rand':
                self.mbins[self._bin_idx(k, 0.)] = 1. / len(k)
            elif strategy[1] == 'current-to-pbest':
                self.mbins[self._bin_idx(0., k)] = 1. / len(k)
            elif strategy[1] == 'rand-to-pbest':
                self.mbins[self._bin_idx(k, 1. - k)] = 1. / len(k)
            elif strategy[1] == 'current-rand-to-pbest':
                for i in xrange(nbins):
                    self.mbins[self.Kb_idx[i]] = 1.0 / self.Kb_idx[i].size
            else:
                self.mbins[:] = 1.0 / len(self.mbins)

        self.learning_rate = min(max(learning_rate, 0.), 1.)
        # gaussian convolution kernel to prevent the multinomial pdf from becoming deterministic
        self.conv_kernel = [convolution_kernel_1D(conv_kernel_scale, n, 'cauchy') for n in xrange(nbins, 0, -1)]


    def _bin_idx(self, Kr, Kb):
        """
        Return the multinomial bin index given its triangular weights

                  (Rand) 9 _____Rand==1.
                        /\
                       /  \
                     5/____\8
                     / \    \
                   2/___4____\7   Curr==1
                   /\    \    \   /
                  /  \    \    \ /
          (Best) 0____1____3____6 (Curr) _____Rand==0.
                / \              \
               /   \              \
          Curr==0  Best==1.       Best==0.

        Input:
            Kr: float # rand weigth from 0. to 1.
            Kb: float # best weight from 0. to 1.
        Output:
            int # bin index
        """
        ir = np.clip((np.round(Kr * (self.nbins - 1))).astype(int), 0, self.nbins - 1) # K index grows with Kr
        ib = (self.nbins - 1) - np.clip((np.round(Kb * (self.nbins - 1))).astype(int), 0, self.nbins - 1) # K index decreases
        i = ib * (ib + 1)/2 + ir
        return i


    def _sample_Jcb(self, F):
        # JADE/current-to-pbest
        Kr = np.zeros_like(F)
        Kb = F
        return Kr, Kb


    def _sample_Jrb(self, F):
        # JADE/rand-to-pbest
        Kr = 1 - F
        Kb = F
        return Kr, Kb


    def _sample_Jcr(self, F):
        # JADE/current-to-rand
        Kr = F
        Kb = np.zeros_like(F)
        return Kr, Kb


    def _sample_Jcrb(self, F):
        # JADE/current-rand-to-pbest
        K = np.zeros(F.size, dtype=int)
        Kb_i = np.round(F.ravel() * (self.nbins - 1)).astype(int)
        for i in xrange(K.size):
            Kidx = self.Kb_idx[Kb_i[i]]
            K[i] = Kidx[np.random.multinomial(1, self.mbins[Kidx]).astype(bool)]
        Kr = np.reshape(self.Kr[K], F.shape)
        Kb = F
        return Kr, Kb


    def _sample_K(self, F):
        # all other strategies
        K = np.where(np.random.multinomial(1, self.mbins, F.size))[1]
        Kr = np.reshape(self.Kr[K], F.shape)
        Kb = np.reshape(self.Kb[K], F.shape)
        return Kr, Kb


    def sample(self, F):
        """
        Return Kr and Kb factors sampled or calculated according to the initialized strategy
        Input:
            F: float[n,1] # F factor
        Output:
            float[n,1] # Kr factor
            float[n,1] # Kb factor
        """
        return self._sample(F)


    def _update_Jcrb(self, Skr, Skb, w_Sk):
        # JADE/current-rand-to-pbest
        if not(self.shade) or w_Sk is None:
            w_Sk = np.ones(Skr.size) / Skr.size
        K = self._bin_idx(Skr, Skb)
        mbins = np.zeros_like(self.mbins)
        for i in xrange(K.size):
            mbins[K[i]] += w_Sk[i]
        dirichlet_M = (1.0 - self.learning_rate)/self.learning_rate
        Kb_i = np.unique(np.round(Skb.ravel() * (self.nbins - 1)).astype(int))
        for i in xrange(len(Kb_i)):
            Kidx = self.Kb_idx[Kb_i[i]]
            mbins_i = convolve(mbins[Kidx], self.conv_kernel[Kb_i[i]])
            mbins_i /= mbins_i.sum()
            mbins[Kidx] = (self.mbins[Kidx] * dirichlet_M + mbins_i) / (dirichlet_M + 1.)
        self.mbins = mbins


    def _update_multinomial(self, Skr, Skb, w_Sk):
        # MADE
        if w_Sk is None:
            w_Sk = np.ones(Skr.size)/Skr.size
        K = self._bin_idx(Skr, Skb)
        mbins = np.zeros_like(self.mbins)
        for i in xrange(K.size):
            mbins[K[i]] += w_Sk[i]
        #Apply convolution kernel, actual scale factor in the resulting 2D convolution will be conv_kernel_scale*3/2
        # if too few nbins border effects will affect the distribution noticeably
        for i in xrange(self.nbins):
            mbins[self.Kb_idx[i]] = convolve(mbins[self.Kb_idx[i]], self.conv_kernel[i])
        for i in xrange(self.nbins):
            mbins[self.Kr_idx[i]] = convolve(mbins[self.Kr_idx[i]], self.conv_kernel[i])
        for i in xrange(self.nbins):
            mbins[self.Kc_idx[i]] = convolve(mbins[self.Kc_idx[i]], self.conv_kernel[i])
        dirichlet_M = (1.0 - self.learning_rate)/self.learning_rate
        mbins = (self.mbins * dirichlet_M + mbins) / (dirichlet_M + 1.)
        self.mbins = mbins


    def _update_none(self, Skr, Skb, w_Sk):
        pass


    def update(self, Skr, Skb, w_Sk=None):
        """
        Update K pdf according to the initialized strategy
        Input:
            Skr: float[n] # successful factors
            Skb: float[n]
            w_Sk=None: float[n] # optional factor weights to be used in strategies with weighted updates (SHADE, MADE)
        """
        if self.learning_rate > 0.:
            Skr = Skr.ravel()
            Skb = Skb.ravel()
            if len(Skr):
                self._update(Skr, Skb, w_Sk)


class CrPdf(object):
    """
    Crossover probability Cr probability distribution function
    Cr is sampled from N(muCr, sigmaCr)
    """
    def __init__(self, strategy, muCr=0.5, sigmaCr=0.1, learning_rate=0.1, conv_kernel_scale = 0.1, nbins=21):
        """
        Input:
            strategy: string # a mutation strategy string, i.e. "DE/rand"
            muCr=0.5 # mean of the crossover probability
            sigmaCr=0.1 # standard deviation for sampling the crossover probability
            learning_rate=0.1: float # how much the pdf is updated, must be 0 <= learning_rate <= 1
            conv_kernel_scale=0.1: float # standard deviation for the convolution kernel used to smooth adaptation in the multinomial strategy
            nbins=21: int # number of multinomial bins
        """
        strategy = strategy.split('/')
        if strategy[0] == 'JADE':
            self._update = self._update_JADE
            self._sample = self._sample_JADE
        elif strategy[0] == 'SHADE':
            self._update = self._update_SHADE
            self._sample = self._sample_SHADE
        elif strategy[0] == 'MSHADE':
            self._update = self._update_MSHADE
            self._sample = self._sample_multinomial
        elif strategy[0] == 'MADE':
            self._update = self._update_MADE
            self._sample = self._sample_multinomial
        else:
            self._update = self._update_none
            self._sample = self._sample_JADE
        # Crossover distribution parameters
        self.muCr = muCr
        self.sigmaCr = sigmaCr
        self.learning_rate = min(max(learning_rate, 0.), 1.)
        # SHADE
        M = int(round((1.0 - learning_rate)/learning_rate)) + 1
        self.SHM = np.ones(M) * muCr
        self.SHM_i = 0
        # MSHADE/MADE
        self.nbins = nbins
        if sigmaCr == 0:
            mbins = np.zeros(nbins)
            mbins[int(round(muCr*(nbins-1)))] = 1.
        else:
            mbins = stats.norm.pdf(np.linspace(0., 1., nbins), muCr, sigmaCr)
        self.mbins = mbins / mbins.sum()
        self.conv_kernel = convolution_kernel_1D(conv_kernel_scale, nbins, 'gaussian')


    def _sample_JADE(self, popsize):
        # JADE: sample Cr_i from N(muCr, sigmaCr)
        if self.sigmaCr == 0:
            Cr = self.muCr * np.ones(popsize)
        else:
            Cr = np.clip(np.random.normal(self.muCr, self.sigmaCr, popsize), 0., 1.)
        return Cr


    def _sample_SHADE(self, popsize):
        # SHADE: sample muCr_i from SHM, sample Cr_i from N(muCr_i, sigmaCr)
        muCr = self.SHM[np.argmax(np.random.multinomial(1, np.ones(self.SHM.size)/self.SHM.size, popsize), axis=1)]
        if self.sigmaCr == 0:
            Cr = muCr
        else:
            Cr = np.clip(np.random.normal(muCr, self.sigmaCr), 0., 1.)
        return Cr


    def _sample_multinomial(self, popsize):
        # MSHADE/MADE: sample muCr_i from multinomial distribution
        Cr = np.argmax(np.random.multinomial(1, self.mbins, popsize), axis=1) / float(self.nbins - 1)
        return Cr


    def sample(self, popsize):
        """
        Sample crossover probabilities from their distribution
        Input:
            popsize: int
        Output:
            float[popsize]
        """
        return self._sample(popsize)


    def _update_JADE(self, Scr, w_Scr):
        # Update muCr using JADE algorithm
        meanCr = Scr.sum()/Scr.size
        self.muCr = (1 - self.learning_rate)*self.muCr + self.learning_rate*meanCr


    def _update_SHADE(self, Scr, w_Scr):
        # Update SHADE muCr memory
        if w_Scr is None:
            w_Scr = np.ones(Scr.size) / Scr.size
        meanCr = np.dot(Scr, w_Scr)
        self.SHM[self.SHM_i] = meanCr
        self.SHM_i = (self.SHM_i + 1) % self.SHM.size


    def _update_MSHADE(self, Scr, w_Scr):
        # Update MSHADE muCr distribution
        if w_Scr is None:
            w_Scr = np.ones(Scr.size) / Scr.size
        meanCr = np.dot(Scr, w_Scr)
        if self.sigmaCr == 0:
            mbins = np.zeros(self.nbins)
            mbins[int(round(meanCr*(self.nbins-1)))] = 1.
        else:
            x = np.linspace(0., 1., self.nbins)
            mbins = stats.norm.pdf(x, meanCr, self.sigmaCr)
            mbins /= mbins.sum()
        dirichlet_M = (1.0 - self.learning_rate)/self.learning_rate
        mbins = (self.mbins * dirichlet_M + mbins) / (dirichlet_M + 1.)
        self.mbins = mbins


    def _update_MADE(self, Scr, w_Scr):
        # Update MADE muCr distribution
        if w_Scr is None:
            w_Scr = np.ones(Scr.size) / Scr.size
        mbins = np.zeros(self.nbins)
        for i in xrange(Scr.size):
            j = int(round(Scr[i] * (self.nbins - 1)))
            mbins[j] += w_Scr[i]
        mbins = convolve(mbins, self.conv_kernel)
        mbins /= mbins.sum()
        dirichlet_M = (1.0 - self.learning_rate)/self.learning_rate
        mbins = (self.mbins * dirichlet_M + mbins) / (dirichlet_M + 1.)
        self.mbins = mbins


    def _update_none(self, Scr, w_Scr):
        # non-adaptive strategies
        pass


    def update(self, Scr, w_Scr=None):
        """
        Update Cr pdf according to the initialized strategy
        Input:
            SCr: float[n] # successful factors
            w_Scr=None: float[n] # optional factor weights to be used in strategies with weighted updates (SHADE, MADE)
        """
        if self.learning_rate > 0.:
            Scr = Scr.ravel()
            if len(Scr):
                self._update(Scr, w_Scr)



class DE(object):

    def __init__(self, initial_population, objective_priorities=[], archive=None, mutation_strategy='DE/rand', muCr=0.5, sigmaCr=0.1, pbest=0.05, learning_rate=0.1, F_min=0.1, F_max=1.0, K_min=0.1, K_max=1.0, alpha=2, pareto_ranked=True, prio_dfactor=1.0):
        """
        Input:
            initial_population: DefaultPopulation() # population object
            objective_priorities=[]: list of tuple(int,int) # objective priorities for multiobjective
            archive=None: DefaultArchive() # archive object
            mutation_strategy='DE/rand': string # DE strategy
            muCr=0.5: float # mean for the crossover probability parameter
            sigmaCr=0.1: float # standard deviation for the crossover probability
            pbest=0.05: float  #
            learning_rate=0.1: float # learning rate parameter for adaptive strategies
            F_min=0.1: float # F parameter, minimum value
            F_max=1.0: float # F parameter, maximum value
            K_min=0.1: float # K parameter when applicable, minimum value (may be ignored deppending on strategy)
            K_max=1.0: float # K parameter when applicable, maximum value (may be ignored deppending on strategy)
            alpha=2: float # crowding measure for multiobjective
            pareto_ranked=True: bool # use Pareto ranking for multiobjective
            prio_dfactor=1.0: float # priority dimension factor
        """
        self.population = initial_population
        self.offspring = copy.deepcopy(initial_population)
        self.archive = archive if archive is not None else EmptyArchive(initial_population)
        self.priorities = objective_priorities
        self.strategy = mutation_strategy
        self.pbest = min(max(0.0, pbest), 1.0)
        # Sorting and ranking parameters
        self.alpha = alpha
        self.pareto_ranked = pareto_ranked
        self.prio_dfactor = prio_dfactor
        # setup factors
        self.F = FPdf(mutation_strategy, F_min, F_max, learning_rate)
        self.K = KPdf(mutation_strategy, K_min, K_max, learning_rate)
        self.Cr = CrPdf(mutation_strategy, muCr, sigmaCr, learning_rate)


    def run_generation(self, popsize, ranklist=None):
        """
        Run one evolutionary step
        Input:
            popsize: int # desired population size
            ranklist=None: int[:] or list of list(int) # rank sorted array of vector id (monoobjective) or list of pareto fronts (multiobjective) to be used for "best vector" selection
        Output:
            ranklist: int[:] or list of list(int) # rank sorted vector id of the population
        """
        if not(ranklist):
            ranklist = rank(self.population.get_objectives(), self.priorities)

        # select parent vectors
        xi = self.population.get_vectors()
        xa = self.archive.get_vectors()
        xr1, xr2, xr3 = sample_3vectors(xi, xa) # xr1, xr2 random vectors from population, xr3 random vector from population plus archive
        xpb = sample_pbest(xi, self.pbest, ranklist)

        # sample F and K values from their distribution
        F = self.F.sample(self.population.size)
        Kr, Kb = self.K.sample(F)

        # generate mutation vector using universal formula
        v = Kr*xr1 + Kb*xpb + (1.0 - Kr - Kb)*xi + F*(xr2 - xr3)

        # apply crossover
        Cr = self.Cr.sample(self.population.size)
        v, Cr = apply_crossover(xi, v, Cr)

        # update offspring population
        self.offspring.set_vectors(v)# update vector component
        self.offspring.repair()

        # compare each offspring vector against their current parent
        v_obj = self.offspring.get_objectives()
        xi_obj = self.population.get_objectives()
        c = compare_objectives(v_obj, xi_obj, self.priorities) # partial order comparison result
        if self.pareto_ranked:
            equal = np.all(v_obj == xi_obj, axis=1)
        else:
            # classical multiobjective DE
            equal = (c == 0)
        success = (c == 1) # indicator array of successful (dominating) offspring over their current parent

        w_success = nds.prio_distance(xi_obj[success], v_obj[success], self.priorities)
        w_success /= w_success.sum()

        # copy dominated parents to archive
        self.archive.append(self.population, np.where(success)[0])

        # combine successful parents and offspring
        self.population.join(self.offspring) # join parent and offspring populations
        # remove parents dominated or equally valued to their offspring and offspring dominated by their parent
        removed = np.concatenate(((c == 1)|equal, (c == -1))) #!!! DIFFERS ON EQUALLY VALUED TREATMENT
        old_idx = self.population.remove(removed)
        # track which individuals came from the parent population for further selection steps
        is_parent = (old_idx[:self.population.size] < self.offspring.size) & (old_idx[:self.population.size] >= 0)

        xi_obj = self.population.get_objectives()
        ranklist = rank(xi_obj, self.priorities)
        # apply further selection steps when necessary
        if self.population.size > popsize:
            # select individuals to remove by rank
            ranklist, remove_list = self.selection_byrank(popsize, ranklist)
            # put parents marked for removal in the archive
            self.archive.append(self.population, [i for i in remove_list if is_parent[i]])
            # select individuals to remove by crowding density
            ranklist, remove_cr = self.selection_bycrowding(popsize, ranklist, xi_obj)
            remove_list.extend(remove_cr)
            # remove selected individuals from population
            remove_list.sort()
            newi = self.population.remove(remove_list)
            self.change_indexes(newi, ranklist) # fix the indexes in ranklist
        assert self.population.size <= popsize # sanity check

        # update parameters
        self.F.update(F[success], w_success)
        self.K.update(Kr[success], Kb[success], w_success)
        self.Cr.update(Cr[success], w_success)

        return ranklist


    def selection_byrank(self, popsize, ranklist):
        """
        Return a list of candidates for removal selected by rank
        Input:
            popsize: int # desired population size
            ranklist: list of list of int # Rank or pareto sets as ordered list of sets (as lists) of individual indexes
        Output:
            list of list of int # ranklist with the lowest rank individuals removed
            list of int # list of individuals from ranklist selected to be removed
        """
        # find the rank cut
        n = 0
        i = 0
        while n < popsize:
            n += len(ranklist[i])
            i += 1
        maxrank = i - 1
        # select individuals from the rank cut on
        remove_list = []
        for r in ranklist[maxrank + 1:]:
            remove_list.extend(r)
        return ranklist[:maxrank+1], remove_list


    def selection_bycrowding(self, popsize, ranklist, xi_obj):
        """
        Return a list of candidates for removal selected by crowding density from the last Pareto front
        Input:
            popsize: int # desired population size
            ranklist: list of list of int # Rank or pareto sets as ordered list of sets (as lists) of individual indexes
            xi_obj: float[:,:] # individuals in the selection space
        Output:
            list of list of int # ranklist with the lowest rank individuals removed
            list of int # list of individuals from ranklist selected to be removed
        """
        assert xi_obj.ndim == 2 and xi_obj.shape[1] > 1
        n = sum(len(l) for l in ranklist)
        # remove vectors one by one based on crowding density
        k = 2 * (xi_obj.shape[1] - 1) + 1  # number of neighbors to calculate crowding density
        # find the excess most crowded vectors in the last pareto front
        crowded_list = remove_crowded(xi_obj, ranklist[-1], k, n - popsize, self.alpha, self.prio_dfactor, self.priorities)
        if len(crowded_list) < n - popsize:
            # for alpha<=1 and too sort ranklist[maxrank] the crowding meassure will fail if there aren't enough neigbors, then select the remaining excess vectors by any method
            candidates = list(set(ranklist).difference(crowded_list))
            crowded_list.extend(candidates[0:n - popsize - len(crowded_list)])
        ranklist[-1] = list(set(ranklist[-1]).difference(crowded_list))
        return ranklist, crowded_list


    def change_indexes(self, newi, ranklist):
        # fix the ranking sort with the new population indexes
        for i in xrange(len(ranklist)):
            for j in xrange(len(ranklist[i])):
                if newi[ranklist[i][j]] != -1:
                    ranklist[i][j] = newi[ranklist[i][j]]


def randidx_different(N, n, r):
    x = np.random.randint(0, n, N)
    for i in xrange(n):
        while any(x[i] == r[:,i]):
            x[i] = np.random.randint(0, n)
    return x


def sample_3vectors(xi, xa):
    """
    Return 3 sets of vectors sampled from the input arrays
    Input:
        xi: float[n,m] # origin row vectors
        xa: float[:,m] # additional row vectors
    Output:
        tuple of
            float[n,m] # xr1 random vector from xi different from xi
            float[n,m] # xr2 random vector from xi different from xr1
            float[n,m] # xr3 random vector from xi and xa different from xr2
    """
    popsize = xi.shape[0]
    ii = np.arange(0, popsize)
    # xr1 random vector from current population different from xi
    r1 = (np.random.randint(1, popsize, popsize) + ii) % popsize
    # r1 = randidx_different(popsize, popsize, ii.reshape((1,popsize)))
    xr1 = xi[r1]
    # xr2 random vector from current population different from xr1 and xi
    r2 = (np.random.randint(1, popsize, popsize) + r1) % popsize
    # r2 = randidx_different(popsize, popsize, np.vstack((ii, r1)))
    xr2 = xi[r2]
    # xr3 random vector from current population joined with current archive different from xr2, xr1 and xi
    r3 = (np.random.randint(1, popsize + xa.shape[0], popsize) + r2) % (popsize + xa.shape[0])
    # r3 = randidx_different(popsize, popsize + self.archive.size, np.vstack((ii, r1, r2)))
    xr3 = np.concatenate((xi, xa))[r3]
    return xr1, xr2, xr3


def sample_pbest(xi, p, ranklist):
    """
    Sample set of best vectors from the best p fraction of xi
    xi vectors are organized in p*len(xi) groups before sampling
    Input:
        xi: float[n,m] # origin row vectors
        ranklist: list of list of int # ranked list of sets (as lists) of input vector indexes (first is highest rank)
        p: float # fraction of best vectors to sample from
    Output:
        tuple of
            float[n,m] # vectors sampled from a set of best vectors of p*NP groups from xi
    """
    popsize = xi.shape[0]
    ngroups = max(1, int(np.ceil(p * popsize)))  # number of groups
    # shuffle vectors into groups. pbgroups[i] is a group, pbgroups[i][j] is a vector index into the current population
    pbgroups = [[] for i in xrange(ngroups)]
    for i in xrange(popsize):
        pbgroups[np.random.randint(ngroups)].append(i)
    # calculate the rank value of each vector, x_rank[i] is the rank of vector with index i
    x_rank = np.zeros(popsize, np.int)
    for i in xrange(len(ranklist)):
        for j in ranklist[i]:
            x_rank[j] = i
    # divide rank values into groups, x_pb_rank[i][j] is the rank corresponding to pbgroups[i][j]
    x_pb_rank = [x_rank[pbgroups[i]] for i in xrange(ngroups)]
    # find the best ranked vector(s) for each group
    pbest = np.hstack([np.array(pbgroups[i])[np.min(x_pb_rank[i])==x_pb_rank[i]] for i in xrange(ngroups) if len(x_pb_rank[i]) > 0])
    # sample from the set of best vectors
    rpb = np.random.choice(pbest, popsize)
    xpb = xi[rpb]
    return xpb


def apply_crossover(xi, v, Cr):
    """
    Apply random crossover on v from xi
    Return v vectors after crossover with xi and the effective crossover rate for each vector
    Input:
        xi: float[n,m] # row vectors
        v: float[n,m] # row vectors modified in place
        Cr: float[n] # crossover probability for each vector. Probability of picking a component from xi is 1-Cr
    Output:
        tuple of
            float[n,m] # v after crossover with x
            float[n] # Effective crossover rate for each vector
    """
    #negjCr = np.empty(xi.shape, dtype = np.bool)
    negjCr = np.random.binomial(1, 1.-Cr[None,...].T, xi.shape).astype(bool)
    noCr = np.all(negjCr, axis=1)
    j = np.random.randint(xi.shape[1], size=np.count_nonzero(noCr))
    negjCr[np.arange(xi.shape[0])[noCr], j] = False
    # popsize = xi.shape[0]
    # for i in xrange(popsize):
    #     negjCr[i] = np.random.binomial(1, 1-Cr[i], xi.shape[1])
    #     if np.all(negjCr[i]):
    #         negjCr[i,np.random.randint(xi.shape[1])] = False
    # #v = v*jCr + xi*(1-jCr)
    v[negjCr] = xi[negjCr]

    # calculate effective crossover rate
    Cr = 1 - np.sum(negjCr,1).astype(float)/negjCr.shape[1]
    return v, Cr


def crowding_meassure(d, alpha):
    """
    Returns the crowding alpha-meassure of an array of (euclidean) distances
    d[i] = sum_j(d[i,j]**(1-alpha))/(1-alpha) if alpha != 1 else sum_j(log(d[i,j]))
    Input:
        d: float[...,:] # array of distances
        alpha: int or float #alpha
    Output:
        float[...] # crowding distances
    """
    axis = d.ndim - 1
    if alpha == 0:#sortcut for a most common case
        return d.sum(axis)
    elif alpha == 1:
        return np.log(d).sum(axis)
    elif alpha == 2: #sortcut for a most common case
        return 1.0/(1.0/d).sum(axis)
    elif alpha == np.inf:
        return d.min(axis)
    else:
        return (d**(1 - alpha)).sum()**(1.0/(1.0 - alpha))


def remove_crowded(x_obj, S, k, n, alpha, prio_dfactor=1.0, priorities=[]):
    """
    Remove the n most crowded vectors with the alpha-measure of crowding density by considering k nearest neighbors
    a most crowded vector is removed from all neighborhoods before searching for the next most crowded vector
    alpha must be >1 or if there are not enough neighbors the algorithm will not be able to choose as the crowding measure will be infinite returning too many indexes
    Input:
        x_obj: float[:,:] # vectors
        S: array like of int # which vectors to consider
        k: int # number of nearest neighbors to consider
        n: int # number of vectors to find
        alpha: float # crowding measure
        prio_dfactor=1.0: float # factor to modulate how priority affects the crowding measure
        priorities=[]: list of tuple(int,int)
    Output:
        list of int # indexes of removed vectors
    """
    assert (len(S) > 1) and (n < len(S))
    if n > 0:
        #dfactor = np.std(x_obj,0) # objective normalization
        dfactor = np.ones(x_obj.shape[1])
        for i in xrange(len(priorities)):
            kmin, kmax = priorities[i]
            dfactor[kmin:kmax] *= prio_dfactor**i
        x_kdtree = KDTree(dfactor * x_obj[S])
        # find the k nearest neighbors for each vector
        di_knn, ii_knn = x_kdtree.query(x_obj[S], k + 1) #di_knn[i,1:] is a float[:] with the distance of each neighbor to vector i, i_knn[i,1:] is int[:] with the x_obj index of each neighbor
        di = crowding_meassure(di_knn[:,1:], alpha) # di[i] crowding measure of vector i
        for i in xrange(n):
            # find the vector with the smallest crowding measure
            i_min = di.argmin()
            # remove it from the list
            x_kdtree.set_present(i_min, False)
            di[i_min] = np.inf
            # update the crowding measure of every remaining vector which had i_min as a neighbor
            for j in xrange(len(S)):
                if x_kdtree.get_present(j) and i_min in ii_knn[j,1:]:
                    di_knn[j], ii_knn[j] = x_kdtree.query(x_obj[S[j]], k + 1)
                    if ii_knn[j,1] == len(S):
                        #this is the last remaining vector
                        break
                    di[j] = crowding_meassure(di_knn[j,1:], alpha)
        crowded_list = [S[i] for i in xrange(len(S)) if not(x_kdtree.get_present(i))]
    else:
        crowded_list = []
    return crowded_list


def compare_objectives(x, y, priorities):
    """
    Return 1 if x dominates y,-1 if x is dominated by y, 0 if no order can be stablished
    Input:
        x, y: float[n,:] or float[n] # row vectors or floats to compare
        priorities: list of tuple(int, int) # empty list for no priorities
    Output:
        int[n]
    """
    if x.ndim == 1 or x.shape[1] == 1:
        result = 1*(x < y) - 1*(x > y)
        result = result.ravel()
    else:
        result = np.empty(len(x), dtype=np.int)
        for i in xrange(len(x)):
            if nds.dominates(x[i], y[i], priorities):
                result[i] = 1
            elif nds.dominates(y[i], x[i], priorities):
                result[i] = -1
            else:
                result[i] = 0
    return result


def rank(x_obj, priorities=[]):
    """
    Input:
        x_obj: float[:,:] or float[:] # row vectors or floats (mono-objective case)
        priorities=[]: list of tuple(int,int)  # empty list for no priorities, otherwise a list of tuples defining the starting and ending+1 vector slice subscripts for each priority on the vector. The rightmost tuple defines the highest priority. For example, the priorities list of a 3D vector [x,y,z] with [x] on the highest priority and [y,z] on the lowest priority would be defined as [(0,1),(1,3)]
    Output:
        list of list of int # Ordered list of sets (as lists) of individual indexes
    """
    if x_obj.ndim == 1 or x_obj.shape[1] == 1:
        return [[i] for i in np.argsort(x_obj.ravel())]
    else:
        F = nds.pareto_front_sort(x_obj, priorities)
        return F
