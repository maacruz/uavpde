# -*- coding: utf-8 -*-
import numpy as np
import scipy.stats as stats
import types
try:
    raise ImportError # Disable numba for testing
    import numba
    from numba import vectorize
    # overload numba.jit to enable compilation cache by default
    def jit(signature_or_function=None, locals={}, target='cpu', cache=True, **options):
        return numba.jit(signature_or_function=signature_or_function, locals=locals, target=target, cache=cache, **options)
except ImportError:
    from numpy import vectorize
    def jit(signature=None, locals={}, target='cpu', cache=True, nopython=False, nogil=False, forceobj=False):
        if isinstance(signature, types.FunctionType):
            return signature
        def real_decorator(f):
            return f
        return real_decorator

#simple memoization for const numpy ndarrays
#just memorizes the last call to the function
#first argument of the memoized function must be a ndarray
#any following arguments must be hashable
class npmemoize(object):
    def __init__(self,f):
        self.f = f
        self.cache = [None,None]
    def __call__(self, *args):
        nparg = args[0].ctypes.data
        if (nparg,args[1:]) == self.cache[0]:
            return self.cache[1]
        else:
            self.cache[0] = (nparg,args[1:])
            self.cache[1] = self.f(*args)
            return self.cache[1]

def ordered_insert(u1, u2):
    """
    Takes two ordered 1D arrays and returns the result of joining them keeping the order. Values in the second input array already present in the first input array are not duplicated in the output
    Input:
        u1, u2: float[:] #arrays to join
    Output:
        (float[:], int[:]) # (joined array of size u1.size+u2.size, indexes of u2 values in the result array)
    """
    u = np.empty(u1.size + u2.size) #result array
    idx = np.empty(u2.size, int) #indexes of u2 values in the result array
    k = _ordered_insert_internal_loop(u1, u2, u, idx)
    return u[:k], idx

# Convenience function for jit compiling
# Returns modified inputs:
#   u: float[:]
#   idx: int[:]
@jit
def _ordered_insert_internal_loop(u1, u2, u, idx):
    i = 0 # u1 counter
    j = 0 # u2 counter
    k = 0 # u counter
    while (i < u1.size) or (j < u2.size):
        if (j < u2.size) and ((i == u1.size) or (u2[j] <= u1[i])):
            if (i == u1.size) or (u2[j] != u1[i]):
                u[k] = u2[j]
                idx[j] = k
                k += 1
            else:
                idx[j] = k
            j += 1
        if (i < u1.size)  and ((j == u2.size) or (u1[i] <= u2[j])):
            u[k] = u1[i]
            i += 1
            k += 1
    return k


def cauchy_sample(loc=0., scale=1., low=None, high=None, size=None):
    """
    Sample from a truncated Cauchy distribution by transforming a uniform sample using the percent point function (inverse CDF)
    Input:
        loc=0.0: float or float[:] # location parameter
        scale=1.0: float or float[:] # scale parameter
        low=None: float or float[:] # lowest possible value
        high=None: float or float[:] # highest possible value
        size=None: int or tuple of ints # Number of samples, If the given shape is(m, n, k), then m * n * k samples are drawn.  If size is None (default), a single value is returned if 'loc' and 'scale' are scalar. Otherwise, 'loc.size' samples are drawn.
    Output:
        float[size]
    """
    q_low = 0.5 + np.arctan((low-loc)/scale) / np.pi if low is not None \
        else np.zeros(loc.size) if isinstance(loc, np.ndarray) \
        else 0.
    q_high = 0.5 + np.arctan((high-loc)/scale) / np.pi if high is not None \
        else np.ones(loc.size) if isinstance(loc, np.ndarray) \
        else 1.
    q = np.random.uniform(q_low, q_high, size)
    x = loc + scale*np.tan(np.pi*q - np.pi/2.0)
    return x


def convolve(a,v):
    """
    Convolve 'a' with 'v'
    Input:
        a: float[:]
        v: float[:]
    Output:
        float[a.size]
    """
    c = np.convolve(a, v, 'same')
    d = v.size - a.size
    if d > 0:
        c = c[int(round(d/2.)):c.size-(d/2)]
    return c


def convolution_kernel_1D(sigma, n, kername='gaussian'):
    """
    Return a truncated normalized Gaussian or Cauchy kernel with at most n+1 elements if n is even or n if n is odd
    Kernel is truncated to +-3*sigma or to a maximum width 1 (i.e. from -0.5 to 0.5)
    Input:
        sigma: float # standard deviation
        n: int # number of elements to return (+1 if n is even)
        kername='gaussian': string # kernel to use, either 'gaussian' or 'cauchy'
    Output:
        float[:]
    """
    assert kername.lower() in ['gaussian', 'cauchy']
    if n % 2 == 0:
        n += 1
    x = np.linspace(0., 1., n)
    i_3sigma = (0.5 - 3. * sigma <= x) & (x <= 0.5 + 3. * sigma)
    if np.count_nonzero(i_3sigma) > 1:
        if kername.lower() == 'cauchy':
            kernel = stats.cauchy.pdf(x[i_3sigma], 0.5, sigma)
        elif kername.lower() == 'gaussian':
            kernel = stats.norm.pdf(x[i_3sigma], 0.5, sigma)
    else:
        kernel = np.array([1.])
    return kernel / kernel.sum()
