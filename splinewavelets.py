# -*- coding: utf-8 -*-
from utils import jit, ordered_insert
import numpy as np
import scipy
from scipy.special import binom
from scipy.interpolate import splev
from scipy.optimize import newton
import scipy.linalg
import rbandmat as bm
import operator as op

def uniform_knot_array(n, k):
    """
    Return clamped uniform knots
    Input:
        n: int # number of coeficients
        k: int # spline degree
    Output:
        float[:]
    """
    assert n >= k+1
    # clamped uniformly spaced knots
    knots = np.empty(n+k+1)
    knots[:k] = 0 # repeated knots at the start for clamping
    knots[n+1:] = 1 # repeated knots at the end for clamping
    knots[k:n+1] = np.linspace(0,1,n-k+1) # uniformly spaced knots
    return knots


def splmake(x, k=3):
    """
    Build a uniform interpolating spline (universal parameterization method)
    Input:
        x: float[...,:] # data points to interpolate (each row is a coordinate)
        k=3: int # spline degree (>=0)
    Output:
        tuple of
            tuple of (float[:], float[...,:], int) # b-spline in tck format
            float[:] # parameter values u for each point
    """
    #x = np.atleast_2d(x)
    x_shape = x.shape
    n = x.shape[-1] # number of data points
    assert n >= k+1
    # clamped uniformly spaced knots
    knots = uniform_knot_array(n, k)
    # make a special case of the 0 and 1 degree bsplines because the maximum can not be searched using the newton method
    if k <= 1:
        u = np.copy(knots[k:n+k])
        coefs = np.copy(x)
        return (knots, coefs, k), u
    # build the parameter vector u (coord. of the maximum of each bspline basis function)
    u = np.empty(n)
    u[0] = 0 # the first bspline basis function has the maximum at 0
    u[-1] = 1 # the last bspline basis function has the maximum at 1
    if n > 2:
        u[1:-1] = (knots[1:n-1] + knots[k+2:n+k])/2 # except the k leading and k trailing bspline basis functions all basis functions are symmetric
        c = np.zeros(n)
        # find the coord of the maximum for the k leading splines using the newton method
        for i in xrange(1,k):
            c[i] = 1 # bspline basis function i
            u[i] = (knots[k+i+1]-knots[i])*np.argmax(splev(np.linspace(knots[i],knots[k+1+i],6), (knots, c, k)))/5.+knots[i] # find a point close to the maximum
            f = lambda t: splev(t, (knots, c, k), der=1)
            fprime = lambda t: splev(t, (knots, c, k), der=2)
            u[i] = newton(f, u[i], fprime) # get a close estimate of the maximum
            u[-i-1] = 1 - u[i] # use symmetry for the trailing splimes
            c[i] = 0
    # Build and solve the system of equations to get the spline coefficients
    x = x.reshape((x.size/n,n))
    A = bm.BandMat(l=k-1, u=k-1, size=n)
    for i in xrange(n):
        j, B = splBasis(k, n, u[i])
        A.set_row(i, j, B)
    coefs = bm.solve(A, x.T)
    return (knots, coefs.T.reshape(x_shape), k), u


def double_knots(t):
    """
    Divide each knot interval of an interpolating spline in 2 by middle point insertion
    Input:
        t: float[:] # knot array starting with degree+1 leading 0 and degree+1 trailing 1
    Output:
        float[:] # refined knot array
    """
    degree = 0
    while t[degree] == 0:
        degree += 1
    degree -= 1
    n = t.shape[0] - 2*degree
    t1 = np.empty(2*n + 1 + 2*degree)
    j = 0
    for i in xrange(t.shape[0]):
        t1[j] = t[i]
        j += 1;
        if (i < t.shape[0] - 1) and (t[i] < t[i+1]): # take into account repeated knots
            t1[j] = (t[i] + t[i+1])/2.
            j += 1
    if j < t1.shape[0]:
        t1 = t1[:j]
    return t1


def splrefine(tck):
    """
    Uniform spline refinement through midpoint knot insertion (sequential Cox-de-Boor)
    Refines a b-spline inserting a knew knot in the midpoint between every two knots but the k=degree+1 leading and trailing ones, the spline shape doesn't change, but the number of control points is almost doubled
    Input:
        tck: tuple(float[:] or None, float[...,n], int) # b-spline in scipy representation (knot vector, coefs, degree). knot vector can be None, since the spline is uniform the knot vector is always known
    Output:
        (float[:] or None, float[...,2*n-degree], int) # refined b-spline (knot vector, coefs, degree)
    """
    knots, coefs, degree = tck
    c_shape = coefs.shape
    coefs = coefs.reshape((reduce(op.mul, c_shape[:-1], 1), c_shape[-1]))
    # n = length of the knots vector without all but one heading 0 and no trailing 1
    # d = knot step
    n = c_shape[-1] - degree
    if knots is not None:
        assert n == knots.size - 2*degree - 1
    q = np.empty((coefs.shape[0], 2*n + degree))
    _splrefine(coefs, degree, n, q)
    q = q.reshape(c_shape[:-1] + (-1,))
    if knots is not None: #Allow implicit knots
        t = double_knots(knots) #np.hstack((np.zeros(degree), np.linspace(0,1,2*n+1), np.ones(degree)))
    else:
        t = None
    return (t,q,degree)


# Convenience function for jit compiling
@jit
def _splrefine(coefs, degree, n, q):
    """
    Returns input q modified
    Input:
        coefs: float[:,:] # input array of size (m, n + degree), b-spline coefs
        degree: int
        n: int
        q: float[:,:] # array of size (m, 2*n + degree) to hold new b-spline refined coefs
    """
    # Example (3rd degree b-spline)
    # Implicit knot vector  [0 0 0 0 0.33 0.66 1 1 1 1]
    # Coefs:                                        P0  P1  P2  P3  P4  P5
    # c Memory position                             0   1   2   3   4   5   6   7   8
    # Starting                                      P0  P1  P2  P3<-This one inserted in the loop
    # 1st knot insertion knot0=0 newknot=0.16           Q00 Q01 Q02 P3  P4<-
    # 2nd knot insertion knot0=0.33 newknot=0.5                 Q10 Q11 Q12 P4  P5<-
    # 3rd knot insertion knot0=0.66 newknot=0.83                        Q20 Q21 Q22 P5
    # End result coefs                              P0  Q00 Q01 Q10 Q11 Q20 Q21 Q22 P5
    # implicit knot vector [0 0 0 0 0.16 0.33 0.5 0.66 0.83 1 1 1 1]
    #d = 1./n #knot size
    for x in xrange(q.shape[0]): #outer loop over each coordinate to keep coalesced memory access
        q[x,:degree] = coefs[x,:degree] #Starting
        for i in xrange(n): #knot counter
            k = 2*i + 1 #pointer to the current new coef
            q[x,k+degree-1] = coefs[x,i+degree]
            # fill the coefs going backwards to avoid overwriting old coefs still needed for the current set of coefs
            for j in xrange(degree-1,-1,-1):
                knot0 = (i - 0.5*((degree-1)-j)) #i*d - (((degree-1)-j)+1)/2*(1-p)*d - ((degree-1)-j)/2*p*d with p=0.5
                knot1 = (i+1+j)  #(i*d - (degree-1-j)*d) + degree*d
                knot0 = max(0,knot0)
                knot1 = min(n,knot1) #min(1,knot1)
                newknot = (i+0.5) # (i+p)*d
                l = float(newknot-knot0)/(knot1-knot0)
                q[x,k+j] = l*q[x,k+j] + (1-l)*q[x,k+j-1]
            q[x,k+degree] = coefs[x,i+degree] #last coef of the current run


def spldiscretize(tck, discr_d, u=np.array([])):
    """
    Discretize a b-spline using arc-length distance
    Input:
        tck: tuple of (float[:], float[:] or float[:,:], int) # b-spline in tck format
        discr_d: float # distance in arc-length units between sample points
        u: float[:] # array of parameter values u to sample points in addition to the points sampled according to distance along the spline
    Output:
        tuple of
            float[:] or float[:,:] # array of sampled points
            int[:] # indexes of the points with parameter u in the array of sampled points
    """
    # estimate curve length
    knots = tck[0]
    degree = tck[2]
    u_est = np.linspace(0, 1, (len(knots) - degree - 1) * 10)  # use 10 times the number of control points for curve lenght estimation
    traj_est = np.atleast_2d(splev(u_est, tck))  # get the estimation points coords from the b-spline
    dl = np.sqrt(np.sum(np.diff(traj_est) ** 2, 0))  # length delta
    l_acc = np.empty(len(u_est))  # emty array to hold the curve length
    l_acc[0] = 0
    np.cumsum(dl, out=l_acc[1:])  # integrate dl to get the length
    # calculate the parameter value at points equally spaced on the curve
    pl = np.linspace(0, l_acc[-1], int(np.round(l_acc[-1] / discr_d)) + 1)
    ul = np.interp(pl, l_acc, u_est)
    # insert the waypoints
    # u = self.knots[self.degree:-self.degree] # get the parameter vector by removing leanding 0's and trailing 1's from the knot vector
    # u_wp = u[self.fixed_wp_idx] #parameter at waypoints
    u_discr, u_idx_list = ordered_insert(ul, u)
    points = np.array(splev(u_discr, tck))
    return points, u_idx_list


def frenet3D(u, tck):
    '''
    Returns the 3D Frenet basis at the given parameter u in the b-spline tck
    Input:
        u: float or float[n]
        tck: tuple of (float[:], float[...,3,:], int)
    Output:
        tuple of float[...,3,n]*3 # T, N, B vectors
    '''
    knots, coefs, degree = tck
    epsilon = knots[degree+1] / 100.
    x0 = np.stack(splev(u, tck))
    x_1 = np.stack(splev(u - epsilon, tck))
    x1 = np.stack(splev(u + epsilon, tck))
    T = x1 - x_1
    T = T/np.linalg.norm(T, 2, 0)
    B = np.cross(x0-x_1, x1-x0, axis=-2)
    lB = np.linalg.norm(B, 2, 0)
    zeros = lB == 0
    if np.any(zeros):
        lB[zeros] = 1.
        B[...,2,zeros] = 1.
        B /= lB
        N = np.cross(B, T, axis=-2)
        N[...,0,np.all(N == 0, axis=-2)] = 1.
        N[...,zeros] /= np.linalg.norm(N[...,zeros], axis=-2)
        B[...,zeros] = np.cross(T[...,zeros], N[...,zeros], axis=-2)
    else:
        B /= lB
        N = np.cross(B, T, axis=-2)
    return T, N, B


def wsplSynthesisMatrixP(degree, ncoefs):
    """
    Returns a spline refinement matrix or synthesys filter P 
    The refinement matrix P relates the coefs of a spline and the refined spline obtained throug midpoint knot insertion
    s.t. c_{i+1} = P*c_i
    Input:
        degree: int
        ncoefs: int
    Output:
        float[ncoefs,2*ncoefs-degree]
    """
    assert ncoefs > degree
    assert degree > 0
    c = -np.ones(2*ncoefs - degree, dtype=int) #column where the nonzero entries start
    #P = np.zeros((2*ncoefs - degree, ncoefs))
    P = np.zeros((2*ncoefs - degree, (degree+1)/2+1)) #size of nonzero run in a row is (degree+1)/2+1
    _wsplSynthesisMatrixP(degree, ncoefs, c, P, np.zeros(P.shape[1]+1))
    return bm.BandMat(size=(2*ncoefs - degree, ncoefs), bandwidth=(degree+1)/2+1, data=(c,P))

# Convenience function for jit compiling
# Returns modified input:
#   P: float[2*ncoefs-degree,ncoefs]
#   c: float[2*ncoefs-degree]
#@jit
def _wsplSynthesisMatrixP(degree, ncoefs, columnP, P, tmp):
    n = ncoefs - degree
    #d = 1./n #knot size
    #c[:degree] = coefs[:degree]
    for i in xrange(degree):
        P[i, 0] = 1
        columnP[i] = i
    for i in xrange(n): #knot counter
        k = 2*i + 1 #pointer to the current new coef
        #c[k+degree-1] = coefs[i+degree]
        P[k+degree-1, 0] = 1
        columnP[k+degree-1] = i + degree
        for j in xrange(degree-1, -1, -1):
            knot0 = (i - 0.5*((degree-1)-j)) #i*d - (((degree-1)-j)+1)/2*(1-p)*d - ((degree-1)-j)/2*p*d with p=0.5 insertion point
            knot1 = (i+1 + j)  #(i*d - (degree-1-j)*d) + degree*d
            knot0 = max(0, knot0)
            knot1 = min(n, knot1) #min(1,knot1)
            newknot = (i+0.5) #(i+p)*d
            l = float(newknot-knot0) / (knot1-knot0)
            #c[k+j] = l*c[k+j] + (1-l)*c[k+j-1]
            tmp[:] = 0
            tmp[:P.shape[1]] = (1-l)*P[k+j-1]
            dc = columnP[k+j] - columnP[k+j-1]
            tmp[dc:dc+P.shape[1]] += l*P[k+j]
            P[k+j] = tmp[:P.shape[1]]
            columnP[k+j] = columnP[k+j-1]
        #c[k+degree] = coefs[i+degree] #last coef of the current run
        P[k+degree, 0] = 1
        columnP[k+degree] = i + degree


# The Bernstein weights are the weigths of the Bernstein basis polinomials for each B-spline scaling function
# There are ncoefs-degree spline scaling functions in a spline curve, each composed by (the linear combination of) degree+1 Bernstein polinomials, therefore (degree+1)*(ncoefs - degree) Bernstein polinomials
# Because of the partition theorem, all the weigths of a given Bernstein polinomial (a matrix row) must sum 1
# The BB-form matrix is obtained by repeated insertion of the existing knots (see De Boor, C. (1986). B (asic)-Spline Basics (No. MRC-TSR-2952). WISCONSIN UNIV-MADISON MATHEMATICS RESEARCH CENTER. https://www.cs.unc.edu/~dm/UNC/COMP258/Papers/bsplbasic.pdf)
# i.e. a 3rd degree spline with 5 control points has 2 Bezier curves, therefore 8 weights (4 for each curve), each control point can affect several curve segments (more as the degree increases)
# Result is in fortran order (columns) to increase efficiency in spline inner product matrix calculation
def wsplBernsteinWeightsMatrix(degree, ncoefs):
    """
    Returns the Bernstein-Bezier form matrix given the number of basis functions (=coefs) and degree of a spline
    Input:
        degree: int # degree of the B-Spline (greater than 0)
        ncoefs: int # num of B-spline coefs
    Output:
        float[(degree+1)*(ncoefs-degree),ncoefs] # Bernstein factors, B[(degree+1)*i:(degree+1)*(i+1),j] are the (degree+1) factors for knot i and scaling function j
    """
    assert ncoefs > degree
    assert degree > 0
    ncurves = ncoefs - degree #num of Bezier curves (== number of knot intervals)
    nBpol = degree + 1 #num of basis polinomials
    c = -np.ones(ncurves*nBpol, dtype=int) #column where the nonzero entries start
    #B = np.zeros((ncurves*nBpol, ncoefs),order='F')
    B = np.zeros((ncurves*nBpol, degree)) #size of nonzero run in a row is degree.
    _wsplBernsteinWeightsMatrix(degree, ncoefs, c, B, np.zeros(degree+1))
    return bm.BandMat(size=(ncurves*nBpol, ncoefs), bandwidth=degree, data=(c,B))

@jit
def _wsplBernsteinWeightsMatrix(degree, ncoefs, columnP, P, tmp):
    n = ncoefs - degree
    #d = 1./n #knot size
    #c[:degree] = coefs[:degree]
    for i in xrange(degree):
        P[i,i] = 1
        columnP[i] = 0
    P[degree, degree-1] = 1
    columnP[degree] = 1
    for i in xrange(1,n): #knot counter, skip the first knot because it is a 0
        columnP[(degree+1)*i] = i + 1
        for m in xrange(degree): #insertion counter
            k = (i-1)*degree + (i+1) + m #pointer to the current new coef
            for j in xrange(degree-1,-1,-1):
                knot0 = (i - min(1,max(0,degree-1-j-m))) #max(0,(((degree-1)-j-m)+1)/2))# *d
                knot1 = (i+1 + j)  #(i*d - (degree-1-j)*d) + degree*d
                knot0 = max(0,knot0)
                knot1 = min(n,knot1) #min(1,knot1)
                newknot = i #*d
                l = float(newknot-knot0)/(knot1-knot0)
                #c[k+j] = l*c[k+j] + (1-l)*c[k+j-1]
                tmp[:] = 0
                tmp[:degree] = (1-l)*P[k+j-1]
                dc = columnP[k+j] - columnP[k+j-1]
                tmp[dc:dc+degree] += l*P[k+j]
                if l == 0.0:
                    columnP[k+j] = columnP[k+j-1]
                    dc = 0
                P[k+j] = tmp[dc:dc+degree]
            #c[k+degree] = coefs[i+degree] #last coef of the current run
            P[k+degree, degree-1] = 1
            columnP[k+degree] = i + 1


def BernsteinInner(degree):
    """
    Returns the inner product matrix for Bernstein polinomials of the given degree
    See Jüttler, B. (1998). The dual basis functions for the Bernstein polynomials. Advances in Computational Mathematics, 8(4), 345-352. (Lemma 1) http://www.ag.jku.at/pubs/tdb98.pdf
    Input:
        degree: int
    Output:
        float[degree+1:degree+1]
    """
    i = np.tile(np.arange(0,degree+1),(degree+1,1))
    j = i.T
    iplusj = i+j
    return binom(degree,i)*binom(degree,j)/binom(2*degree,iplusj)/(2*degree+1)


def wsplScalingFInnerProductMatrix(degree, ncoefs):
    """
    Returns the Scaling function inner product matrix or Gramm matrix G=<phi_i|phi_j> for a spline
    This is a diagonal banded matrix of bandwidth 2*degree+1 that contains the inner product of every pair of scaling functions in the spline
    Input:
        degree: int # degree of the spline
        ncoefs: int # number of B-spline scaling functions
    Output:
        float[ncoefs,ncoefs]
    """
    assert ncoefs > degree
    G = bm.BandMat(size=ncoefs, l=degree, u=degree)
    Bw = wsplBernsteinWeightsMatrix(degree, ncoefs).copy(switch_storage_format=True)
    BI = BernsteinInner(degree) #inner product matrix
    _wsplScalingFInnerProductMatrix(degree, ncoefs, BI, Bw, G)
    return G

@jit
def _wsplScalingFInnerProductMatrix(degree, ncoefs, BI, Bw, G):
    nB = degree + 1 # number of basis functions
    n = ncoefs - degree # number of knot intervals
    for i in xrange(ncoefs):
        for j in xrange(i,ncoefs):
            # each spline coef can affect up to degree+1 segments, so the maximum distance with overlaping is 2*degree+1
            k0 = max(0,j-degree)
            k1 = min(j+degree,n)
            # B-splines in terms of all Bernstein polinomials, each group of (degree+1) rows is a set of coefs of Bernstein polinomials
            phy_i = Bw[nB*k0:nB*k1, i]
            phy_j = Bw[nB*k0:nB*k1, j]
            s = 0
            for k in xrange(k1-k0):
                s += dot_kernel(phy_i[nB*k:nB*(k+1)],phy_j[nB*k:nB*(k+1)],BI) #dot product of the corresponding sets of Bernstein coefs in spline curve i and spline curve j
            if s == 0: #since we know it is a banded matrix, stop the loop once we hit the first 0 in the row
                break
            G[i,j] = G[j,i] = s/n


@jit
def dot_kernel(x,y,K):
    """
    Returns the inner product of the input vectors using the matrix K: x*K*y
    Input:
        x,y: float[n]
        K: float[n,n]
    Output:
        float
    """
    n = len(K)
    s = 0.0
    for i1 in xrange(n):
        s1 = 0.0
        for i2 in xrange(n):
            s1 += x[i2] * K[i1, i2]
        s += s1 * y[i1]
    return s


def wsplSynthesisMatrices(degree, ncoefs):
    """
    Returns the wavelet coeficient matrix or synthesys filter Q, the synthesys filter P and the scaling function inner product matrix I
    The wavelet coeficient matrix allows to reconstruct the higher resolution spline coefs from the detail coefs w_i by the folowing relation c_{i+1} = P*c_i + Q*w_i
    Input:
        degree: int
        ncoefs: int
    Output:
        float[2*ncoefs-degree,ncoefs], float[2*ncoefs-degree,ncoefs-degree], float[2*ncoefs-degree,2*ncoefs-degree] # P, Q and G
    """
    P = wsplSynthesisMatrixP(degree, ncoefs)
    G = wsplScalingFInnerProductMatrix(degree, 2*ncoefs - degree)
    SFdot = bm.dot(P.T, G) # 2 level scaling functions dot product matrix <phi_j|phi_(j+1)> = P'*G
    SFdot = SFdot.copy(switch_storage_format=True) # switch to column storage
    SFdot.slicing = 'non-zero' # we only need non zero submatrix for rank calculation
    c0 = 0
    c1 = 0
    q = []
    r = np.empty(((ncoefs - degree +1)/2 , 2), int) #column where the nonzero entries start
    for i in xrange((ncoefs - degree + 1)/2): # SFdot is symmetric, therefore Q will be symmetric
        # Find the first non rank deficient submatrix
        if c1>c0:
            while np.linalg.matrix_rank(SFdot[:,c0:c1]) < c1-c0:
                c0 += 1
        # Find the first rank deficient submatrix
        c1 += 1
        while np.linalg.matrix_rank(SFdot[:,c0:c1]) == c1-c0:
            c1 += 1
        #SFdot[:,c0:c1] will have a nullspace of just 1 vector
        q.append(scipy.linalg.qr(SFdot[:,c0:c1].T, pivoting=True, check_finite=False)[0][:,-1]) #the nullspace vector is the last column of the Q matrix from QR factorization
        r[i] = c0, c1
        c0 += 1
    Q = bm.BandMat(size=(2*ncoefs - degree, ncoefs - degree), bandwidth=np.max(np.diff(r)), store_as_rows=False)
    n, m = Q.shape
    for i in xrange((ncoefs - degree + 1) / 2):  # SFdot is symmetric, therefore Q will be symmetric
        i_qmax = np.argmax(np.abs(q[i]))
        q[i] = q[i]/q[i][i_qmax] # normalize wrt the largest element
        #Q[c0:c1,i] =  q
        Q.set_column(i,r[i][0],q[i])
        #use symmetry to fill the other half of the Q matrix
        #Q[n-c1:n-c0,m-1-i] =  np.flipud(q)
        Q.set_column(m-1-i, n-r[i][1], np.flipud(q[i]))
    return P, Q, G


def BernsteinFunc(x, degree):
    """
    Returns a vector of Bernstein polinomials evaluated at the given point x
    Input:
        x: float # point where the polinomials are evaluated
        degree: int # degree of the polinomials
        n: int # derivative of the polinomials
    Output:
        float[degree+1] # result
    """
    i = np.arange(degree+1)
    B = binom(degree,i) * x**i * (1-x)**(degree-i)
    return B


def BernsteinFuncDer(x, degree, n):
    """
    Returns a vector of the n-th derivative of Bernstein polinomials evaluated at the given point x
    see Doha, E. H., Bhrawy, A. H., & Saker, M. A. (2011). On the derivatives of Bernstein polynomials: an application for the solution of high even-order differential equations. Boundary Value Problems, 2011(1), 1. https://www.emis.de/journals/HOA/BVP/Volume2011/829543.pdf
    Input:
        x: float # point where the polinomials are evaluated
        degree: int # degree of the polinomials
        n: int # derivative of the polinomials
    Output:
        float[degree+1] # result
    """
    B = BernsteinFunc(x, degree-n)
    if n > 0: #simple optimization for the n==0 case
        Bd = np.zeros(degree+1)
        p = np.arange(1,degree+1).prod()/np.arange(1,degree-n+1).prod()
        for i in xrange(degree+1):
            k = np.arange(max(0,i+n-degree),min(i,n)+1)
            Bd[i] = p*np.sum((-1)**(k+n)*binom(n,k)*B[i-k])
    else:
        Bd = B
    return Bd


def splBasis(degree, ncoefs, t, der=0):
    """
    Return the value of each possibly non-zero BSpline basis function (or their nth derivative) at the given point and the knot at which the first possibly non-zero basis function starts
    Basis functions are evaluated at closed left open right [ti,ti+1) intervals, except for the last knot which is closed right [tn-1, tn]. Therefore, at discontinuity points, the left value is returned.
    Input:
        degree: int # BSpline degree
        ncoefs: int # number of coefs of the BSpline
        t: float # parameter value at which the basis functions are evaluated
        der=0: int # nth derivative
    Output:
        tuple of
            int # first knot with a non-zero basis function value at t
            float[:] # basis function nth derivative value
    """
    n = ncoefs - degree
    knot = min(int(t * n), n-1) # base knot
    B = [_splBasis_CoxDeBoor(degree, degree, n, i, t, der) for i in xrange(knot, knot+degree+1)]
    return knot, np.array(B)


def _splBasis_CoxDeBoor(nclamped, degree, n, i, t, d):
    if (i < 0) or (i >= n+nclamped) or (d > degree):
        return 0.0
    u_i = max(0, i-nclamped)
    u_id, u_i1, u_id1 = max(0,min(i-nclamped+degree,n)), max(0,min(i+1-nclamped,n)), max(0,min(i-nclamped+degree+1,n))
    if degree == 0:
        if (u_i <= t*n < u_i1) or ((t == 1.0) and (t*n == u_i1)):
            return 1.0
        else:
            return 0.0
    else:
        if d == 0:
            N1 = _splBasis_CoxDeBoor(nclamped, degree-1, n, i, t, 0)
            N2 = _splBasis_CoxDeBoor(nclamped, degree-1, n, i+1, t, 0)
            u = t*n
            c1 = (u - u_i)/(u_id - u_i) if u_i != u_id else 0
            c2 = (u_id1 - u)/(u_id1 - u_i1) if u_i1 != u_id1 else 0
            N = N1*c1 + N2*c2
        else:
            N1 = _splBasis_CoxDeBoor(nclamped, degree-1, n, i, t, d-1)
            N2 = _splBasis_CoxDeBoor(nclamped, degree-1, n, i+1, t, d-1)
            c1 = 1.0/(u_id - u_i) if u_i != u_id else 0
            c2 = 1.0/(u_id1 - u_i1) if u_i1 != u_id1 else 0
            N = n*degree*(c1*N1 - c2*N2)
        return N


def wsplConstraintsMatrix(degree, ncoefs, constraints):
    """
    Calculate the constraint matrix C for a constrained wavelet analysis
    C is sparse with degree+1 nonzero elements per row
    Input:
        degree: int # B-spline degree
        ncoefs: int # number of coefs in the B-spline
        constraints: list of tuple(int, float) # constraints expressed as B-spline derivative (0=position, 1=tangent, ...) and parameter value where the constrainit is held
    """
    nc = len(constraints) # number of constraints
    #n = ncoefs - degree # number of knot intervals
    C = np.zeros((nc, ncoefs)) #FIXME: this should be a bm of bandwidth degree+1
    for i in xrange(nc):
        d, t = constraints[i]
        j, B = splBasis(degree, ncoefs, t, d)
        C[i,j:j+degree+1] = B
    return C




class SplineWaveletTransform(object):
    def __init__(self, degree=3, constraints=[]):
        """
        Input:
        degree: int # B-spline degree
        constraints: list of tuple(int, float) # constraints expressed as B-spline derivative (0=position, 1=tangent, ...) and parameter value where the constrainit is held
        """
        self.degree = degree
        self.constraints = constraints
        # cache
        self.PQ = dict() # Refinement and Detail coefs matrices
        self.G = dict() # Scaling functions inner product (Gramian) matrices for each level
        self.L = dict() # G Cholesky factor
        self.J = dict() # Wavelets inner product matrices J Cholesky factor
        self.M = dict() # Constraint solution matrices
        self.C = dict() # Constraint matrices
        

    @staticmethod
    def __M(C, L):
        # Calculate constraint solution matrix M = pinv(C*inv(G)) s.t. S*w = M*C_i+1*Q*w
        nc, n = C.shape
        AT = np.empty((n, nc),order='F') # Temporary A matrix transposed
        for i in xrange(nc):
            AT[:,i] = bm.cho_solve(L,C[i,:])
        q, r = scipy.linalg.qr(AT,mode='economic') # LQ factors == (QR).T
        M = np.dot(q,np.linalg.inv(r.T))
        return M
    

    def reconstruct(self, scalingcoefs, detailcoefs):
        """
        Returns the scaling funtion (or spline) coefs at twice the resolution, given the scaling function coefs and detail coefs
        Input:
            scalingcoefs: float[...,n] # array with the spline coefs at the current resolution
            detailcoefs: float[...,m] # array with the detail coefs
        Output:
            float[...,2*n-degree] # array with the spline coefs at the next resolution
        It must hold that m+n == 2*n-degree
        """
        c = scalingcoefs.reshape((reduce(op.mul, scalingcoefs.shape[:-1], 1), scalingcoefs.shape[-1])).T
        w = detailcoefs.reshape((reduce(op.mul, detailcoefs.shape[:-1], 1), detailcoefs.shape[-1])).T
        n, m = c.shape[0], w.shape[0]
        assert n >= self.degree+1 and n + m == 2*n - self.degree and np.issubdtype(scalingcoefs.dtype, np.floating) and np.issubdtype(detailcoefs.dtype, np.floating)
        #read matrices from cache or calculate them if cache fails
        try:
            P, Q = self.PQ[n]
        except KeyError:
            P, Q, self.G[2*n-self.degree] = wsplSynthesisMatrices(self.degree, n)
            self.PQ[n] = (P, Q)
        try:
            L = self.L[n]
        except KeyError:
            self.G[n] = wsplScalingFInnerProductMatrix(self.degree, n)
            L = self.L[n] = bm.cholesky(self.G[n])
        Qw = bm.dot(Q, w)
        # Apply constraints
        if len(self.constraints) > 0:
            try: #read matrices from cache
                C = self.C[2*n-self.degree]
                M = self.M[n]
            except KeyError:
                C = self.C[2*n-self.degree] = wsplConstraintsMatrix(self.degree, 2*n-self.degree, self.constraints)
                C_2 = self.C[n] = wsplConstraintsMatrix(self.degree, n, self.constraints)
                assert len(self.constraints) <= np.linalg.matrix_rank(C_2) # we can only handle the under-constrained case
                M = self.M[n] = SplineWaveletTransform.__M(C_2, L)
            y = np.dot(M, bm.dot(C, Qw))
            c_r = bm.cho_solve(L, y)
            c = c - c_r
        # Reconstruction c_l+1 = P*c + Q*w
        c1 = splrefine((None, c.T, self.degree))[1] + Qw.T
        return c1.reshape(scalingcoefs.shape[:-1] + (-1,))
    

    def decompose(self, scalingcoefs):
        """
        Returns the scaling funtion coefs (at half the resolution) and detail coefs, given the scaling function coefs
        Input:
            scalingcoefs: float[...,n] # array with the spline coefs at the current resolution. To be decomposable, n must be greater than both degree+1 and the number of constraints, and (n+degree) must be even
        Output:
            float[...,(n+degree)/2], float[...,(n-degree)/2] # array with the spline coefs at half the resolution, array with the detail coefs
        """
        c = scalingcoefs.reshape((reduce(op.mul, scalingcoefs.shape[:-1], 1), scalingcoefs.shape[-1])).T
        n = c.shape[0]
        assert n>self.degree+1 and n>len(self.constraints) and (n+self.degree) % 2 == 0 and np.issubdtype(scalingcoefs.dtype, np.float)
        #read matrices from cache or calculate them if cache fails
        try:
            P, Q = self.PQ[(n+self.degree)/2]
            G = self.G[n]
            L = self.L[(n+self.degree)/2]
            J = self.J[(n+self.degree)/2]
        except KeyError:
            P, Q, G = wsplSynthesisMatrices(self.degree, (n+self.degree)/2)
            self.G[n] = G
            self.L[n] = bm.cholesky(G)
            self.PQ[(n+self.degree)/2] = (P, Q)
            self.G[(n+self.degree)/2] = wsplScalingFInnerProductMatrix(self.degree, (n+self.degree)/2)
            L = self.L[(n+self.degree)/2] = bm.cholesky(self.G[(n+self.degree)/2])
            J = bm.dot(Q.T, bm.dot(G,Q))
            J = self.J[(n+self.degree)/2] = bm.cholesky(J)
        ic = bm.dot(G, c)
        # Spline coefs: solve G_(l-1)*c_(l-1) = P.T*G*c
        b = bm.dot(P.T, ic)
        c1 = bm.cho_solve(L, b)
        # Wavelet coefs: solve J_(l-1)*w_(l-1) = Q.T*G*c
        b = bm.dot(Q.T, ic)
        w1 = bm.cho_solve(J, b)
        # Apply constraints
        if len(self.constraints) > 0:
            try: #read matrices from cache
                C = self.C[n]
                M = self.M[(n+self.degree)/2]
            except KeyError:
                C = self.C[n] = wsplConstraintsMatrix(self.degree, n, self.constraints)
                C_2 = self.C[(n+self.degree)/2] = wsplConstraintsMatrix(self.degree, (n+self.degree)/2, self.constraints)
                M = self.M[(n+self.degree)/2] = self.__M(C_2, L)
            y = np.dot(M, bm.dot(C, bm.dot(Q,w1)))
            c_r = bm.cho_solve(L, y)
            c1 = c1 + c_r
        return c1.T.reshape(scalingcoefs.shape[:-1] + (-1,)), w1.T.reshape(scalingcoefs.shape[:-1] + (-1,))
    

    def inverse(self, splinewavelet, nlevels=-1):
        """
        Inverse transform or reconstruction for at most the given number of levels
        Input:
            splinewavelet: list of float[...,:]
            nlevels=-1: int # number of levels to decompose, if -1 returns the spline at its highest resolution
        Output:
            tuple of
                list of float[...,:]
                int # effective number of levels
        """
        c = splinewavelet[0]
        if nlevels < 0:
            nlevels = len(splinewavelet) - 1
        else:
            nlevels = min(nlevels, len(splinewavelet) - 1)
        i = -1 # for the nlevels == 0 case, otherwise i would be undefined
        for i in xrange(nlevels):
            c = self.reconstruct(c, splinewavelet[i + 1])
        wlist = [c]
        wlist.extend(splinewavelet[nlevels+1:])
        return wlist, i+1
    

    def direct(self, splinewavelet, nlevels=-1):
        """
        Direct transform or decomposition for the given number of levels at most.
        Input:
            splinewavelet: list of float[...,:] # splinewavelet
            nlevels=-1: int # number of levels to decompose, if -1 decompose to the maximum possible level
        Output:
            tuple of
                list of float[...,:] # splinewavelet
                int # effective number of levels
        """
        c = splinewavelet[0]
        wlist = splinewavelet[:0:-1]
        ncoefs = c.shape[-1]
        if nlevels < 0:
            nlevels = int(np.log2(ncoefs))
        n = 0
        while n < nlevels:
            if ((c.shape[-1] + self.degree) % 2 != 0) or (c.shape[-1] <= self.degree + 1) or (c.shape[-1] <= len(self.constraints)):
                break
            c, w = self.decompose(c)
            wlist.append(w)
            n += 1
        wlist.append(c)
        wlist.reverse()
        return wlist, n


    def array_to_splw(self, splwarray, currentlevel=0):
        """
        Expand an array packed splinewavelet as a list of coefficients
        Input:
            splwarray: float[...,:]
            currentlevel=0: int
        Output:
            list of float[...,:]
        """
        maxlevel = 0
        n = splwarray.shape[-1]
        # N = 2*n - degree => n = (N + degree)/2
        # m = n - degree => m = (N - degree)/2
        ncoefs = [(n, 0)]
        while ((n + self.degree) % 2 == 0) and (n  > self.degree + 1) and (n  > len(self.constraints)):
            maxlevel += 1
            n = (n + self.degree)/2
            m = n - self.degree
            ncoefs.append((n, m))
        assert currentlevel <= maxlevel
        icoefs, m = ncoefs[currentlevel]  # current number of scaling coefs
        wlist = [splwarray[..., :icoefs]] # scaling coefs
        if m > 0:
            wlist.append(splwarray[..., icoefs:icoefs + m])
        for i in xrange(currentlevel-1, 0, -1):
            icoefs, m = ncoefs[i]
            wlist.append(splwarray[..., icoefs:icoefs + m])
        return wlist


    def splw_to_array(self, splw):
        """
        Return a splinewavelet packed as an array from an expanded list of coefficients
        Input:
            splw: list of float[...,:] # a splinewavelet
        Output:
            tuple of
                float[...,:] # array
                int     # current decomposition level
        """
        splwarray = np.concatenate(splw, -1)
        currentlevel = len(splw) - 1
        return splwarray, currentlevel


