# def test_UAV():
#     casepath = "SingleUAV/Caso1"
#     pconf, sceneconf, trayconf, algconf = uav.readLegacyxmlConfig(casepath)
#     scenery = Scenery(pconf, sceneconf)
#     scenery.uav_height_safetymargin = -1.0
#     scenery.dtedmap.dted[:] = 0
#     obj = uav.UAVObjective(scenery, pconf, trayconf)
#
#     points = np.zeros((1,3,4)) # simplest population trajectory: size = 1, 4 control points
#     pop = uav.TrajectoryPopulationBSplines(points, obj)
#     pop.evaluate()
#     assert np.all(pop.get_vectors() == np.zeros((1,3*(4+2))))
#     #assert np.all(pop.get_objectives() == np.zeros((1,10)))
#     traj, wpi = pop.discretize(0)
#     assert np.all(np.vstack(traj) == np.zeros((3,2)))
#     pop.refine()
#     assert np.all(pop.get_vectors() == np.zeros((1,3*(6*2-pop.degree))))
#     assert pop.fixed_wp_idx == [0,6]
#
#     points[0,0,:] = [0,1,2,3]
#     pop = uav.TrajectoryPopulationISplines(points, obj)
#     pop.evaluate()
#     #assert np.all(pop.get_objectives() == np.zeros((1,10)))
#     traj, wpi = pop.discretize(0)
#
