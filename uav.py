import xmltodict
import numpy as np

class UAV(object):

    def __init__(self, idn, a, b, c):
        """
        Input:
            idn: string # UAV id
            a: float
            b: float
            c: float
        """
        self.idn = idn
        self.a = a
        self.b = b
        self.c = c
        self.V = 247.0 # km/h
        self.fuel = 2000.0
        self.flightCeiling = 20000.0 # m
        self.waypoints = []
        self.orientation = 0.0
        self.trajectory_population = None

    def get_waypoints(self):
        """
        Output:
            float[3,:] # array of waypoint coordinates, rows are x,y,z, each waypoint a column
        """
        return np.array([w[1:] for w in self.waypoints]).T

    def get_refueling_wpidx(self):
        """
        Output:
            list of int
        """
        return [i for i in xrange(len(self.waypoints)) if self.waypoints[i][0] == 'Refuelling']


def readLegacyxmlConfig(casepath):
    """
    Read and parse the legacy xml files problem.xml, scenery.xml, tray.xml and alg.xml from a directory returning a dictionary for each file
    The xml:
        <Tag1 p1='val1' p2='val2'>
           <RepeatedTag>v1</RepeatedTag>
           <RepeatedTag>...</RepeatedTag>
           <Tag2>v2</Tag2>
        </Tag1>
    becomes:
        {'Tag1':{'@p1':'val1','@p2':'val2','RepeatedTag':['v1',...],'Tag2':'v2'}}
    Input:
        casepath: string # path to the directory where the xml files are
    Output:
        tuple of
            dict(),  # problem.xml
            dict(),  # scenery.xml
            dict(),  # tray.xml
            dict()   # alg.xml
    """
    pconf = xmltodict.parse(open(casepath + '/problem.xml'))['ConfiguracionProb']
    sceneconf = xmltodict.parse(open(casepath + '/scenery.xml'))['Context']
    trayconf = xmltodict.parse(open(casepath + '/tray.xml'))['ConfiguracionTray']
    algconf = xmltodict.parse(open(casepath + '/alg.xml'))['ConfiguracionAlg']
    return pconf, sceneconf, trayconf, algconf
