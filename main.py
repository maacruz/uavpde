import sys
import time
import os.path
import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import scipy.io as scio
import sceneries
import trajectories
import uav as uavs
import differentialevolution as de
import populations
import warnings
warnings.filterwarnings("ignore", message="elementwise comparison failed; returning scalar instead")
warnings.filterwarnings("ignore", message="Using default event loop until function specific to this GUI is implemented")

# strategy:(archive size, pbest fraction, sigmaCr, use pareto ranking for selection, last front to consider for best individual)
strategies = {'DE/rand':(0,1.0,0.0,False,2),
              'DE/best':(0,0.0,0.0,False,2),
              'JADE/current-to-pbest':(4,0.05,0.1,True,-1),
              'JADE/rand-to-pbest':(4,0.05,0.1,True,-1),
              'JADE/current-rand-to-pbest':(4,0.05,0.1,True,-1)}


def run(casepath, result_path, planner_name, strategy=None, Nr=1, popsize=0, maxiter=0, encoding='BSplines', showplot=True):
    # load xml configuration files and build scenery
    pconf, sceneconf, trayconf, algconf = uavs.readLegacyxmlConfig(casepath)
    scenery = sceneries.Scenery(pconf, sceneconf)
    # seed the rng for repeatability of populations among planners
    np.random.seed(17)
    # seeds for the initial population generator for each run
    seeds = np.random.random_integers(2**30-1,size=Nr)
    # run the Nr restarts for this scenery and planner configuration
    for n in xrange(Nr):
        for uav_idx in xrange(len(scenery.UAVlist)):
            uav = scenery.UAVlist[uav_idx]
            # setup optimization algorithm
            if maxiter <= 0:
                maxiter = int(algconf[uav.idn]['NgeneracionesOffline'])
            Cr = float(algconf[uav.idn]['CrossoverParam'])
            F_min = float(algconf[uav.idn]['MutationF1'])
            F_max = float(algconf[uav.idn]['MutationF2'])
            if not(strategy):
                i = algconf[uav.idn]['MutationType']
                if i <= 2:
                    strategy = 'DE/rand'
                else:
                    strategy = 'DE/best'
                if ((i == 0) or (i == 3)):
                    F_max = F_min
            archive_size, p, sigmaCr, pareto_ranked, best = strategies[strategy]
            if popsize <= 0:
                popsize = int(algconf[uav.idn]['Nindividuos'])
            # setup objective function
            objective = uavs.UAVObjective(scenery, pconf, trayconf, uav_idx)
            # population initialization
            limits = [(scenery.dtedmap.x_westlimit, scenery.dtedmap.x_eastlimit), (scenery.dtedmap.y_southlimit, scenery.dtedmap.y_northlimit), (scenery.uav_height_safetymargin, uav.FlightCeiling)]
            waypoints = [wp[1:] for wp in uav.waypoints] # keep the waypoint coordinates
            np.random.seed(seeds[n])
            control_points, wp_idx_list = trajectories.legacy_random_initialization(popsize, waypoints, limits)
            print 'Dimensionality:%i' % ((control_points.shape[-1] - len(waypoints))*control_points.shape[-2])
            if encoding == 'BSplines':
                population = uavs.TrajectoryPopulationBSplines(control_points, objective, wp_idx_list)
            else:
                population = uavs.TrajectoryPopulationISplines(control_points, objective, wp_idx_list, alpha=int(trayconf[uav.idn]['Alpha']))
            print 'Number of free control points:', population.individuals.shape[-1]/3
            # setup population archive
            if archive_size > 0:
                archive = populations.DefaultArchive(archive_size*population.size, population)
            else:
                archive = None
            # initialize optimizator
            de_opt = de.DE(population, objective_priorities=objective.priorities, archive=archive, mutation_strategy=strategy, muCr=Cr, sigmaCr=sigmaCr, c=0.1, p=p, pareto_ranked=pareto_ranked, F_min=F_min, F_max=F_max, isSHADE=False, prio_dfactor=1.0)
            # setup grahics
            if showplot:
                fig_scenery, ax_scenery = scenery.draw()
                fig_scenery.show()
                fig = plt.figure()
                ax = fig.add_subplot(111, projection='3d')
                #plt.hold(True)
                #xv, yv = np.meshgrid(np.linspace(0,1,scenery.dtedmap.dted.shape[1]), np.linspace(0,1,scenery.dtedmap.dted.shape[0]))
                #ax.plot_surface(xv,yv,scenery.dtedmap.dted, cmap=cm.hot, alpha=0.2)
                for i in xrange(de_opt.population.size):
                    traj, wp = de_opt.population.discretize(i)
                    ax.plot(traj[0],traj[1],traj[2])
                plt.show(block=False)
            # run optimization loop
            pf = [] # population pareto rank
            for i in xrange(maxiter):
                pf = de_opt.run_generation(pf[:best])
                # show and draw best solution set every 10 iterations
                if i % 10 == 0:
                    print 'Iteration:%i' % i, len(pf)
                    print 'Objectives:\n',[o[0] for o in objective.objective_list]
                    for j in xrange(len(pf)):
                        print '%2i %s' % (j, de_opt.population.get_objectives()[pf[j]])
                    if showplot:
                        plt.cla() # clear axis
                        for i in pf[0]:
                            traj, wp = de_opt.population.discretize(i,10000)
                            ax.plot(traj[0],traj[1],traj[2])
                        ax.scatter(traj[0][wp],traj[1][wp], traj[2][wp], s=100, c='r')
                        ax.set_zlim([0, uav.FlightCeiling*2])
                        plt.pause(0.001)
            if showplot:
                for i in pf[0]:
                    traj, wp = de_opt.population.discretize(i)
                    x, y = scenery.dtedmap.getGridCoordinates(traj[0],traj[1])
                    ax_scenery.plot(x, y)
                    fig_scenery.show()
                plt.close(fig_scenery)
            if result_path:
                # save best solution set
                mat_output = {'generation':maxiter, 'population':de_opt.population.get_vectors()[pf[0]], 'objectives':de_opt.population.get_objectives()[pf[0]]}
                mat_fname = os.path.join(result_path, '%s_%03i.mat' % (planner_name, n))
                print 'Saving %s' % mat_fname
                scio.savemat(mat_fname, mat_output)
                if showplot:
                    fig_scenery.savefig(os.path.join(result_path, '%s_traj_%03i.png' % (planner_name, maxiter)))
                    plt.savefig(os.path.join(result_path, '%s_traj3D_%03i.png' % (planner_name, maxiter)))
                    plt.close()

def syserror(s):
    sys.exit('Error: %s\n' % s)
    
class Config(object):
    def __init__(self, fname):
        self.config = self.readFile(fname)
        
    def readFile(self, fname):
        lv = dict()
        gv = dict()
        exec(open(fname), gv, lv)
        return lv
    
    def getValue(self, key, datatype, default, valid, n):
        if self.config.has_key(key):
            if isinstance(self.config[key], list):
                value = self.config[key]
                if n>0 and len(value) != n:
                    syserror('%s must have %i values' % (key, n))
                bad = []
                for i in xrange(n):
                    if not(isinstance(value[i], datatype)) or (valid and value[i] not in valid):
                        bad.append[i]
                if bad:
                    syserror('%s has errors in position %s' % (key, ','.join(bad)))
            elif isinstance(self.config[key], datatype):
                value = [self.config[key]]*n
                if valid and value not in valid:
                    syserror('%s must be %s' % (key, valid))
            else:
                if valid:
                    syserror('%s must be a %s or a list of %s of %s' % (key, datatype, datatype, ' '.join(valid)))
                else:
                    syserror('%s must be a %s or a list of %s' % (key, datatype, datatype))
        else:
            value = [default]*n
        return value

if __name__ == '__main__':
    np.set_printoptions(precision=4, suppress=True, linewidth=120)
    if len(sys.argv) < 1:
        print 'Usage: main [casepath|configfile] [resultpath]'
        exit()

    argparser = argparse.ArgumentParser()
    argparser.add_argument('fname', help='Path to xml files or experiment configuration file. If configfile is provided any other command line option will be ignored', metavar='casepath|configfile')
    argparser.add_argument('resultspath', help='Path to a directory where results will be saved', nargs='?', default=None)
    argparser.add_argument('-n', '--popsize', help='Population size', type=int, nargs='?', default=0)
    argparser.add_argument('-g', '--maxgen', help='Number of generations the optimizer will run', type=int, nargs='?', default=0)
    argparser.add_argument('-e', '--encoding', help='How the trajectory is encoded in the genome. Either BSplines or ISplines', nargs='?', choices=['BSplines','ISplines'], default='BSplines')
    argparser.add_argument('-s', '--strategy', help='DE strategy', nargs='?', choices=strategies.keys())
    argparser.add_argument('-p', '--showplot', help='Show plots while running', action='store_true')
    argparser.add_argument('-r', '--nr', help='Number of times to run', type=int, nargs='?', default=1)

    args = argparser.parse_args()
    fname = args.fname
    
    if os.path.isdir(fname):
        planner_name = os.path.split(fname)[-1]
        if args.popsize and args.popsize < 5:
            syserror("Population size must be at least 5")
        run(fname, args.resultspath, planner_name, args.strategy, args.nr, args.popsize, args.maxgen, args.encoding, args.showplot)
    else:
        config = Config(fname)

        planner_name_list = config.getValue('ExpName', str, default=None, valid=None, n=0)
        if not(planner_name_list):
            syserror('Missing ExpName config key')
        nexp = len(planner_name_list)

        Nr = config.getValue('Nr', int, default=1, valid=None, n=nexp)
        result_path = config.getValue('ResultPath', str, default=None, valid=None, n=nexp)
        if not(all(result_path)):
            print 'Warning: Some experiments will not be saved to disk'
        strategy = config.getValue('Strategy', str, default='JADE/current-rand-to-pbest', valid=set(strategies.keys()), n=nexp)
        legacy_config_path = config.getValue('LegacyConfigPath', str, default='', valid=None, n=nexp)
        encoding = config.getValue('Encoding', str, default='BSplines', valid=set(['BSplines','ISplines']), n=nexp)
        popsize = config.getValue('PopSize', int, default=0, valid=None, n=nexp)
        maxiter = config.getValue('MaxGeneration', int, default=0, valid=None, n=nexp)
        showplot = any(config.getValue('ShowPlot', bool, default=True, valid=None, n=nexp))

        for i in xrange(nexp):
            print 'Running planner %s\n' % planner_name_list[i]
            run(legacy_config_path[i], result_path[i], planner_name_list[i], strategy[i], Nr[i], popsize[i], maxiter[i], encoding[i], showplot)
    