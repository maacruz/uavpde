import numpy as np
from abc import ABCMeta, abstractmethod, abstractproperty

class AbstractPopulation(object):
    __metaclass__ = ABCMeta
    
    def __init__(self):
        self.size = 0
        self.individuals = None
    
    @abstractmethod
    def get_vectors(self):
        """
        Return an array with the vector population. Vectors are rows
        Output:
            float[:,:] or None
        """
        pass
    
    @abstractmethod
    def set_vectors(self, v):
        """
        Set all the vector values from the given array. Objective values become invalid.
        Input:
            v: float[:,:] # vector values. Vectors are rows. v.shape[0] must be equal to population size, v.shape[1] must be equal to initialized vector dimension
        """
        pass

    @abstractmethod
    def get_objectives(self):
        """
        Return an array with objective values for the vector population.
        If self.random_initialize() or self.evaluate() haven't been called objective values will be invalid raising an exception
        Output:
            float[:,...]
        """
        pass
    
    @abstractmethod
    def evaluate(self):
        """
        Calculate objective values for the vector population calling the objective function set on instantiation
        """
        pass

    @abstractmethod
    def join(self, population):
        """
        Grow the population with individuals from another compatible population
        Input:
            population: AbstractPopulation # A population with the same vector and objective dimensionality
        """
        pass

    @abstractmethod
    def remove(self, removed):
        """
        Remove individuals from the population
        Input:
            removed: array_like # Individuals to remove
        """
        pass

    @abstractmethod
    def repair(self):
        """
        Fix individuals out of limits
        """
        pass




class EmptyArchive(AbstractPopulation):
    def __init__(self, population):
        super(EmptyArchive, self).__init__()
        self.vector_dimension = population.vector_dimension

    def get_vectors(self):
        return np.zeros((0, self.vector_dimension))

    def set_vectors(self, v):
        pass

    def get_objectives(self):
        return np.array([])

    def evaluate(self):
        pass

    def join(self, population):
        pass

    def remove(self, removed):
        return np.array([])

    def append(self, population, x):
        pass

    def repair(self):
        pass




class DefaultPopulation(AbstractPopulation):
    """
    Base class implementing a population of vectors
    """
    def __init__(self, objective_function, objective_dimension, vector_dimension):
        """
        Input:
            objective_function: float[:,:] function(...) # an objective function to be called by self.evaluate()
            objective_dimension: int # number of objective values
            vector_dimension: int # vector dimension
        """
        self.size = 0
        self.vector_dimension = vector_dimension
        self.valid_range = None
        self.individuals = None
        self.objective_values = np.zeros((0, objective_dimension))
        self.objective_function = objective_function
        self.objective_dimension = objective_dimension
        self.valid_objective_values = False


    def random_initialize(self, size, init_range, valid_range=None):
        """
        Initialize a population of random vectors
        Input:
            size: int # number of vectors in the population
            valid_range: list of tuple(float, float) # minimum and maximum possible initialization values for each vector coordinate
            valid_range=None: list of tuple(float, float) # minimum and maximum possible valid values for each vector coordinate
        """
        assert len(init_range) == self.vector_dimension
        data = np.empty((size, self.vector_dimension))
        for i in xrange(self.vector_dimension):
            data[:, i] = (init_range[i][1] - init_range[i][0])*np.random.rand(size) + init_range[i][0]
        self.init_fromdata(data, valid_range)


    def init_fromdata(self, data_array, valid_range=None):
        """
        Initialize a population of vectors from the given array
        Input:
            data_array: float[:,:] # row vectors
            valid_range=None: list of tuple(float, float) # minimum and maximum possible valid values for each vector coordinate
        """
        assert data_array.shape[1] == self.vector_dimension and data_array.ndim == 2
        self.size = data_array.shape[0]
        self.valid_range = valid_range
        self.individuals = data_array
        self.valid_objective_values = False


    def get_vectors(self):
        """
        Return an array with the vector population. Vectors are rows
        Output:
            float[:,:] or None
        """
        if self.individuals is not None:
            return self.individuals[:self.size, :]
        else:
            return None
    

    def set_vectors(self, v):
        """
        Make the given array the vector population . Objective values become invalid.
        Input:
            v: float[:,:] # vector values. Vectors are rows. v.shape[1] must be equal to initialized vector dimension
        """
        if v.shape[1] != self.vector_dimension:
            raise Exception("Vector dimensions of vector array v and population do not match")
        self.individuals = v
        self.valid_objective_values = False
        self.size = v.shape[0]


    def get_objectives(self):
        """
        Return an array with objective values for the vector population.
        If self.random_initialize() or self.evaluate() haven't been called objective values will be invalid raising an exception
        Output:
            float[:,...]
        """
        if not(self.valid_objective_values):
            self.evaluate()
        if self.individuals is not None:
            return self.objective_values[:self.size]
        else:
            return None


    def repair(self):
        """
        Fix any vector values not within valid range by reflection of coordinates out of range
        """
        if self.individuals is not None and self.valid_range is not None:
            lower = np.array([i[0] for i in self.valid_range])
            upper = np.array([i[1] for i in self.valid_range])
            x = self.individuals[:self.size]
            x[x < lower] = (2*lower - x)[x < lower]
            x[x > upper] = (2*upper - x)[x > upper]
            np.clip(x, lower, upper, x)
            self.valid_objective_values = False


    def evaluate(self):
        """
        Calculate objective values for the vector population calling the objective function set on instantiation
        The objective function must return float[n,:] or float[n]
        """
        if self.individuals is not None:
            if self.objective_values is None or self.objective_values.shape[0] < self.individuals.shape[0]:
                self.objective_values = np.empty((self.individuals.shape[0], self.objective_dimension))
            self.objective_values[:self.size] = np.reshape(self._call_objective_function(), (self.size, self.objective_dimension))
            self.valid_objective_values = True


    def _call_objective_function(self):
        """
        Call the objective function set at initialization with appropriate arguments and return its result
        """
        return self.objective_function(self.individuals[:self.size])


    def join(self, population):
        """
        Grow the population with individuals from another compatible population
        Input:
            population: DefaultPopulation # A population with the same vector and objective dimensionality
        """
        if self.individuals is None:
            raise Exception("Population not initialized")
        if population.individuals is None:
            raise Exception("Argument population not initialized")
        if self.size + population.size <= len(self.individuals):
            self.individuals[self.size:self.size + population.size] = population.individuals[:population.size]
            if self.valid_objective_values and population.valid_objective_values:
                self.objective_values[self.size:self.size + population.size] = population.objective_values[:population.size]
            else:
                self.valid_objective_values = False
        else:
            self.individuals = np.concatenate((self.individuals[:self.size], population.individuals[:population.size]))
            if self.valid_objective_values and population.valid_objective_values:
                self.objective_values = np.concatenate((self.objective_values[:self.size], population.objective_values[:population.size]))
            else:
                self.objective_values = np.empty((self.size + population.size, self.objective_dimension))
                self.valid_objective_values = False
        self.size += population.size
    

    def remove(self, removed):
        """
        Remove individuals from the population
        Input:
            removed: bool[:] or int[:] or list of int # a boolean array the same length as the population size or a list/array of indexes
        Output:
            int[:] # new index of each original population member, -1 if removed
        """
        if self.individuals is None:
            raise Exception("Population not initialized")
        if isinstance(removed, np.ndarray):
            removed = removed.ravel()
            if removed.dtype == np.dtype('bool'):
                assert len(removed) == self.size
                removed_idx = np.where(removed)[0]
            else:
                removed_idx = removed
                removed = np.zeros(self.size, dtype=np.bool)
                removed[removed_idx] = True
        else:
                removed_idx = removed
                removed = np.zeros(self.size, dtype=np.bool)
                removed[removed_idx] = True            
        old_idx = np.arange(self.size)
        c = 0
        last = self.size - 1
        while (c < len(removed_idx)) and (removed_idx[c] < last):
            while removed[last]:
                old_idx[last] = -1
                last -= 1
            if removed_idx[c] >= last:
                break
            self.individuals[removed_idx[c]] = self.individuals[last]
            if self.valid_objective_values:
                self.objective_values[removed_idx[c]] = self.objective_values[last]
            old_idx[last] = removed_idx[c]
            old_idx[removed_idx[c]] = -1
            c += 1
            last -= 1
        self.size = self.size - len(removed_idx)
        return old_idx
    



class DefaultArchive(DefaultPopulation):
    """
    Class implementing an archive for a DefaultPopulation object
    Archives up to a given number of individuals, once reached as newer individuals are added older individuals are forgotten
    """
    def __init__(self, maxsize, population):
        """
        Input:
            maxsize: int # maximum size of the archive
            population: DefaultPopulation # population this is an archive for
        """
        if population.individuals is None:
            raise Exception("Attempting to create an archive for a non initialized population")
        super(DefaultArchive, self).__init__(population.objective_function, population.objective_dimension, population.vector_dimension)
        self.size = 0
        self.pointer = 0
        self.maxsize = maxsize
        self.individuals = np.zeros((maxsize, self.vector_dimension))

    def append(self, population, idx):
        """
        Append individuals from the given population
        Input:
            population: DefaultPopulation
            idx: int[:] or list of int # population members to append
        Result:
            Archive grows by len(idx) with the given individuals.
            If archive size is already the maximum size, len(idx) randomly chosen old members are forgotten
        """
        if population.individuals is None:
            raise Exception("Attempting to add members for a non initialized population")
        if self.maxsize > 0:
            if self.size < self.maxsize:
                for i in idx:
                    self.individuals[self.pointer] = population.individuals[i]
                    self.pointer = (self.pointer + 1) % self.maxsize
                    self.size += 1
                self.size = min(self.size, self.maxsize)
            else:
                self.individuals[np.random.randint(0,self.maxsize,len(idx))] = population.individuals[idx]
