import numpy as np


# stub
import scipy.linalg
from numpy import dot
import bandmat as bm
import bandmat.linalg as bmlinalg
"""
Rectangular banded matrix algebra library
Deppends on the squared banded matrix library BandMat (https://github.com/MattShannon/bandmat/tree/master/bandmat)
"""


class BandMat(bm.BandMat):
    """
    Main class
    """
    def __init__(self, size, l=0, u=0, bandwidth=0, store_as_rows=True, data=None, transposed=False, slicing='full'):
        """
        Initialize an empty banded matrix
        Input:
            size: int or tuple(int,int) # matrix size or shape
            l = 0: int # number of upper diagonals if it is a square matrix
            u = 0: int # number of lower diagonals if it is a square matrix
            bandwidth = 0: int # bandwidth if it is a rectangular matrix
            store_as_rows = True: bool
            data = None: float[l+u+1,size] or tuple(int[size[0]],float[size[0],bandwidth]) or tuple(int[size[1]],float[bandwidth,size[1]]) # initialization data
        """
        # banded matrix must be either square or rectangular
        assert (l >= 0 and u >= 0 and bandwidth == 0) or (isinstance(size, tuple) and bandwidth > 0and l == u == 0)
        self.slicing = slicing
        if bandwidth == 0:
            # square matrix
            if isinstance(size, tuple):
                if size[0] != size[1]:
                    raise Exception("rectangular matrix with no bandwidth")
                size = size[0]
            self.square = True
            if data is None:
                data = np.zeros((l+u+1, size))
            else:
                if not(isinstance(data, np.ndarray)):
                    raise Exception("data must be a numpy array")
            super(BandMat, self).__init__(l, u, data, transposed)
            self.shape = (size,size)
        else:
            # rectangular matrix
            self.square = False
            shape = size[::-1] if transposed else size
            if data is None:
                self.start = -np.ones(shape[int(not(store_as_rows))], dtype=int)
                if store_as_rows:
                    self.data = np.zeros((shape[0], bandwidth))
                else:
                    self.data = np.zeros((bandwidth, shape[1]), order='F')
            else:
                if not(isinstance(data, tuple)):
                    raise Exception("data must be a tuple(int[], float[]) or None")
                self.start = data[0]
                self.data = data[1]
                assert len(self.start) == self.data.shape[int(not(store_as_rows))] == shape[int(not(store_as_rows))] and self.data.shape[int(store_as_rows)] == bandwidth
            self.bandwidth = bandwidth
            self.store_as_rows = store_as_rows
            self.shape = size
            self.transposed = transposed

    def set_row(self, row, start, row_data):
        if not(0 <= start < self.shape[1]):
            raise IndexError("start position %i is out of bounds" % start)
        if not(0 <= row < self.shape[0]):
            raise IndexError("row %i is out of bounds" % row)
        if self.transposed:
            if not(self.square) and self.store_as_rows:
                raise Exception("set_column not possible for this storage format and transpose state")
            self.__set_column(row, start, row_data)
        else:
            if not(self.square) and not(self.store_as_rows):
                raise Exception("set_column not possible for this storage format and transpose state")
            self.__set_row(row, start, row_data)

    def __set_row(self, row, start, row_data):
        if self.square:
            u = self.u
            l = self.l
            for j in xrange(max(0,u+row-start-l-u), min(len(row_data),u+row-start+1,self.data.shape[1]-start)):
                # if (u+row-j-start >= 0) and (u+row-j-start < l+u+1) and (j+start < self.data.shape[1]):
                self.data[u+row-j-start, j+start] = row_data[j]
        else:
            row_data = row_data[:self.bandwidth]
            self.data[row, :len(row_data)] = row_data
            self.start[row] = start

    def set_column(self, column, start, column_data):
        if not(0 <= start < self.shape[0]):
            raise IndexError("start position %i is out of bounds" % start)
        if not(0 <= column < self.shape[1]):
            raise IndexError("column %i is out of bounds" % column)
        if self.transposed:
            if not(self.square) and not(self.store_as_rows):
                raise Exception("set_column not possible for this storage format and transpose state")
            self.__set_row(column, start, column_data)
        else:
            if not(self.square) and self.store_as_rows:
                raise Exception("set_column not possible for this storage format and transpose state")
            self.__set_column(column, start, column_data)

    def __set_column(self, column, start, column_data):
        if self.square:
            i = max(0, self.u - column)
            j = max(0, column - start - self.u)
            k = (self.l + self.u + 1) - i
            column_data = column_data[j:j+k]
            self.data[i:i+len(column_data), column] = column_data
        else:
            column_data = column_data[:self.bandwidth]
            self.data[:len(column_data), column] = column_data
            self.start[column] = start

    def get_column(self, i):
        if not(0 <= i < self.shape[1]):
            raise IndexError("column %i is out of bounds" % i)
        if self.transposed:
            return self.__get_row(i)
        else:
            return self.__get_column(i)

    def __get_column(self, i):
        if self.square:
            j = max(0, self.u - i)
            column_data = self.data[j:,i]
            start = max(0, i - self.u)
            return start, column_data
        else:
            if not(self.store_as_rows):
                return self.start[i], self.data[:, i]
            else:
                raise NotImplementedError

    def get_row(self, i):
        if not (0 <= i < self.shape[0]):
            raise IndexError("row %i is out of bounds" % i)
        if self.transposed:
            return self.__get_column(i)
        else:
            return self.__get_row(i)

    def __get_row(self, i):
        if self.square:
            raise NotImplementedError
        else:
            if self.store_as_rows:
                return self.start[i], self.data[i]
            else:
                raise NotImplementedError

    def __repr__(self):
        if self.square:
            return super(BandMat, self).__repr__()
        else:
            return ("BandMat(shape=%r, bandwidth=%r, store_as_rows=%r, transposed=%r, %r, %r)" %
                    (self.shape, self.bandwidth, self.store_as_rows, self.transposed, self.start, self.data))

    def full(self):
        """
        Returns a conventional numpy array
        Output:
            float[shape[0],shape[1]]
        """
        if self.square:
            return super(BandMat, self).full()
        else:
            shape = self.shape[::-1] if self.transposed else self.shape
            if self.store_as_rows:
                B = np.zeros(shape)
                for i in xrange(self.data.shape[0]):
                    if self.start[i] >= 0:
                        B[i, self.start[i]:self.start[i]+self.bandwidth] = self.data[i, :shape[1]-self.start[i]]
            else:
                B = np.zeros(shape, order='F')
                for i in xrange(self.data.shape[1]):
                    if self.start[i] >= 0:
                        B[self.start[i]:self.start[i]+self.bandwidth, i] = self.data[:shape[0]-self.start[i], i]
        if self.transposed:
            B = B.T
        return B

    def toarray(self):
        return self.full()

    @property
    def T(self):
        if self.square:
            return BandMat(size=self.shape[0], l=self.u, u=self.l, data=self.data, transposed=not(self.transposed))
        else:
            return BandMat(size=self.shape[::-1], bandwidth=self.bandwidth, store_as_rows=self.store_as_rows, data=(self.start, self.data), transposed=not(self.transposed))

    def __getitem__(self, item):
        """
        [] indexing with numpy array semantics
        Input:
            item: int or tuple(int or tuple,int or tuple) # retrieve a row (int case), an element (tuple(int,int) case), a row slice (tuple(int,slice) case), a column slice (tuple(slice, int) case), or a submatrix slice (tuple(slice,slice) case).
        Output:
            elem: float or float[:] or float[:,:]
        """
        if type(item) is int:
            row = item
            column = slice(None)
        elif type(item) is slice:
            row = item
            column = slice(None)
        else:
            row, column = item
        if self.transposed:
            row, column = column, row
        # submatrix slice
        if type(row) == type(column) == slice:
            if self.square: # not implemented for square BandMat
                raise NotImplementedError
            else:
                data = self.data
                start = self.start
                bandwidth = self.bandwidth
                if not(self.store_as_rows) and row.start is None and row.stop is None and row.step is None:
                    i0, i1, step = column.indices(data.shape[1])
                    if self.slicing == 'non-zero':
                        row_min = np.min(start[i0:i1:step])
                        row_max = min(np.max(start[i0:i1:step]) + bandwidth, self.shape[int(self.transposed)])
                    elif self.slicing == 'full':
                        row_min = 0
                        row_max = self.shape[int(self.transposed)]
                    result = np.zeros((row_max - row_min, (i1-i0+step-1)/step), order='F')
                    for i in xrange(i0, i1, step):
                        j0 = start[i]-row_min
                        j1 = min(j0 + bandwidth, row_max - row_min)
                        result[j0:j1, i-i0] = data[:j1-j0, i]
                    return result if not(self.transposed) else result.T
                elif self.store_as_rows and column.start is None and column.stop is None and column.step is None:
                    i0, i1, step = row.indices(data.shape[0])
                    if self.slicing == 'non-zero':
                        column_min = np.min(start[i0:i1:step])
                        column_max = min(np.max(start[i0:i1:step]) + bandwidth, self.shape[int(not(self.transposed))])
                    elif self.slicing == 'full':
                        column_min = 0
                        column_max = self.shape[int(not(self.transposed))]
                    result = np.zeros(((i1-i0+step-1)/step, column_max - column_min))
                    for i in xrange(i0, i1, step):
                        j0 = start[i]-column_min
                        j1 = min(j0 + bandwidth, column_max - column_min)
                        result[i-i0, j0:j1] = data[i,:j1-j0]
                    return result if not (self.transposed) else result.T
                else:
                    raise NotImplementedError
        # row/column slice
        elif (type(row) == slice and type(column) == int) or (type(row) == int and type(column) == slice):
            if type(column) == int:
                start, data = self.__get_column(column)
                i0, i1, step = row.indices(self.shape[int(self.transposed)])
            else:
                start, data = self.__get_row(row)
                i0, i1, step = column.indices(self.shape[int(not(self.transposed))])
            i0_d = i0 if start <= i0 else i0+step*((start-i0+step-1)/step)
            i1_d = i1 if start+len(data) > i1 else max(i0_d, start+len(data))
            data_new = np.zeros((i1-i0+step-1)/step)
            data_new[(i0_d-i0)/step:(i1_d-i0+step-1)/step] = data[i0_d-start:i1_d-start:step]
            return data_new
        # one element
        elif type(row) == type(column) == int:
            if not((0 <= row < self.shape[int(self.transposed)]) and (0 <= column < self.shape[int(not(self.transposed))])):
                e = (row, int(self.transposed)) if not (0 <= row < self.shape[int(self.transposed)]) else (column, int(not(self.transposed)))
                raise IndexError("Index %i is out of bounds for axis %i" % e)
            if self.square:
                if 0 <= row-column+self.u <= self.data.shape[0]:
                    return self.data[row-column+self.u, column]
                else:
                    return 0.0
            else:
                if self.store_as_rows:
                    if self.start[row] < 0:
                        raise IndexError("Uninitialized element at index %r" % item)
                    if 0 <= column-self.start[row] < self.bandwidth:
                        return self.data[row, column-self.start[row]]
                    else:
                        return 0.0
                else:
                    if self.start[column] < 0:
                        raise IndexError("Uninitialized element at index %r" % item)
                    if 0 <= row-self.start[column] < self.bandwidth:
                        return self.data[row-self.start[column], column]
                    else:
                        return 0.0

    def __setitem__(self, item, value):
        # no fancy indexing yet, just 1 element
        if self.transposed:
            column, row = item
        else:
            row, column = item
        if not((0 <= row < self.shape[int(self.transposed)]) and (0 <= column < self.shape[int(not(self.transposed))])):
            e = (row, int(self.transposed)) if not (0 <= row < self.shape[int(self.transposed)]) else (column, int(not(self.transposed)))
            raise IndexError("Index %i is out of bounds for axis %i" % e)
        if not(self.square):
            raise NotImplementedError
        else:
            if 0 <= row-column+self.u <= self.data.shape[0]:
                self.data[row-column+self.u, column] = value
            else:
                raise IndexError("Index [%i,%i] is out of band" % item)

    def equiv(self, l_new=None, u_new=None, transposed_new=None, zero_extra=False):
            raise NotImplementedError("Not implemented")

    def copy(self, switch_storage_format=False):
        if self.square:
            B = super(BandMat, self).copy()
            return BandMat(self.shape[0], l=B.l, u=B.u, data=B.data)
        else:
            if self.transposed:
                B = BandMat(self.shape, bandwidth=self.bandwidth, store_as_rows=not(self.store_as_rows), data=(self.start.copy(), self.data.T.copy()))
                if switch_storage_format:
                    return B
                else:
                    return B.__switch_storage_copy()
            else:
                if switch_storage_format:
                    return self.__switch_storage_copy()
                else:
                    return BandMat(self.shape, bandwidth=self.bandwidth, store_as_rows=self.store_as_rows, data=(self.start.copy(), self.data.copy()))


    def __switch_storage_copy(self):
        start = self.start
        data = self.data
        if np.any(start < 0):
            raise Exception("Matrix must be completely initialized")
        assert np.all(np.diff(start) >= 0)
        store_as_rows_new = not(self.store_as_rows)
        bandwidth_new = 0
        j = 1
        for i in xrange(len(start)):
            while j<len(start) and (start[j]-start[i] < self.bandwidth):
                j += 1
            bandwidth_new = max(bandwidth_new, j-i)
        bandwidth_new = max(bandwidth_new, 1)
        cidx = int(not(store_as_rows_new)) # index compressed to bandwidth
        start_new = np.zeros(self.shape[cidx], dtype=int)
        data_new = np.zeros((self.shape[0], bandwidth_new)) if store_as_rows_new else np.zeros((bandwidth_new, self.shape[1]), order='F')
        p0 = 0
        for i in xrange(self.shape[cidx]):
            while (p0 < self.shape[1-cidx]) and (start[p0]+self.bandwidth <= i):
                p0 += 1
            start_new[i] = p0
            for j in xrange(bandwidth_new):
                if (p0 + j < start.shape[0]) and (i - start[p0+j] >= 0):
                    if store_as_rows_new:
                        data_new[i, j] = data[i-start[p0+j], p0+j]
                    else:
                        data_new[j, i] = data[p0+j, i-start[p0+j]]
                elif (p0 < self.shape[cidx] - bandwidth_new):
                    break
        return BandMat(self.shape, bandwidth=bandwidth_new, store_as_rows=store_as_rows_new, data=(start_new, data_new))


#@jit
def _sq_rows_to_cols(l, u, start, data_old, data_new):
    for i in xrange(len(data_old)):
        j0 = start[i]
        for j in xrange(data_old.shape[-1]):
            if (u+i-j-j0 >= 0) and (u+i-j-j0 < l+u+1):
                data_new[u+i-j-j0, j+j0] = data_old[i][j]


def cho_solve(L,b):
    """
    Solve the linear system of equations A x = b given the Cholesky lower triangular factor of A
    Input:
        L: float[:,:] or square BandMat # lower triangular square matrix
        b: float[:] or float[:, ...] # right hand side, it must hold b.shape[0] == L.shape[1]
    Output:
        float[:] or float[:, ...] # system solution, same shape as b
    """
    if type(L) == BandMat:
        if L.square:
            if b.ndim > 1:
                b_shape = b.shape
                b = b.reshape(b_shape[0], reduce(lambda x,y: x*y, b_shape[1:]))
                result = np.empty(b.shape, order='F')
                for i in xrange(b.shape[1]):
                    result[:, i] = bmlinalg.cho_solve(L, b[:, i])
                return result.reshape(b_shape)
            return bmlinalg.cho_solve(L, b)
        else:
            raise TypeError("Matrix L must be a square matrix")
    else:
        return scipy.linalg.cho_solve((L, True), b, check_finite=False)


def cholesky(M):
    """
    Returns the Cholesky factor L (lower triangular) of a square matrix M, if it exists then M = L*L'
    Input:
        M: float[:,:] or square BandMat
    Output:
        L: float[:,:] or square Bandmat
    """
    if type(M) == BandMat:
        if M.square:
            result = bmlinalg.cholesky(M, lower=True)
            return BandMat(result.size, l=result.l, u=result.u, data=result.data)
        else:
            raise TypeError("Not a square matrix")
    else:
        return scipy.linalg.cholesky(M, lower=True, check_finite=False)[0]


def solve(A, b):
    if type(A) == BandMat:
        if A.square:
            return bmlinalg.solve(A,b)
        else:
            raise TypeError("Matrix A must be a square matrix")
    else:
        return scipy.linalg.solve(A, b)

def dot(a,b):
    if len(a.shape) > 2 or len(b.shape) > 2:
        raise VauleError("unsupported ndarray shape")
    if a.shape[-1] != b.shape[0]:
        raise ValueError("matrices are not aligned")
    ta = type(a)
    tb = type(b)
    ndarray_t = np.ndarray
    bm_t = BandMat
    # ndarray*ndarray
    if ta == tb == ndarray_t:
        return np.dot(a,b)
    # BandMat*ndarray
    elif (ta == bm_t) and (tb == ndarray_t):
        if a.square:
            if len(b.shape) == 1: # ndarray==vector
                return bm.dot_mv(a,b)
            else: # ndarray == matrix
                result = np.empty((a.shape[0], b.shape[1]))
                for i in xrange(b.shape[1]):
                    result[:, i] = bm.dot_mv(a, b[:, i])
                return result
        else:
            if len(b.shape) == 1:
                result = np.empty(a.shape[0])
            else:
                result = np.empty((a.shape[0], b.shape[1]))
            if a.store_as_rows:
                if a.transposed:
                    a = a.copy()
                _dot_rm(a.start, a.data, b, result)
            else:
                if not(a.transposed):
                    a = a.copy(switch_storage_format=True)
                    _dot_rm(a.start, a.data, b, result)
                else:
                    _dot_rm(a.start, a.data.T, b, result)
            return result
    # ndarray*BandMat
    elif (ta == ndarray_t) and (tb == bm_t):
        if b.square:
            if len(a.shape) == 1: # ndarray == vector
                return bm.dot_mv(b.T, a)
            else: # ndarray == matrix
                result = np.zeros((a.shape[0], b.shape[1]))
                for i in xrange(a.shape[0]):
                    result[i] = bm.dot_mv(b.T, a[i])
                return result
        else:
            if len(a.shape) == 1:
                result = np.empty(b.shape[1])
            else:
                result = np.empty((a.shape[0], b.shape[1]), order='F')
            if not(b.store_as_rows):
                if b.transposed:
                    b = b.copy()
                _dot_mc(a, b.start, b.data, result)
            else:
                if not(b.transposed):
                    b = b.copy(switch_storage_format=True)
                    _dot_mc(a, b.start, b.data, result)
                else:
                    _dot_mc(a, b.start, b.data.T, result)
            return result
    # BandMat*BandMat
    elif (ta == bm_t) and (tb == bm_t):
        if a.square and b.square:
            result = bm.dot_mm(a,b)
            return BandMat(size=result.size, l=result.l, u=result.u, data=result.data)
        elif not(a.square) and b.square:
            if not(a.store_as_rows):
                a = a.copy(switch_storage_format=True)
            elif a.transposed:
                a = a.copy()
            if b.transposed:
                b = b.copy()
            result = BandMat(size=a.shape, bandwidth=_dot_rs_bandwidth(a, b))
            _dot_rs(a.start, a.data, b.l, b.u, b.data, result.start, result.data)
            return result
        elif a.square and not(b.square):
            return dot(b.T, a.T).T
        else:
            if not(a.store_as_rows) and a.transposed and b.store_as_rows and b.transposed:
                return dot(b.T, a.T).T
            else:
                if not(a.store_as_rows):
                    a = a.copy(switch_storage_format=True)
                elif a.transposed:
                    a = a.copy()
                if b.store_as_rows:
                    b = b.copy(switch_storage_format=True)
                elif b.transposed:
                    b = b.copy()
                if a.shape[0] == b.shape[1] and a.shape[1] == b.shape[0]:
                    u, l = _dot_rcs_ul(a, b)
                    result = BandMat(size=a.shape[0], u=u, l=l)
                    _dot_rcs(a.shape, a.start, a.data, b.shape, b.start, b.data, u, l, result.data)
                else:
                    result = BandMat(size=(a.shape[0], b.shape[1]), bandwidth=_dot_rc_bandwidth(a, b))
                    _dot_rc(a.shape, a.start, a.data, b.shape, b.start, b.data, result.start, result.data)
                return result
    else:
        raise TypeError("unknown matrix type")


# banded row matrix * ndarray
def _dot_rm(start, data, b, result):
    if len(b.shape) == 1:
        for i in xrange(data.shape[0]):
            j0 = start[i]
            j1 = min(b.shape[0], j0+data.shape[1])
            result[i] = np.dot(data[i,:j1-j0], b[j0:j1])
    else:
        for i in xrange(data.shape[0]):
            j0 = start[i]
            j1 = min(b.shape[0], j0+data.shape[1])
            np.dot(data[i, :j1-j0], b[j0:j1], result[i])

# ndarray * banded column matrix
def _dot_mc(a, start, data, result):
    if len(a.shape) == 1:
        for i in xrange(data.shape[1]):
            j0 = start[i]
            j1 = min(a.shape[0], j0+data.shape[0])
            result[i] = np.dot(a[j0:j1], data[:j1-j0, i])
    else:
        for i in xrange(data.shape[1]):
            j0 = start[i]
            j1 = min(a.shape[1], j0+data.shape[0])
            np.dot(a[:, j0:j1], data[:j1-j0, i], result[:, i])

# banded row matrix * banded square matrix
def _dot_rs_bandwidth(a, b):
    if a.shape[0] == 0 or a.shape[1] == 0 or b.shape[0] == 0:
        return a.bandwidth
    result_bw = []
    l = b.l
    u = b.u
    a_bw = a.bandwidth
    b_bw = b.shape[0]
    for row in xrange(a.shape[0]):
        c0 = a.start[row]
        c1 = min(b.shape[1], c0 + a_bw)
        result_bw.append(min(b_bw, c1+u+1) - max(0, c0-l))
    return max(result_bw)

def _dot_rs(bstart, bdata, l, u, sdata, rstart, rdata):
    for row in xrange(bdata.shape[0]):
        c0 = bstart[row] #starting non-zero column in the full rectangular matrix
        c1 = min(sdata.shape[1], c0 + bdata.shape[1]) #ending non-zero column in the full matrix
        cs = max(0, c0 - l)
        for column in xrange(cs, min(sdata.shape[1], c1+u+1)):
            r0 = max(0, column - u) # starting non-zero row in the full square matrix
            r1 = min(sdata.shape[1], column + l + 1) # ending non-zero row in the full square matrix
            j0s = max(0, u - column)  # where the actual data starts in the square bandmat
            #j1s = column+l+1 - max(0, column+l+1-sdata.shape[1]) - max(0, column-u)
            j1s = sdata.shape[0] - max(0, column+l+1-sdata.shape[1]) # where the actual data ends in the square bandmat
            j0c = 0 # where the actual data starts in the rectangular bandmat
            j1c = c1 - c0 # where the actual data ends in the rectangular bandmat
            d0 = c0 - r0
            d1 = c1 - r1
            if d0 > 0: # if the rectangular matrix starts after the square matrix, correct the square bandmat starting point
                j0s += d0
            else: # otherwise correct the rectangular bandmat starting point
                j0c -= d0 # (d0 < 0)
            if d1 > 0: # if the rectangular matrix ends after the square matrix, correct the rectangular bandmat ending point
                j1c -= d1
            else:
                j1s += d1 # otherwise correct the square bandmat ending point
            rdata[row, column-cs] = np.dot(bdata[row, j0c:j1c], sdata[j0s:j1s, column])
            rstart[row] = cs

# banded row matrix * banded column matrix
def _dot_rc_bandwidth(a, b):
    if a.shape[0] == 0 or a.shape[1] == 0 or b.shape[0] == 0 or b.shape[1] == 0:
        return a.bandwidth
    result_bw = []
    a_bw = a.bandwidth
    b_bw = b.bandwidth
    column0 = 0
    column1 = 1
    for row in xrange(a.shape[0]):
        c0 = a.start[row]
        c1 = min(b.shape[0], c0 + a_bw)
        while column0 < b.shape[1] and b.start[column0] + b_bw <= c0:
            column0 += 1
        column1 = max(column1, column0)
        while column1 < b.shape[1] and b.start[column1] < c1:
            column1 += 1
        result_bw.append(column1 - column0)
    return max(result_bw)

def _dot_rc(ashape, astart, adata, bshape, bstart, bdata, rstart, rdata):
    a_bw = adata.shape[1]
    b_bw = bdata.shape[0]
    column0 = 0
    column1 = 1
    for row in xrange(ashape[0]):
        c0 = astart[row]
        c1 = min(bshape[0], c0 + a_bw)
        while column0 <bshape[1] and bstart[column0] + b_bw <= c0:
            column0 += 1
        column1 = max(column1, column0)
        while column1 < bshape[1] and bstart[column1] < c1:
            column1 += 1
        rstart[row] = column0
        for column in xrange(column0, column1):
            if astart[row] < bstart[column]:
                i = bstart[column] - astart[row]
                j = 0
            else:
                i = 0
                j = astart[row] - bstart[column]
            d = min(a_bw - i, ashape[1] - astart[row], b_bw - j, bshape[0] - bstart[column])
            rdata[row, column-column0] = np.dot(adata[row, i:i+d], bdata[j:j+d, column])

def _dot_rcs_ul(a, b):
    if a.shape[0] == 0 or a.shape[1] == 0 or b.shape[0] == 0 or b.shape[1] == 0:
        return 0,0
    u = []
    l = []
    a_bw = a.bandwidth
    b_bw = b.bandwidth
    column0 = 0
    column1 = 1
    for row in xrange(a.shape[0]):
        c0 = a.start[row]
        c1 = min(b.shape[0], c0 + a_bw)
        while column0 < b.shape[1] and b.start[column0] + b_bw <= c0:
            column0 += 1
        column1 = max(column1, column0)
        while column1 < b.shape[1] and b.start[column1] < c1:
            column1 += 1
        u.append(column1 - row - 1)
        l.append(row - column0)
    return max(u), max(l)


def _dot_rcs(ashape, astart, adata, bshape, bstart, bdata, u, l, rdata):
    a_bw = adata.shape[1]
    b_bw = bdata.shape[0]
    column0 = 0
    column1 = 1
    for row in xrange(ashape[0]):
        c0 = astart[row]
        c1 = min(bshape[0], c0 + a_bw)
        while column0 <bshape[1] and bstart[column0] + b_bw <= c0:
            column0 += 1
        column1 = max(column1, column0)
        while column1 < bshape[1] and bstart[column1] < c1:
            column1 += 1
        for column in xrange(column0, column1):
            if astart[row] < bstart[column]:
                i = bstart[column] - astart[row]
                j = 0
            else:
                i = 0
                j = astart[row] - bstart[column]
            d = min(a_bw - i, ashape[1] - astart[row], b_bw - j, bshape[0] - bstart[column])
            rdata[u+row-column, column] = np.dot(adata[row, i:i+d], bdata[j:j+d, column])
