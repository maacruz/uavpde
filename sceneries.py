import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import patches
from maps import DTEDmap
from adus import ADU
from uav import UAV

def _blockconf(conf, grouptag, elementtag):
    """
    Return a configuration block as a list, if the configuration block doesn't exists return an empty list
    Usecase: when a configuration block has only one element, xmltodict doesn't put that element in a list
       _blocconf(xmltodict(<blocktag><repeatedtag>...</repeatedtag></blocktag>),'blocktag','repeatedtag') => [...]
    """
    try:
        blk = conf[grouptag][elementtag]
        if not(isinstance(blk,list)): blk = [blk]
    except:
        blk = []
    return blk

# Scenery class
class Scenery(object):
    """
    Contains the UAV scenery definition:
       dtedmap: DTEDmap object
       UAVlist: list of UAV objects, must have at least one ### WARNING:Only the first one is used at the moment (SingleUAV case)
       ADUlist: list of ADU objects, may be empty
       NFZlist: list of NFZ, may be empty. NFZ is a tuple(x_west:float, x_east:float, y_south:float, y_north:float)
       waypoints: dict to store a list of fixed waypoints for each UAV, indexed by UAV.idn or the special string "SharedWP". waypoint is a tuple(type:string, x:float, y:float, z:float or None). type can be "WP" or "Refuelling"
       uav_height_safetimargin: float # minimum height the UAV should have over the terrain
    """
    def __init__(self, pconf=None, sceneconf=None):
        """
        Input:
            pconf=None: dict()       # xmldict from parsing legacy configuration problem.xml
            sceneconf=None: dict()   # xmldict from parsing legacy configuration from scenery.xml
        """
        self.dtedmap = DTEDmap()
        self.UAVlist = []
        self.ADUlist = []
        self.NFZlist = []
        self.waypoints = {'SharedWP':[]}
        self.uav_height_safetymargin = 0.0
        if pconf or sceneconf:
            self._setup(pconf, sceneconf)

    def _setup(self, pconf, sceneconf):
        """
        Setup the scenery from the legacy configuration data from xml files
        Input:
            pconf: dict() # problem configuration xmltodict data
            sceneconf: dict() # scenery configuration xmltodict data
        """
        try:
            self.uav_height_safetymargin = float(pconf['AlturaMapa']) #minimum height the uav should have over the terrain
        except KeyError: pass
        try:
            ignoreADUz = (pconf['RadarSuelo']=='1') #if True ADU z coordinate in the config file is ignored
        except KeyError:
            ignoreADUz = False
        
        # ADUs
        for aduconf in _blockconf(sceneconf, 'SingleADUs', 'AirDefenseUnit'):
            adu_idn = aduconf['Identifier']
            ispopup = (aduconf['IsPopUp'].lower() == 'true')
            position = aduconf['Position']
            x = float(position['X'])
            y = float(position['Y'])
            z = float(position['Z']) if not(ignoreADUz) else None
            adu_type = int(aduconf['DetectionRadar']['Type'])
            self.ADUlist.append(ADU(adu_idn, adu_type, x, y, z, ispopup))
        # NFZs
        for nfzconf in _blockconf(sceneconf, 'NoFlightZones', 'NoFlightZone'):
            position = nfzconf['Position']
            x_west = float(position['x_West'])
            x_east = float(position['x_East'])
            y_south = float(position['y_South'])
            y_north = float(position['y_North'])
            self.NFZlist.append((x_west,x_east,y_south,y_north))
        # MultiUAV waypoints, unused
        waypointlist = []
        for wpconf in _blockconf(sceneconf, 'SharedWPs', 'SharedWP'):
            x, y = float(wpconf['x']), float(wpconf['y'])
            z = float(wpconf['z']) if wpconf['z'] else None
            waypointlist.append(('WP', x, y, z))
        self.waypoints['SharedWP'] = waypointlist
        # UAVs
        for uavconf in _blockconf(sceneconf, 'AirVehicles', 'AirVehicle'):
            uav_idn = uavconf['Identifier']
            dimensions = uavconf['Dimensions']
            uav = UAV(uav_idn, float(dimensions['@A']), float(dimensions['@B']), float(dimensions['@C']))
            try:
                uav.V = float(uavconf['InitialState']['Velocity'])
            except KeyError: pass
            try:
                uav.fuel = float(uavconf['InitialState']['Fuel'])
            except KeyError: pass
            uav.FlightCeiling = float(pconf[uav_idn]['TechoVuelo'])
            self.UAVlist.append(uav)
            uav.waypoints = []
            for wpconf in _blockconf(uavconf, 'Trajectories', 'Way_Point'):
                uav.waypoints.append((wpconf['@Type'], float(wpconf['@x']), float(wpconf['@y']), float(wpconf['@z'])))
            uav.orientation = np.arctan2(uav.waypoints[1][1] - uav.waypoints[0][1], uav.waypoints[1][2] - uav.waypoints[0][2])
        # DTED file
        self.dtedmap.loadMat(sceneconf['Terrain']['ElevationFile'])
        for adu in self.ADUlist:
            self.dtedmap.setPOI(adu.idn, adu.x, adu.y, adu.z, adu.viewlimit)
            if ignoreADUz:
                adu.z = self.dtedmap.getMapHeight(adu.x, adu.y)

    def draw(self):
        """
        Draw the scenery: paint the waypoints, ADUs and NFZs over the map image
        """
        fig, ax = plt.subplots()
        ax.imshow(self.dtedmap.dted, origin='lower')
        ax.autoscale(False)
        # draw waypoints
        for uav in self.UAVlist:
            x, y = np.array([wp[1:3] for wp in uav.waypoints]).T
            x, y = self.dtedmap.getGridCoordinates(x, y)
            ax.scatter(x, y, s=100, c='black', marker='*', facecolors='none')
            ax.plot(x, y, c='black', linewidth=2, linestyle='dashed')
        # draw ADUs
        x = np.array([adu.x for adu in self.ADUlist])
        y = np.array([adu.y for adu in self.ADUlist])
        x, y =  self.dtedmap.getGridCoordinates(x, y)
        killR, _ = self.dtedmap.getGridCoordinates(adu.killRange, 0)
        viewR, _ = self.dtedmap.getGridCoordinates(adu.viewlimit, 0)
        ax.scatter(x, y, s=10, c='red', marker='o')
        for i in xrange(len(x)):
            ax.add_patch(patches.Circle((x[i], y[i]), radius=killR, alpha=0.4, color='red', fill=True))
            ax.add_patch(patches.Circle((x[i], y[i]), radius=killR, alpha=0.8, color='red', fill=False, linewidth=2))
            ax.add_patch(patches.Circle((x[i], y[i]), radius=viewR, alpha=0.4, color='yellow', fill=True))
            ax.add_patch(patches.Circle((x[i], y[i]), radius=viewR, alpha=0.8, color='grey', fill=False, linewidth=2, linestyle='dashed'))
        #draw NFZs
        for (x0,x1,y0,y1) in self.NFZlist:
            x0, y0 = self.dtedmap.getGridCoordinates(x0, y0)
            x1, y1 = self.dtedmap.getGridCoordinates(x1, y1)
            ax.add_patch(patches.Rectangle((x0, y0), x1-x0, y1-y0, alpha=0.5, facecolor='orange'))
            ax.add_patch(patches.Rectangle((x0, y0), x1-x0, y1-y0, alpha=0.8,  fill=False))
        return fig, ax
        
