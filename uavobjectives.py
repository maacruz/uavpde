import numpy as np
import xmltodict
from scipy.interpolate import splprep, splev

from adus import adu_Pkill
from utils import *


@jit
def _UAV_max_load(z):
    """
    Maximum load factor at each trajectory point
    Positive between [-23667.63112,  23667.63201]
    Input:
      z: float[n] #trajectory height
    Output:
      float[n] #maximum load factor
    """
    return 5.3809e-9*z**2 - 4.4291e-4*z + 6.1


@jit
def _UAV_load(traj, V):
    """
    airplane load factor at each trajectory point
    see http://www.aerospaceweb.org/question/performance/q0146.shtml for derivation and relationship to turning radius and roll angle
    since it uses a second derivative, returned length vector is 2 less than input vector lenght, first and last points in the trajectory don't have load factor
    Input:
      traj: float[3,n] #trajectory
      V: float #speed
    Output:
      float[n-2] #maximum load factor
    """
    x = traj[0]
    y = traj[1]
    d = np.sqrt((x[1:]-x[:-1])**2 + (y[1:]-y[:-1])**2)
    #circumcircle radius of a triangle: R = l1*l2*l3/np.sqrt((l1+l2+l3)*(l2+l3-l1)*(l3+l1-l2)*(l1+l2-l3))
    l1 = d[:-1]
    l2 = d[1:]
    l3 = np.sqrt((x[2:]-x[:-2])**2 + (y[2:]-y[:-2])**2)
    #curvature radius is: R = V**2/(9.81*sqrt(n**2-1))
    n = np.sqrt(1 + V**4*(l1+l2+l3)*(l2+l3-l1)*(l3+l1-l2)*(l1+l2-l3) / (9.81*l1*l2*l3)**2)
    return n


@jit
def _UAV_climbing_limit(z):
    """
    UAV max climbing slope at each trajectory point
    Input:
      z: float[n] #trajectory height
    Output:
      float[n] #maximum climbing slope (positive numbers)
    """
    return -1.5377e-10*z**2 - 2.6997e-5*z + 0.4211


@jit
def _UAV_diving_limit(z):
    """
    UAV max diving slope at each trajectory point
    Input:
      z: float[n] #trajectory height
    Output:
      float[n] #maximum diving slope (negative numbers)
    """
    return 2.5063e-9*z**2 - 6.3014e-6*z - 0.3257


@jit
def _UAV_slope(traj):
    """
    UAV current slope at each trajectory point
    Input:
      traj: float[3,n] #trajectory
    Output:
      float[n-1] #current slope
    """
    x = traj[0]
    y = traj[1]
    z = traj[2]
    return (z[1:]-z[:-1])/np.sqrt((x[1:]-x[:-1])**2+(y[1:]-y[:-1])**2)


@jit
def _UAV_length(traj):
    """
    Length of each trajectory discrete step
    Input:
      traj: float[3,n] #trajectory
    Output:
      float[n-1] #length
    """
    x = traj[0]
    y = traj[1]
    z = traj[2]
    return np.sqrt((x[1:]-x[:-1])**2+(y[1:]-y[:-1])**2+(z[1:]-z[:-1])**2)


@jit
def _UAV_hlength(traj):
    x = traj[0]
    y = traj[1]
    return np.sqrt((x[1:]-x[:-1])**2+(y[1:]-y[:-1])**2)


#@jit
def UAVcost_turning_limit(discretized_trajectory, V):
    """
    Turning limit objective
    Input:
      discretized_trajectory: float[3,n] #trajectory
      V: float #UAV speed
    Output:
      int #number of trajectory points where the current turning radius (current load) is smaller (greater) than the minimum radius (maximum load) at that point
    """
    z = discretized_trajectory[2]
    n_max = _UAV_max_load(z)
    #R = l1*l2*l3/np.sqrt((l1+l2+l3)*(l2+l3-l1)*(l3+l1-l2)*(l1+l2-l3))
    #R_min = V**2/9.81/np.sqrt(n_max**2-1)
    #c = (R<R_min).sum()
    n = _UAV_load(discretized_trajectory,V)
    c = np.sum((n_max[1:-1] < n))
    return c


#@jit
def UAVcost_slope_limits(discretized_trajectory):
    """
    Slope limits objective
    Input:
      discretized_trajectory: float[3,n] #trajectory
    Output:
      int #number of trajectory points where the current slope is either greater than the maximum climbing or diving slope
    """
    z = discretized_trajectory[2]
    climbing_limit = _UAV_climbing_limit(z)
    diving_limit = _UAV_diving_limit(z)
    slope = _UAV_slope(discretized_trajectory)
    c = np.sum((slope<=diving_limit[:-1]) | (slope>=climbing_limit[:-1]))
    return c


@jit
def _UAV_fuel(z, n, n_max, climbing_slope, climbing_limit):
    """
    UAV fuel consumption
    Input:
      z: float[m] #trajectory height
      n: float[m-2] #trajectory load
      n_max: float[m] #trajectory maximum load
      climbing_slope: float[m-1] #trajectory climbing slope
      climbing_limit: float[m] #trajectory climbing limit
    Output:
      float
    """
    z2 = z**2
    fuel_straight = 9.553e-8*z2 - 2.4524e-3*z + 29.5
    fuel_climbing_limit = 1.6679e-11*z*z2 - 2.4832e-7*z2 - 4.259e-3*z + 87.881
    fuel_turning_limit = -3.0435e-1*n_max**2 + 15.552*n_max + 0.3565
    fuel_t = fuel_straight[1:-1] + climbing_slope[1:]/climbing_limit[1:-1]*(fuel_climbing_limit[1:-1]-fuel_straight[1:-1]) + np.sqrt(n**2-1+1e-15)/np.sqrt(n_max[1:-1]**2-1)*(fuel_turning_limit[1:-1]-fuel_straight[1:-1])
    return fuel_t


def UAVcost_fuel_limit(discretized_trajectory, fuel_limit, V, refueling_waypoint_list=[]):
    """
    Fuel limit objective
    Input:
      discretized_trajectory: float[3,n] #trajectory
      fuel_limit: float #maximum fuel allowed
      V: float #UAV speed
      refueling_waypoint_list: list of int #indexes of refueling waypoints on the discrete trajectory
    Output:
      float #amount of overspent fuel
    """
    z = discretized_trajectory[2]
    n_max = _UAV_max_load(z)
    climbing_limit = _UAV_climbing_limit(z)
    n = _UAV_load(discretized_trajectory, V)
    climbing_slope = np.maximum(0,_UAV_slope(discretized_trajectory))
    dt = _UAV_length(discretized_trajectory)/(V*1000)
    fuel = _UAV_fuel(z,n,n_max,climbing_slope,climbing_limit)*dt[1:]
    c = 0.0
    rf = [0]+refueling_waypoint_list+[None]
    for i in xrange(len(rf)-1):
        c += max(0,np.sum(fuel[rf[i]:rf[i+1]])-fuel_limit)
    return c


def UAVcost_flight_altitude(discretized_trajectory, dtedmap, scale_factor, minimum_height=0.0):
    """
    Average trajectory (relative wrt the map) altitude and minimum altitude objectives
    Input:
      discretized_trajectory: float[3,n] #trajectory
      dtedmap: DTEDmap object #map
      minimum_height=0: float #minimum (relative) altitude
    Output:
      (int, float) #(number of trajectory points under the minimum altitude, average altitude)
    """
    x = discretized_trajectory[0]
    y = discretized_trajectory[1]
    z = discretized_trajectory[2]
    terrain_z = dtedmap.getMapHeight(x,y)
    height = np.fmax(0, z-terrain_z) #!!!
    c_lowerlimit = np.sum(height <= minimum_height)
    d = _UAV_hlength(discretized_trajectory)
    h = (height[1:] + height[:-1])/2
    l = d.sum()
    if l > 0:
        c = np.sum(h*d)/d.sum()
    else:
        c = np.average(h)
    return c_lowerlimit, c/scale_factor


def UAVcost_map_limit(discretized_trajectory, dtedmap):
    """
    Trajectory within map limits objective
    Input:
      discretized_trajectory: float[3,n] #trajectory
      dtedmap: DTEDmap object #map
    Output:
      int #number of trajectory points out of the map bounds
    """
    x = discretized_trajectory[0]
    y = discretized_trajectory[1]
    c = np.sum((x<dtedmap.x_westlimit) | (x>dtedmap.x_eastlimit) | (y<dtedmap.y_southlimit) | (y>dtedmap.y_northlimit))
    return c


def UAVcost_map_nfz(discretized_trajectory, NFZlist=[]):
    """
    Trajectory out of NFZs objective
    Input:
      discretized_trajectory: float[3,n] #trajectory
      NFZlist: list of NFZ #NFZ is a tuple(x_west:float, x_east:float, y_south:float, y_north:float)
    Output:
      float #sum of distance from each trajectory point inside a NFZ to the closest NFZ boundary
    """
    x = discretized_trajectory[0]
    y = discretized_trajectory[1]
    d_accum = np.zeros(x.size)
    for nfz in NFZlist:
        x_west,x_east,y_south,y_north = nfz
        d = np.fmax(np.fmin(np.fmin(x-x_west,x_east-x), np.fmin(y-y_south,y_north-y)), 0)
        d_accum += d
    c = d_accum.sum()
    return c


#@jit
def UAVcost_path_length(discretized_trajectory, lmin):
    """
    Path length objective
    Input:
      discretized_trajectory: float[3,n] #trajectory
    Output:
      float #normalized trajectory length
    """
    d = _UAV_length(discretized_trajectory)
    return np.round(d.sum()*2000/lmin)/2000.0 #DISCRETIZATION NOT IN THE PAPER


def UAVcost_PDetectKillADU(discretized_trajectory, V, dtedmap, adu, uav):
    """
    Probability of detection and destruction by the given ADU
    Input:
      discretized_trajectory: float[3,n] #trajectory
      V: float #UAV speed
      dtedmap: DTEDmap object #map
      adu: ADU object
      uav: UAV object
    Output:
      tuple(float, float) #(probability of detection, probability of destruction)
    """
    x = discretized_trajectory[0]
    y = discretized_trajectory[1]
    z = discretized_trajectory[2]
    los_idx = np.nonzero(dtedmap.isVisibleFromPOI(x, y, z, adu.idn)[1:-1])[0] #indexes of trajectory points radar visible from ADU
    if len(los_idx) == 0:
        return 0.0, 0.0
    return _PDetectKillADU(discretized_trajectory, los_idx, V, adu.adu_type, adu.x, adu.y, adu.z, adu.zeta1, adu.zeta2, uav.a, uav.b, uav.c)


@jit
def _PDetectKillADU(discretized_trajectory, los_idx, V, adu_type, adu_x, adu_y, adu_z, adu_zeta1, adu_zeta2, uav_a, uav_b, uav_c):
    """
    Private function called from PDetectKillADU, containing the actual implementation
    """
    x = discretized_trajectory[0]
    y = discretized_trajectory[1]
    z = discretized_trajectory[2]
    x_adu = adu_x - x[1:-1][los_idx]
    y_adu = adu_y - y[1:-1][los_idx]
    z_adu = adu_z - z[1:-1][los_idx]
    d2_adu = x_adu**2 + y_adu**2 + z_adu**2
    dx = (x[1:]-x[:-1])[1:][los_idx]
    dy = (y[1:]-y[:-1])[1:][los_idx]
    dz = (z[1:]-z[:-1])[1:][los_idx]
    dl = _UAV_length(discretized_trajectory)[1:][los_idx]
    
    # Calculate UAV detection probability by current ADU PRD_adu
    n = _UAV_load(discretized_trajectory, V)[los_idx] #load factor, allows to calculate roll angle
    #elevation = UAV-to-ADU line to horizontal plane angle
    #el = np.arctan2(z_adu, np.sqrt((x_adu**2+y_adu**2))) 
    tan_el = z_adu / (np.sqrt((x_adu**2+y_adu**2))+1e-12)
    cos_el = 1.0/np.sqrt(1.0+tan_el**2)
    #azimut = angle between UAV direction and UAV-to-ADU line projected on the horizontal plane
    #az = np.arctan2(y_adu*dx - x_adu*dy, x_adu*dx - y_adu*dy)
    sin_az = (y_adu*dx - x_adu*dy)
    cos_az = (x_adu*dx - y_adu*dy)
    r_az = np.sqrt(sin_az**2 + cos_az**2) + 1e-12
    sin_az /= r_az
    cos_az /= r_az
    #angle between UAV direction and UAV-to-ADU line (trigonometric trick)
    #aze = np.arccos(np.cos(el)*np.cos(az))
    cos_aze = cos_el*cos_az
    sin_aze = np.sqrt(1.0-cos_aze**2)
    #roll angle, see http://www.aerospaceweb.org/question/performance/q0146.shtml
    #psi = np.arccos((1.0-1e-12)/n) # decrease 1.0 by epsilon=1e-12 to avoid eventual NaNs due to FP errors
    cos_psi = (1.0-1e-12)/n
    sin_psi = np.sqrt(1.0-cos_psi**2)
    #psiE = psi - np.arctan2(np.tan(el), np.sin(az)) #??
    d_elaz = np.sqrt(tan_el**2+sin_az**2) + 1e-12
    cos_elaz = sin_az/d_elaz
    sin_elaz = tan_el/d_elaz # tan_elaz*cos_elaz
    cos_psiE = cos_psi*cos_elaz + sin_psi*sin_elaz
    sin_psiE = sin_psi*cos_elaz - cos_psi*sin_elaz
    #ellipsoid radar cross section
    #rcs = np.pi*(uav_a*uav_b*uav_c/8)**2/((uav_a/2*np.sin(aze)*np.cos(psiE))**2 + (uav_b/2*np.sin(aze)*np.sin(psiE))**2 + (uav_c/2*np.cos(aze))**2)**2 
    rcs = np.pi*(uav_a*uav_b*uav_c/8)**2/((uav_a/2*sin_aze*cos_psiE)**2 + (uav_b/2*sin_aze*sin_psiE)**2 + (uav_c/2*cos_aze)**2)**2  + 1e-12
    # Detection probability at each point
    DP = 1.0/(1.0+adu_zeta2*(d2_adu**2/rcs)**adu_zeta1)
    # Detection probability
    PRD_adu = 1.0 - np.prod(1.0-DP)
    
    # Calculate UAV destruction probability by current ADU PRK_adu
    d_adu = np.sqrt(d2_adu)
    cos_theta = (x_adu*dx + y_adu*dy + z_adu*dz)/(d_adu*dl)
    Pk = adu_Pkill(adu_type, d_adu, cos_theta, z_adu)
    Pk_adu = _Pk_reduce(Pk, los_idx)
    return PRD_adu, Pk_adu


@jit
def _Pk_reduce(Pk, los_idx):
    cons = los_idx[1:]-los_idx[:-1]
    Pk_adu = Pk[0]
    for i in xrange(cons.size):
        if cons[i] == 1:
            Pk_adu = max(Pk_adu,Pk[i+1]) #!!! NOT IN THE PAPER
        else:
            Pk_adu = 1.0 - (1.0-Pk_adu)*(1.0-Pk[i+1])
    return Pk_adu


def UAVcost_PDetectKill(discretized_trajectory, V, dtedmap, ADUlist, uav):
    """
    Probability of detection and destruction
    Input:
      discretized_trajectory: float[3,n] #trajectory
      V: float #UAV speed
      dtedmap: DTEDmap() # map object
      ADUlist: list of ADU()
      uav: UAV() #UAV object, if None use the first one in the scenery
    Output:
      tuple(float, float) #(probability of detection, probability of destruction)
    """
    P_adu = [UAVcost_PDetectKillADU(discretized_trajectory, V, dtedmap, adu, uav) for adu in ADUlist]
    PRD = 1.0 - reduce(lambda x0,x1:x0*(1.0-x1[0]), P_adu, 1.0)
    Pk = 1.0 - reduce(lambda x0,x1:x0*(1.0-x1[1]), P_adu, 1.0)
    if Pk < 0.995: #DISCRETIZATION NOT IN THE PAPER!!!
        Pk = np.round(100*Pk)/100.0
    return PRD, Pk


class UAVObjective(object):
    """
    Class to provide an objective function for a given scenery and legacy configuration
    """
    def __init__(self, scenery, pconf=None, trayconf=None, uavidx=0):
        """
        Input:
            scenery: Scenery()      # scenery object
            pconf=None: dict()      # xmldict from parsing legacy configuration problem.xml
            trayconf=None: dict()   # xmldict from parsing legacy configuration from tray.xml
            uavidx=0: int           # UAV index in the scenery
        """
        self._xml_setup(pconf, trayconf)

        self.scenery = scenery
        self.uav = scenery.UAVlist[uavidx]
        self.refueling_waypoints = self.uav.get_refueling_wpidx() #what about shared waypoints??
        wp = self.uav.get_waypoints()#.reshape(1,3,-1)
        self.lmin = np.sum(np.linalg.norm(np.diff(wp, axis=1), axis=0))

        # to fix scaling issues when using crowding measures make average altitude relative to the average altitude of a straight line flight
        #self.avgaltitude = (np.array([wp[3] for wp in self.uav.waypoints[:-1]]) + np.diff([wp[3] for wp in self.uav.waypoints])/2.0) * np.linalg.norm(np.diff([wp[1:] for wp in self.uav.waypoints], axis=0),2,axis=1)/self.lmin
        self.avgaltitude = self._get_scene_average_altitude(wp, scenery)


    def _get_scene_average_altitude(self, wp, scenery):
        tck, u = splprep(wp, k=1)
        l = np.sum(np.linalg.norm(np.diff(wp, axis=1), 2, axis=0))
        u = np.linspace(0.,1.,max(int(l/self.spline_discretization_step),2))
        t = np.stack(splev(u, tck))
        _, avgalt = UAVcost_flight_altitude(t, scenery.dtedmap, 1.0, 0)
        return avgalt


    def _xml_setup(self, pconf, trayconf):
        """
        Setup the scenery from the legacy configuration data from xml files
        To be used during initialization
        Objectives are identified by the strings:
            'Suelo'      -> UAVcost_flight_altitude()[0]   # No crashing to the ground constraint
            'Elevacion'  -> UAVcost_flight_altitude()[1]   # Average flight altitude
            'Pendiente'  -> UAVcost_slope_limits()         # Climbing/diving slope within limits constraint
            'MapaLimite' -> UAVcost_map_limit()            # Trajectory within map bounds constraint
            'Radios'     -> UAVcost_turning_limit()        # Turning limit constraint
            'NFZ'        -> UAVcost_map_nfz()              # No flying over NFZs constraint
            'Combustible'-> UAVcost_fuel_limit()           # Fuel consumption constraint
            'Longitud'  ->  UAVcost_path_length()          # Path length
            'PDeteccion' -> UAVcost_PDetectKill()[0]       # Detected by ADUs probability
            'PKill'      -> UAVcost_PDetectKill()[1]       # Killed by ADUs probability
        Objective goals (lower and upper limits) are ignored since they make optimization harder and don't make much sense anyway (removed in the 2013 paper)
        Input:
            pconf: dict() # problem configuration xmltodict data
            trayconf: dict() or None # trajectory configuration xmltodict data
        Result:
            Initializes
                self.nobjectives
                self.objective_list
                self.priorities
                self.priorityorder
                self.spline_discretization_step
        """
        objPriority = {}
        objnamelist = ['Suelo', 'Elevacion', 'Pendiente', 'MapaLimite', 'Radios', 'NFZ', 'Combustible', 'Longitud', 'PDeteccion', 'PKill'] #objective name strings in the order obtained inside self.objective_function

        if pconf:
            # find all tags under tag UAV1 named xxxPrior, those are the objective priorities
            for k,v in pconf[self.uav.idn].iteritems():
                if k.endswith('Prior'):
                    objPriority[k[:-5]]=int(v) #remove the trailing Prior from the tag and use it as key
            self.objective_list = sorted(objPriority.items(), key=lambda x:x[1])
        else:
            self.objective_list = [(n, 1) for n in objnamelist]
        self.nobjectives = len(self.objective_list)

        # build priority list for the objective comparison function
        priority_list = [list() for i in xrange(max([o[1] for o in self.objective_list]))]
        for o in self.objective_list:
            priority_list[o[1]-1].append(o[0])
        priorities = [0]
        for i in xrange(len(priority_list)):
            priorities.append(priorities[-1] + len(priority_list[i]))
        self.priorities = zip(priorities[:-1], priorities[1:])

        # build a sorting index array to resort objectives to their configured priority order
        objvalidx = dict()
        for i in xrange(len(objnamelist)):
            objvalidx[objnamelist[i]] = i
        self.priorityorder = np.array([objvalidx[o[0]] for o in self.objective_list])

        if trayconf:
            self.spline_discretization_step = float(trayconf[self.uav.idn]['MaxSeparation'])*1000.0 # configuration file has the value in km
        else:
            self.spline_discretization_step = 1000.0

        return


    def objective_function(self, population):
        """
        UAV Objective function for the optimization algorithm
        Returns the objective values of the input population
        Input:
            population: Class()
                            size: int # population size
                            discretize: float[3,:] function(int, float)
        Output:
            float[population.size,10]
        """
        objvaltmp = np.zeros(self.nobjectives)
        objective_values = np.empty((population.size, self.nobjectives))
        dtedmap = self.scenery.dtedmap
        ADUlist = self.scenery.ADUlist
        NFZlist = self.scenery.NFZlist
        minimum_height = self.scenery.uav_height_safetymargin
        for i in xrange(population.size):
            # Discretize trajectory
            discretized_trajectory, discretized_waypoints_list = population.discretize(i, self.spline_discretization_step)
            refueling_waypoint_list = [discretized_waypoints_list[j] for j in self.refueling_waypoints]
            # Calculate objective values
            objvaltmp[0:2] = UAVcost_flight_altitude(discretized_trajectory, dtedmap, self.avgaltitude, minimum_height)
            objvaltmp[2:3] = UAVcost_slope_limits(discretized_trajectory)
            objvaltmp[3:4] = UAVcost_map_limit(discretized_trajectory, dtedmap)
            objvaltmp[4:5] = UAVcost_turning_limit(discretized_trajectory, self.uav.V)
            objvaltmp[5:6] = UAVcost_map_nfz(discretized_trajectory, NFZlist)
            objvaltmp[6:7] = UAVcost_fuel_limit(discretized_trajectory, self.uav.fuel, self.uav.V, refueling_waypoint_list)
            objvaltmp[7:8] = UAVcost_path_length(discretized_trajectory, self.lmin)
            objvaltmp[8:10] = UAVcost_PDetectKill(discretized_trajectory, self.uav.V, dtedmap, ADUlist, self.uav)
            objective_values[i] = objvaltmp[self.priorityorder]
        return objective_values