import numpy as np
import maps
from maps import *
##########################################################################
### Unit tests
def test_R2_compute_viewshed_y():
    #2x3 map with an elevation of 1 at (0,1) and poi at upper left corner (0,2)
    dtedmap = DTEDmap((3,2))
    dtedmap.dted[1,0] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    ar = dtedmap.dx/dtedmap.dy
    maps._R2_compute_viewshed_y(0, 2, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[2,1],[1,0.5],[-abignumber,-abignumber]]))

    #2x3 map with an elevation of 1 at (0,1) and poi at lower left corner (0,0)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_y(0, 0, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber,-abignumber],[1,0.5],[2,1]]))

    #2x3 map with an elevation of 1 at (0,1) and poi at upper right corner (1,2)
    dtedmap = DTEDmap((3,2))
    dtedmap.dted[1,0] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_y(1, 2, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[1,0],[0.5,0],[-abignumber,-abignumber]]))

    #2x3 map with an elevation of 1 at (0,1) and poi at lower right corner (1,0)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_y(1, 0, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber,-abignumber],[0.5,0],[1,0]]))


    #2x3 map with an elevation of 1 at (1,1) and poi at upper right corner (1,2)
    dtedmap = DTEDmap((3,2))
    dtedmap.dted[1,1] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_y(1, 2, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[1,2],[0.5,1],[-abignumber,-abignumber]]))

    #2x3 map with an elevation of 1 at (1,1) and poi at lower right corner (1,0)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_y(1, 0, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber,-abignumber],[0.5,1],[1,2]]))

    #2x3 map with an elevation of 1 at (1,1) and poi at upper center (0.5,2)
    dtedmap = DTEDmap((3,2))
    dtedmap.dted[1,1] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_y(0.5, 2, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[0.5,1.5],[0.25,0.75],[-abignumber,-abignumber]]))

    #2x3 map with an elevation of 1 at (1,1) and poi at lower center (0.5,0)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_y(0.5, 0, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber,-abignumber],[0.25,0.75],[0.5,1.5]]))

    #3x3 map with an elevation of 1 at (1,1) and poi at (0.5,0.5) elevation 0.25
    dtedmap = DTEDmap((3,3))
    dtedmap.dted[1,1] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_y(0.5, 0.5, 0.25, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[0,0,0],[1.0/3.0,1,-abignumber],[0.5,1.5,2.5]]),atol=1e-5)

def test_R2_compute_viewshed_x():

    dtedmap = DTEDmap((1,1))
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(0, 0, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber]]).T)

    dtedmap = DTEDmap((2,3))
    dtedmap.dted[0,1] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(2, 0, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[2,1],[1,0.5],[-abignumber,-abignumber]]).T)

    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(0, 0, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber,-abignumber],[1,0.5],[2,1]]).T)


    dtedmap = DTEDmap((2,3))
    dtedmap.dted[0,1] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(2, 1, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[1,0],[0.5,0],[-abignumber,-abignumber]]).T)

    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(0, 1, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber,-abignumber],[0.5,0],[1,0]]).T)


    dtedmap = DTEDmap((2,3))
    dtedmap.dted[1,1] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(2, 1, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[1,2],[0.5,1],[-abignumber,-abignumber]]).T)

    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(0, 1, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber,-abignumber],[0.5,1],[1,2]]).T)


    dtedmap = DTEDmap((2,3))
    dtedmap.dted[1,1] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(2, 0.5, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[0.5,1.5],[0.25,0.75],[-abignumber,-abignumber]]).T)

    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(0, 0.5, 0, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[-abignumber,-abignumber],[0.25,0.75],[0.5,1.5]]).T)

    #3x3 map with an elevation of 1 at (1,1) and poi at (0.5,0.5) elevation 0.25
    dtedmap = DTEDmap((3,3))
    dtedmap.dted[1,1] = 1
    viewshed = np.empty(dtedmap.dted.shape)
    viewshed[...] = -abignumber
    R2d = -viewshed
    maps._R2_compute_viewshed_x(0.5, 0.5, 0.25, dtedmap.dted, dtedmap.dx/dtedmap.dy, viewshed, R2d)
    np.testing.assert_allclose(viewshed,np.array([[0,0,0],[1.0/3.0,1,-abignumber],[0.5,1.5,2.5]]).T,atol=1e-5)

def test_compute_viewshed():
    # simple 1x1 map
    dtedmap = DTEDmap((1,1))
    result, x1, x2, y1, y2 = compute_viewshed(dtedmap, 0, 0)
    np.testing.assert_allclose(result,np.array([[-abignumber]]).T)
    assert (x1, x2, y1, y2) == (0,1,0,1)
    
    # 3x4 map with poi at lower left corner (0,0), bumps in the diagonal direction at distance 1 grid cell
    dtedmap = DTEDmap((4,3))
    poi_x,poi_y = 0,0
    dtedmap.dted[1,1] = 1
    expectedresult = np.array([[-abignumber, 0, 0],
                               [0,1,1],
                               [0,2.0/3.0,2],
                               [0,1,2]])
    result, x1, x2, y1, y2 = compute_viewshed(dtedmap, poi_x, poi_y)
    np.testing.assert_allclose(result, expectedresult, atol=1e-5)
    assert (x1, x2, y1, y2) == (0,3,0,4)

    # test different aspect ratios
    dtedmap.dx=4.0
    expectedresult = np.array([[-abignumber, 0, 0],
                               [0,1,1],
                               [0,0.5,2],
                               [0,1,2]])
    result, x1, x2, y1, y2 = compute_viewshed(dtedmap, poi_x, poi_y)
    np.testing.assert_allclose(result, expectedresult, atol=1e-5)
    assert (x1, x2, y1, y2) == (0,3,0,4)

    # 5x7 map with poi at center (2,3), bumps around the center in the diagonal directions at distance 1 grid cell
    dtedmap = DTEDmap((7,5))
    poi_x,poi_y = 2,3 #center of the map
    dtedmap.dted[4,3] = 1
    dtedmap.dted[2,3] = 0.75
    dtedmap.dted[4,1] = 0.25
    dtedmap.dted[2,1] = 0.5
    A = np.array([[1,1],
                  [2.0/3.0,2],
                  [1,2]])
    expectedresult = np.vstack((np.flipud(np.hstack((0.5*np.fliplr(A),np.zeros((3,1)),0.75*A))),
                                np.array([0,0,-abignumber,0,0]),
                                np.hstack((np.fliplr(0.25*A),np.zeros((3,1)),A))))
    result, x1, x2, y1, y2 = compute_viewshed(dtedmap, poi_x, poi_y)
    np.testing.assert_allclose(result, expectedresult, atol=1e-5)
    assert (x1, x2, y1, y2) == (0,5,0,7)
    
def test_DTEDmap_POI():
    # 3x4 map with poi at diagonal lower left corner (0.5,0.5), bump in the diagonal direction at distance 1 grid cell
    dtedmap = DTEDmap((4,3))
    dtedmap.dted[1,1] = 1

    # test setPOI()
    dtedmap.setPOI("POI0",0.5,0.5,poi_viewlimit=1.5)
    poi_x, poi_y, poi_z, poi_viewlimit, viewshed, x1, x2, y1, y2 = dtedmap.poi["POI0"]
    assert (x1, x2, y1, y2) == (0,3,0,3)
    np.testing.assert_allclose(viewshed, np.array([[0,0,0],
                                                   [0,1,1.5],
                                                   [0.5,1.5,2.5]]), atol=1e-5)
    # test isVisibleFromPOI()
    x = np.array([0,0.5,0.5])
    y = np.array([2,1.5,1.5])
    z = np.array([0.5,0.74,0.76])
    los = dtedmap.isVisibleFromPOI(x,y,z,"POI0")
    np.testing.assert_array_equal(los,np.array([False,False,True]))
    
def test_DTEDmap_loadMat():
    dtedmap = DTEDmap()
    dtedmap.loadMat('test.map')